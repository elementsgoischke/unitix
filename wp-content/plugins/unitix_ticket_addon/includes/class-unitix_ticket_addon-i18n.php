<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.agentur-elements.com
 * @since      1.0.0
 *
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/includes
 * @author     Jan Goischke <j.goischke@ag-elements.com>
 */
class Unitix_ticket_addon_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'unitix_ticket_addon',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
