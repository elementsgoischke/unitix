<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.agentur-elements.com
 * @since      1.0.0
 *
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/includes
 * @author     Jan Goischke <j.goischke@ag-elements.com>
 */
class Unitix_ticket_addon_Activator {

	# $hook = array("_slug" => "name", "_class" => "classname", "_function" => "functionname", "_args" => array())
	private $hooks = array();
	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() 
	{
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/tools/brave-payments-verification/brave.func.php';

	}
}
