// WooCommerce Cart
jQuery(document).ready(function($)
{
    // Maximale Anzahl an Tickets
    var cartItemsMax = 10;

    // Maximale Anzahl auf 10 setzen
    $('[id^=quantity_]').attr('data-max', cartItemsMax);

    // Wenn sich der Wert des Input ändert
    $('[id^=quantity_]').change(function()
    {
      // Manuelle Änderungen ausschließen
      if ($(this).val() == 0) return;

      // Prüfen ob es nicht mehr als 10 sind
      if ($(this).val() > cartItemsMax) $(this).val(cartItemsMax);

      // Update der verfügbaren Tickets
      $('[id^=quantity_]').not($(this)).val(0).change();
    });

    // OnInput Event hinzufügen
    $('[id^=quantity_]').on('input', function(e) {
      $(this).change();
    });
});