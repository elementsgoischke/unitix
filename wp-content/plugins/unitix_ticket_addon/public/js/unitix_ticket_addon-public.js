(function( $ ) {
	'use strict';

	// External Link target Fix
	$(document).ready(function() {
		$("a[href^='http']").not("[href*='unitix.net']").attr('target','_blank');
	});

	

})( jQuery );