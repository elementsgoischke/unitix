// Tribe Events Page
jQuery(document).ready(function($)
{
  // Wenn Tickets verfügbar sind...
  if ($('.available-stock').length)
  {
    // Warenkorb abrufen
    var cartItems = parseInt( $('.header_cart_span').text(), 10);
    var cartItemsMax = 10;
    var cartItemsToAdd = cartItemsMax - cartItems;

    // Maximale Anzahl auf 10 setzen
    $('[id^=quantity_]').attr('data-max', cartItemsToAdd);

    // Wenn sich der Wert des Input ändert
    $('[id^=quantity_]').change(function()
    {
      // Manuelle Änderungen ausschließen
      if ($(this).val() == 0) return;

      // Prüfen ob es nicht mehr als 10 sind
      if ($(this).val() > cartItemsToAdd) $(this).val(cartItemsToAdd);

      // Update der verfügbaren Tickets
      $('[id^=quantity_]').not($(this)).val(0).change();
    });

    // OnInput Event hinzufügen
    $('[id^=quantity_]').on('input', function(e)
    {
      $(this).change();
    });
  }
});