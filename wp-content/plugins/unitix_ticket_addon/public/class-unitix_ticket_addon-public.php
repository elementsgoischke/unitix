<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.agentur-elements.com
 * @since      1.0.0
 *
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/public
 * @author     Jan Goischke <j.goischke@ag-elements.com>
 */
class Unitix_ticket_addon_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version )
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;

		// Options
		$this->disableAdminBar();

		// Shortcodes
		$this->addShortcode('Y',array('Unitix_ticket_addon_Public','getCopyrightYear'));
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Unitix_ticket_addon_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Unitix_ticket_addon_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name . '-public', plugin_dir_url( __FILE__ ) . 'css/unitix_ticket_addon-public.css', array(), $this->version, 'all' );
		#wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), '4.1.4', 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 * wp_enqueue_script( string $handle, string $src = '', array $deps = array(), string|bool|null $ver = false, bool $in_footer = false );
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{
		wp_enqueue_script( $this->plugin_name . '-public', plugin_dir_url( __FILE__ ) . 'js/unitix_ticket_addon-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-trusted-logo-head', plugin_dir_url( __FILE__ ) . 'js/unitix_ticket_addon-trusted-logo-head.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-internet-defense', plugin_dir_url( __FILE__ ) . 'js/unitix_ticket_addon-internet-defense.js', array( 'jquery' ), $this->version, true );
		#wp_enqueue_script( 'bootstrap', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), '4.1.4', false );

		// load scripts for specific pages
		if(function_exists('is_cart'))
		{
			if (is_cart() )   // WooCommerce - Cart
			{
				// wp_enqueue_script(   $this->plugin_name . '-min-max-quanitity', plugin_dir_url( __FILE__ ) . 'js/unitix_ticket_addon-min-max-quantity.js', array( 'jquery' ), $this->version, false );
			}
			elseif ($this->is_tribe_event_page())				 // Tribe Event Page
			{
				// wp_enqueue_script( $this->plugin_name . '-min-max-quanitity-events', plugin_dir_url( __FILE__ ) . 'js/unitix_ticket_addon-min-max-quantity-events.js', array( 'jquery' ), $this->version, false );
			}
		}
	}

	/**
	 * Check if we are on a Tribe Events page
	 *
	 * @since					1.0.0
	 * @access    		public
	 * @dependencies	tribe_events
	 */
	public function is_tribe_event_page()
	{

		if (tribe_is_event() || tribe_is_event_category() || tribe_is_in_main_loop() || tribe_is_view() || 'tribe_events' == get_post_type() || is_singular( 'tribe_events' ))
		{
			return true;
			exit;
		}
		return false;
	}

	/**
	* Get the current Year
	* Shortcode function
	**/
	public static function getCopyrightYear ()
	{
		return (string) date("Y");
	}

	/**
	* Add Shortcodes
	**/
	private function addShortcode ($name,$func)
	{
		add_shortcode($name, $func);
		return $this;
	}

	/**
	 *
	 *
	 */
	private function disableAdminBar()
	{
		show_admin_bar(false);
		return $this;
	}
}