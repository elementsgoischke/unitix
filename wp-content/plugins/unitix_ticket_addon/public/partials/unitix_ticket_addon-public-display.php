<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.agentur-elements.com
 * @since      1.0.0
 *
 * @package    Unitix_ticket_addon
 * @subpackage Unitix_ticket_addon/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->