<?php
 /**
  * Dependencies
  */
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'widgets/class-unitix_ticket_addon-widget-comodo-logo.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'widgets/class-unitix_ticket_addon-widget-internet-defense.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'widgets/class-unitix_ticket_addon-widget-brave-ref.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'widgets/class-unitix_ticket_addon-widget-brave.php';
/***********************************************/
function unitix_register_widgets()
{
  register_widget( 'unitix_widget_comodo_logo' );
  register_widget( 'unitix_widget_internet_defense' );
  register_widget( 'unitix_widget_brave_ref' );
  register_widget( 'unitix_widget_brave' );
}
add_action( 'widgets_init', 'unitix_register_widgets' );