<?php
/**
 *
 *
 */
class unitix_widget_comodo_logo extends WP_Widget
{
   /**
    *
    */
   function __construct()
   {
     parent::__construct(
       // widget ID
       'unitix_widget_comodo_logo',
       // widget name
       __('unitix Widget - Comodo Logo', ' unitix_widget_domain'),
       // widget description
       array( 'description' => __( 'unitix Widget - Comodo Logo', 'unitix_widget_domain' ) )
     );
   }

    /**
     *
     */
    public function widget( $args, $instance )
    {
      // $title = apply_filters( 'widget_title', $instance['title'] );
      echo $args['before_widget'];
      // //if title is present
      // if ( ! empty( $title ) )
      // echo $args['before_title'] . $title . $args['after_title'];

     //output
     // echo __( 'unitix.net', 'unitix_widget_domain' );
     ?>
     <div style="margin: 15px 0px">
       <script language="JavaScript" type="text/javascript">
        TrustLogo("https://www.unitix.net/wp-content/uploads/2018/11/comodo_secure_seal_113x59_transp.png", "CL1", "none");
       </script>
    </div>
     <?php
     echo $args['after_widget'];
   }

   /**
    *
    */
   public function form( $instance )
   {
     ?>
     <p>
       <label>Comodo Logo Image</label>
     </p>
     <?php
   }

   /**
    *
    */
   public function update( $new_instance, $old_instance )
   {
     $instance = array();
     $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
     return $instance;
   }

   /**
    *
    */
   public function formBKP( $instance )
   {
     if ( isset( $instance[ 'title' ] ) )
     $title = $instance[ 'title' ];
     else
     $title = __( 'Default Title', 'unitix_widget_domain' );
     ?>
     <p>
     <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
     <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
     </p>
     <?php
   }


}