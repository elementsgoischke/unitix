=== The Events Calendar: Community Tickets ===

Contributors: ModernTribe, borkweb, aguseo, barry.hughes, bordoni, brianjessee, cliffpaulick, courane01, faction23, GeoffBel, geoffgraham, ggwicz, jbrinley, jentheo, leahkoerper, lucatume, mastromktg, MZAWeb, neillmcshea, nicosantos, patriciahillebrandt, peterchester, reid.peifer, roblagatta, ryancurban, shane.pearlman, tribecari, vicskf, zbtirrell
Tags: widget, events, simple, tooltips, grid, month, list, calendar, event, venue, community, registration, api, dates, date, plugin, posts, sidebar, template, theme, time, google maps, google, maps, conference, workshop, concert, meeting, seminar, summit, forum, shortcode, The Events Calendar, The Events Calendar PRO, Community Events, tickets, RSVP, registration
Requires at least: 4.5
Stable tag: 4.5.6
Tested up to: 4.9.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Community Tickets is an add-on for The Events Calendar, Community Events, Event Tickets, and Tickets Plus. Users submitting events via Community Events can now add tickets to their events, and the site admin can collect fees from ticket sales.

== Description ==

= The Events Calendar: Community Tickets =

* Frontend user ticket creation for events
* Users can create free or paid tickets
* Admin may choose to collect flat fee or percentage from ticket sales
* Admin and event organizers can view sales and attendee reports
* Admin can use PayPal split payments or collect all the funds and distribute themselves

Note: you'll need to have the latest versions of <a href="http://m.tri.be/3j">The Events Calendar</a>, <a href=“http://m.tri.be/18ks”>Community Events</a>, <a href=“https://wordpress.org/plugins/event-tickets/”>Event Tickets</a>, and <a href="http://m.tri.be/18nq">Tickets Plus</a> installed for this plugin to function.

== Installation ==

1. From the dashboard of your site, navigate to Plugins --> Add New.
2. Select the Upload option and hit "Choose File."
3. When the popup appears select the the-events-calendar-community-tickets.x.x.zip file from your desktop. (The 'x.x' will change depending on the current version number).
4. Follow the on-screen instructions and wait as the upload completes.
5. When it's finished, activate the plugin via the prompt. A message will show confirming activation was successful.
6. For access to new updates, make sure you have added your valid License Key under Events --> Settings --> Licenses.

= Requirements =

* PHP 5.2.4 or greater (recommended: PHP 7.0 or greater)
* WordPress 4.5 or above
* jQuery 1.11.x
* Event Tickets 4.6 or above
* Event Tickets Plus 4.6 or above
* The Events Calendar 4.6 or above
* Community Events 4.5 or above

== Documentation ==

Community Tickets extends the functionality of Modern Tribe's The Events Calendar (http://m.tri.be/3j), Community Events, Event Tickets, and Tickets Plus to allow for frontend event submission with tickets on your WordPress site.

All of our online documentation can be found on <a href="http://m.tri.be/eu">our documentation site</a>.

Some docs you may find particularly useful are:

* <a href="http://m.tri.be/18l-">New User Primer: Community Tickets</a>
* <a href="http://m.tri.be/18m1">Configuring Community Tickets</a>
* <a href="http://m.tri.be/18m0">Community Tickets: Tracking sales & getting paid</a>

Tutorials are available at http://m.tri.be/44

== Frequently Asked Questions ==

= Where do I go to file a bug or ask a question? =

Please visit the forum for questions or comments: http://m.tri.be/18ku

= Are there any troubleshooting steps you'd suggest I try that might resolve my issue before I post a new thread? =

First, make sure that you're running the latest version of The Events Calendar, Community Tickets, Event Tickets, and Tickets Plus. If you've got any other add-ons, make sure those are current / running the latest code as well.

The most common issues we see are either plugin or theme conflicts. You can test if a plugin or theme is conflicting by manually deactivating other plugins until just The Events Calendar is running on your site. If the issue persists from there, revert to the default Twenty Fifteen theme. If the issue is resolved after deactivating a specific plugin or your theme, you'll know that is the source of the conflict.

Note that we aren't going to say "tough luck" if you identify a plugin/theme conflict. While we can't guarantee 100% integration with any plugin or theme out there, we will do our best (and reach out the plugin/theme author as needed) to figure out a solution that benefits everyone.

= I'm still stuck. Where do I go to file a bug or ask a question? =

If you're a Community Tickets user, you're entitled access to our actively-monitored <a href="http://m.tri.be/3x">forums</a> at the theeventscalendar.com website. We can provide a deeper level of support here and hit these forums on a daily basis during the work week. You will receive a reply within 24-48 hours (during the week).

== Contributors ==

The plugin is produced by <a href="http://m.tri.be/3i">Modern Tribe Inc</a>.

= Current Contributors =

* <a href="https://profiles.wordpress.org/users/barryhughes">Barry Hughes</a>
* <a href="https://profiles.wordpress.org/users/brianjessee">Brian Jessee</a>
* <a href="https://profiles.wordpress.org/users/geoffgraham">Geoff Graham</a>
* <a href="https://profiles.wordpress.org/users/ggwicz">George Gecewicz</a>
* <a href="https://profiles.wordpress.org/users/bordoni">Gustavo Bordoni</a>
* <a href="https://profiles.wordpress.org/users/jbrinley">Jonathan Brinley</a>
* <a href="https://profiles.wordpress.org/users/leahkoerper">Leah Koerper</a>
* <a href="https://profiles.wordpress.org/users/lucatume">Luca Tumedei</a>
* <a href="https://profiles.wordpress.org/users/borkweb">Matthew Batchelder</a>
* <a href="https://profiles.wordpress.org/users/neillmcshea">Neill McShea</a>
* <a href="https://profiles.wordpress.org/users/mastromktg">Nick Mastromattei</a>
* <a href="https://profiles.wordpress.org/users/nicosantos">Nico Santo</a>
* <a href="https://profiles.wordpress.org/users/peterchester">Peter Chester</a>
* <a href="https://profiles.wordpress.org/users/roblagatta">Rob La Gatta</a>
* <a href="https://profiles.wordpress.org/users/reid.peifer">Reid Peifer</a>
* <a href="https://profiles.wordpress.org/users/ryancurban">Ryan Urban</a>
* <a href="https://profiles.wordpress.org/users/faction23">Samuel Estok</a>
* <a href="https://profiles.wordpress.org/users/shane.pearlman">Shane Pearlman</a>
* <a href="https://profiles.wordpress.org/users/zbtirrell">Zachary Tirrell</a>

= Past Contributors =

* <a href="https://profiles.wordpress.org/users/brook-tribe">Brook Harding</a>
* <a href="https://profiles.wordpress.org/users/caseypatrickdriscoll">Casey Driscoll</a>
* <a href="https://profiles.wordpress.org/users/ckpicker">Casey Picker</a>
* <a href="https://profiles.wordpress.org/users/dancameron">Dan Cameron</a>
* <a href="https://profiles.wordpress.org/users/MZAWeb">Daniel Dvorkin</a>
* <a href="https://profiles.wordpress.org/users/jazbek">Jessica Yazbek</a>
* <a href="https://profiles.wordpress.org/users/jkudish">Joachim Kudish</a>
* <a href="https://profiles.wordpress.org/users/jgadbois">John Gadbois</a>
* <a href="https://profiles.wordpress.org/users/jonahcoyote">Jonah West</a>
* <a href="https://profiles.wordpress.org/users/joshlimecuda">Josh Mallard</a>
* <a href="https://profiles.wordpress.org/justinendler/">Justin Endler</a>
* <a href="https://profiles.wordpress.org/users/kellykathryn">Kelly Groves</a>
* <a href="https://profiles.wordpress.org/users/kelseydamas">Kelsey Damas</a>
* <a href="https://profiles.wordpress.org/users/kyleunzicker">Kyle Unzicker</a>
* <a href="https://profiles.wordpress.org/users/mat-lipe">Mat Lipe</a>
* <a href="https://profiles.wordpress.org/users/mdbitz">Matthew Denton</a>
* <a href="https://profiles.wordpress.org/users/mattwiebe">Matt Wiebe</a>
* <a href="https://profiles.wordpress.org/users/nickciske">Nick Ciske</a>
* <a href="https://profiles.wordpress.org/users/paulhughes01">Paul Hughes</a>
* <a href="https://profiles.wordpress.org/users/codearachnid">Timothy Wood</a>
* <a href="https://profiles.wordpress.org/users/thatdudebutch">Wayne Stratton</a>

= Translations =

Modern Tribe’s premium plugins are translated by volunteers at <a href=“http://m.tri.be/194h”>translations.theeventscalendar.com</a>. There you can find a list of available languages, download translation files, or help update the translations. Thank you to everyone who helps to maintain our translations!

== Add-Ons ==

But wait: there's more! We've got a whole stable of plugins available to help you be awesome at what you do. Check out a full list of the products below, and over at <a href="http://m.tri.be/3k">The Events Calendar website.</a>

Our Free Plugins:

* <a href="https://wordpress.org/plugins/the-events-calendar/" target="_blank">The Events Calendar</a>
* <a href="https://theeventscalendar.com/product/wordpress-event-tickets/" target="_blank">Event Tickets</a>
* <a href="http://wordpress.org/extend/plugins/advanced-post-manager/" target="_blank">Advanced Post Manager</a>

Our Premium Plugins:

* <a href="http://m.tri.be/2c" target="_blank">Events Calendar PRO</a>
* <a href="http://m.tri.be/18nq" target="_blank">Event Tickets Plus</a>
* <a href="http://m.tri.be/2e" target="_blank">The Events Calendar: Eventbrite Tickets</a>
* <a href="http://m.tri.be/2g" target="_blank">The Events Calendar: Community Events</a>
* <a href="http://m.tri.be/18m2" target="_blank">The Events Calendar: Community Tickets</a>
* <a href="http://m.tri.be/fa" target="_blank">The Events Calendar: Filter Bar</a>

== Changelog ==

= [4.5.6] 2018-08-22 =

* Tweak - Remove references to purchase limit options [105775]
* Deprecated - `get_purchase_limit()` on `Tribe__Events__Community__Tickets__Main`
* Deprecated - `enforce_purchase_limit_on_add()` on `Tribe__Events__Community__Tickets__Main`
* Deprecated - `enforce_purchase_limit_on_update()` on `Tribe__Events__Community__Tickets__Main`
* Deprecated - `set_purchase_limit()` on `Tribe__Events__Community__Tickets__Main`
* Deprecated - `maybe_display_purchase_limit()` on `Tribe__Events__Community__Tickets__Main`


= [4.5.5] 2018-08-01 =

* Tweak - Manage plugin assets via `tribe_assets()` [40267]

= [4.5.4] 2018-03-28 =

* Feature - Added updater class to enable changes on future updates [84675]
* Fix - Updated asset code to avoid loading styles and scripts on the frontend unnecessarily [101285]
* Fix - Added checks to prevent a PHP warning when `fee_percentage` is calculated [99976]
* Fix - Utilize new filters in Event Tickets Plus and The Events Calendar to modify venue, sales, attendee, and order report links to keep users on the front end (thanks to Gurdeep for highlighting this) [93923]
* Tweak - Removed references to deprecated function `events_get_events_link` in favor of `tribe_get_events_link` [99980]
* Tweak - Updated text to clarify intentions on labels [96522]

= [4.5.3] 2017-12-07 =

* Fix - Removed backend eCommerce links for all users on front end ticket form [93911]
* Fix - Fixed some layout issues with the front-end "Order Report" view [93339]
* Fix - Fixed some layout issues with the front-end "Attendees Report" view [93338]
* Fix - Made some changes to ensure the "edit" action links on front-end reports is only visible if users are allowed to edit submissions [93339]
* Tweak - Added CSS for new ticket alerts [91215]
* Tweak - Fixed label CSS color in Twenty Seventeen theme [91218]
* Tweak - Decreased the width of the ticket capacity fields so they're sized more proportionally to the values they hold [94728]
* Tweak - Prevent EDD tickets from being created on Community Tickets front-end, since only WooCommerce is supported for this feature [91758]
* Language - 2 new strings added, 13 updated, 0 fuzzied, and 0 obsoleted

= [4.5.2] 2017-11-21 =

* Tweak - Fix New Ticket and New RSVP alignment [92555]
* Tweak - Only display admin links in Community Tickets if user is able to access the admin [79565]
* Tweak - Changed views: `community-tickets/modules/tickets`
* Language - 0 new strings added, 5 updated, 0 fuzzied, and 0 obsoleted

= [4.5.1] 2017-11-16 =

* Fix - Adjusted the version check logic used when testing if suitable versions of required plugins are available [93298]
* Language - 0 new strings added, 8 updated, 0 fuzzied, and 0 obsoleted

= [4.5] 2017-11-09 =

* New - Ticket editor redesign from Event Tickets Plus 4.6
* Tweak - Compatibility modifications to CSS and JavaScript to use new Tickets Editor
* Language - 0 new strings added, 13 updated, 0 fuzzied, and 0 obsoleted

= [4.4.5] 2017-07-26 =

* Tweak - Hide the Split Payments payment option (which only works with Ticket products) in carts that have no tickets. [74528]

= [4.4.4] 2017-07-13 =

* Fix - Added notice when Woo, Community, or Event Tickets is not active [70357]

= [4.4.3] 2017-06-06 =

* Tweak - Compatibility with version 4.5 of Community Events
* Tweak - Added actions: `tribe_events_community_section_before_tickets`, `tribe_events_community_section_after_tickets`
* Tweak - Removed actions: `tribe_events_community_after_the_tickets`
* Language - 0 new strings added, 14 updated, 0 fuzzied, and 0 obsoleted [events-community-tickets]

= [4.4.2] 2017-05-17 =

* Tweak - further adjustments made to our plugin licensing system [78506]

= [4.4.1] 2017-05-03 =

* Tweak - adjustments made to our plugin licensing system

= [4.4] 2017-01-09 =

* Tweak - Brings the plugin up-to-date with changes made in Event Tickets [67176, 70431]
* Tweak - Brings the plugin up-to-date with changes made in Community Events [69762]
* Tweak - Transitioned the dropdown selectors to the same system used in the parent plugins [69353]
* Tweak - Text and translation fixes

= [4.3.2] 2016-12-20 =

* Tweak - Updated the template override instructions in a number of templates [68229]

= [4.3.1] 2016-10-20 =

* Tweak - Added plugin file path constant
* Tweak - Registered plugin as active with Tribe Common [66657]

= [4.3] 2016-10-13 =

* Tweak - Updated to be 4.3 compatible

= [4.2.2] 2016-07-06 =

* Security - Tightened security around event reports
* Tweak - re-label "PayPal client ID" setting to be "PayPal Application ID" [45120]
* Tweak - improved integration with Woo in the custom PayPal split payment gateway [44807]

= [4.2.1] 2016-06-22 =

* Tweak - Fixed spacing on Fee Option Defaults tooltip

= [4.2] 2016-06-08 =

* Tweak - Language files in the `wp-content/languages/plugins` path will be loaded before attempting to load internal language files (Thank you to user @aafhhl for bringing this to our attention!)
* Tweak - Move plugin CSS to PostCSS

= [4.1.1] 2016-05-19 =

* Fix - Default to preventing organizers from deleting tickets when sales have been made

= [4.1] 2016-03-15 =

* Fix - Resolved issue where the "Email Attendees" modal box didn't open in a modal box (mad fist bump to @xlr8r for the heads up!)

= [4.0.4] 2016-03-02 =

* Fix - PayPal payments are fully working again with split payments checkout (HTTP 1.1)

= [4.0.3] 2016-02-17 =

* Fix - Prevents Community Tickets to output HTML on the CSV exporting tool

= [4.0.2] 2015-12-16 =

* Fix - Resolved fatals introduced by relocated files in WP 4.4 (Thank you Dirk for reporting this!)
* Fix - Resolved textdomain issue that caused translations to fail

= [4.0.1] 2015-12-10 =

* Fix - Fixed issue where non-ticket products were removed from the cart when the "Update Cart" button was pressed
* Fix - Resolved bug where site fees were being calculated on the entire cart regardless of whether or not all of the items in the cart were tickets

= [4.0] 2015-12-02 =

* Fix - Compatibility fix with PHP 5.2 when accessing the WooCommerce cart

= [3.12.1] 2015-11-12 =

* Fix - Ensure translations are loaded as expected (our thanks to Dirk for highlighting this issue)
* Fix - Update some strings to ensure they can be translated as expected

= [3.12] 2015-11-04 =

* Initial release
