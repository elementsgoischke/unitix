<?php
/**
 * Portfolio single template
 *
 * @package Unitix
 * @subpackage Frontend
 * @since 1.3
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */
global $post;
$permalink = urlencode(get_permalink($post->ID));
$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail');
$featured_image = $featured_image['0'];
$post_title = rawurlencode(get_the_title($post->ID)); ?>

<div class="portfolio-share overlay">
    <span class="overlay"><?php
        _e( 'Share', 'unitix-td' ); ?>
    </span>
    <ul class="list-inline">
        <li>
            <a href="https://twitter.com/share?url=<?php echo esc_url($permalink); ?>&amp;text=<?php echo esc_attr($post_title); ?>" target="_blank" data-toggle="tooltip" title="<?php _e( 'Share on Twitter', 'unitix-td' ); ?>">
                <i class="fa fa-twitter"></i>
            </a>
        </li>
        <li>
            <a href="http://www.facebook.com/sharer.php?u=<?php echo esc_url($permalink); ?>" target="_blank" data-toggle="tooltip" title="<?php _e( 'Share on Facebook', 'unitix-td' ); ?>">
                <i class="fa fa-facebook"></i>
            </a>
        </li>
        <li>
            <a href="//pinterest.com/pin/create/button/?url=<?php echo esc_url($permalink); ?>&amp;media=<?php echo esc_attr($featured_image); ?>&amp;description=<?php echo esc_attr($post_title); ?>" target="_blank" data-toggle="tooltip" title="<?php _e( 'Pin on Pinterest', 'unitix-td' ); ?>">
                <i class="fa fa-pinterest"></i>
            </a>
        </li>
        <li>
            <a href="https://plus.google.com/share?url=<?php echo esc_url($permalink); ?>" target="_blank" data-toggle="tooltip" title="<?php _e( 'Share on Google+', 'unitix-td' ); ?>">
                <i class="fa fa-google-plus"></i>
            </a>
        </li>
    </ul>
</div>
