<div class="list-fancy-icons">
    <div class="list-item">
        <?php if( $show_icon === 'true') : ?>
            <?php if( $icon !== '') : ?>
                <div class="<?php echo esc_attr($shape); ?>">
                    <i class="fa fa-<?php echo esc_attr($icon); ?>" data-animation="<?php echo esc_attr($animation); ?>"></i>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php
        if( !empty( $title ) ) : ?>
            <h3><?php echo esc_html($title); ?></h3>
        <?php
        endif; ?>
        <?php
        if( !empty( $content ) ) : ?>
            <p><?php echo wp_kses_post($content); ?></p>
        <?php
        endif; ?>
    </div>
</div>
