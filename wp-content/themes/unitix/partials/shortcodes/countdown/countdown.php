<h1 class="countdown <?php echo implode(' ', $classes); ?>" data-date="<?php echo esc_attr($date); ?>">
    <div class="counter-element">
        <span class="counter-days odometer">
            0
        </span>
        <b>
            <?php esc_html_e('days', 'unitix-td'); ?>
        </b>
    </div>
    <div class="counter-element">
        <span class="counter-hours odometer">
            0
        </span>
        <b>
            <?php esc_html_e('hours', 'unitix-td'); ?>
        </b>
    </div>
    <div class="counter-element">
        <span class="counter-minutes odometer">
            0
        </span>
        <b>
            <?php esc_html_e('minutes', 'unitix-td'); ?>
        </b>
    </div>
    <div class="counter-element">
        <span class="counter-seconds odometer">
            0
        </span>
        <b>
            <?php esc_html_e('seconds', 'unitix-td'); ?>
        </b>
    </div>
</h1>