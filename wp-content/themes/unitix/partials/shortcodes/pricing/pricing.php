<div class="<?php echo esc_attr(implode( ' ', $classes )); ?>">
    <?php
    if( $heading !== "" ) : ?>
    <h2 class="pricing-head"><?php echo esc_html($heading); ?></h2>
    <?php
    endif; ?>
    <div class="pricing-body">
        <?php
        if( $show_price === 'true' ) : ?>
        <div class="pricing-price">
            <div class="overlay">
                <h4>
                    <?php
                    if( $currency === 'custom' ) : ?>
                    <small><?php echo esc_html($custom_currency); ?></small>
                    <?php
                    else : ?>
                    <small><?php echo esc_html($currency); ?></small>
                    <?php
                    endif; ?>
                    <?php echo esc_html($price); ?>
                    <small><?php echo esc_html($per); ?></small>
                </h4>
            </div>
        </div>
        <?php
        endif; ?>
        <ul class="pricing-list">
        <?php
        foreach( $list as $item ) : ?>
            <li><?php echo esc_html($item); ?></li>
        <?php
        endforeach; ?>
        </ul>
        <?php
        if( $show_button === 'true' ) : ?>
        <a href="<?php echo esc_url($button_link); ?>" class="btn btn-lg btn-primary"><?php echo esc_html($button_text); ?></a>
        <?php
        endif; ?>
    </div>
</div>