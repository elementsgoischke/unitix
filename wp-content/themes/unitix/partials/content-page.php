<?php

/**

 * Displays a single post

 *

 * @package Unitix

 * @subpackage Frontend

 * @since 0.1

 *

 * @copyright (c) 2014 Oxygenna.com

 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/

 * @version 1.18.7

 */

?>

<div class="container">

    <div class="row">

        <div class="col-xs-12">

        <article id="post-<?php the_ID();?>"  <?php post_class(); ?>>

            <?php the_content( '', false ); ?>

            <?php oxy_atom_author_meta(); ?>

        </article>

        </div>

    </div>

</div>