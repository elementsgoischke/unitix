<?php
/**
 * Shows a woocommerce account page
 *
 * @package Unitix
 * @subpackage Frontend
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

global $woocommerce;
$shop_decoration = oxy_get_option('woocom_general_decoration');
?>
<section class="section section-commerce section-short <?php echo apply_filters( 'oxy_woocommerce_shop_classes', 10 );?> has-top">
    <?php echo oxy_section_decoration( 'top', $shop_decoration ); ?>
    <div class="container">
        <?php wc_print_notices(); ?>
        <div class="row">
        <?php if( is_user_logged_in() ) : ?>
            <div class="col-md-3">
                <?php get_template_part('woocommerce/myaccount/navigation'); ?>
            </div>
            <div class="col-md-9">
                <?php the_content(); ?>
            </div>
        <?php else : ?>
            <div class="col-md-12">
                <?php the_content(); ?>
            </div>
        <?php endif; ?>
        </div>
    </div>
</section>
