<?php
/**
 * Displays a category list
 *
 * @package Unitix
 * @subpackage Frontend
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

get_header();
$search_result_decoration = oxy_get_option('search_result_decoration');
oxy_blog_header( esc_html__('Results for', 'unitix-td'), '<small>' . get_search_query()  . '</small>' );
?>
<section class="section add-bg <?php echo oxy_get_option('search_result_swatch'); ?> has-top">
    <?php echo oxy_section_decoration( 'top', $search_result_decoration ); ?>
    <div class="container">
        <div class="row">
            <?php get_template_part( 'partials/loop' ); ?>
        </div>
    </div>
</section>
<?php get_footer();