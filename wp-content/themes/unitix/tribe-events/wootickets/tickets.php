<?php
/**
 * Renders the WooCommerce tickets table/form
 *
 * @author Agentur Elements - Jan Goischke
 * @version 1.1.0
 *
 * @var bool $global_stock_enabled
 * @var bool $must_login
 */
global $woocommerce;
$is_there_any_product = false;
$is_there_any_product_to_sell = false;
$unavailability_messaging = is_callable(array($this, 'do_not_show_tickets_unavailable_message'));

if (!empty($tickets)) 
{
	$tickets = tribe('tickets.handler')->sort_tickets_by_menu_order($tickets);
}

ob_start();

/**
 * Filter classes on the Cart Form
 */
$cart_classes = (array) apply_filters('tribe_events_tickets_woo_cart_class', array('cart'));
?>
<form id="buy-tickets" action="<?php echo esc_url( wc_get_cart_url() ) ?>" class="<?php echo esc_attr( implode( ' ', $cart_classes ) ); ?> " method="post" enctype="multipart/form-data" novalidate> 	
	<h2><?php esc_html_e( 'Tickets', 'event-tickets-plus' ) ?></h2><br />	
	<div>
	<?php
	/**
	 * Reorder the tickets per the admin interface order
	 */
	foreach($tickets as $ticket):
		/**
		 * @var Tribe__Tickets__Ticket_Object $ticket
		 * @var WC_Product $product
		 */
		global $product;
		if(class_exists('WC_Product_Simple')){
			$product = new WC_Product_Simple($ticket->ID);
		} else {
			$product = new WC_Product($ticket->ID);
		}
		$is_there_any_product = true;
		$data_product_id      = '';
		if($ticket->date_in_range()) 
		{
				$is_there_any_product = true;
				echo sprintf( '<input type="hidden" name="product_id[]" value="%d">', esc_attr( $ticket->ID ) );
				/**
				 * Filter classes on the Price column
				 *
				 * @since  4.3.2
				 *
				 * @param array $column_classes
				 * @param int $ticket->ID
				 */
				// $column_classes = (array) apply_filters( 'tribe_events_tickets_woo_quantity_column_class', array( 'woocommerce' ), $ticket->ID );
				// Max quantity will be left open if backorders allowed, restricted to 1 if the product is
				// constrained to be sold individually or else set to the available stock quantity
				// $max_quantity = $product->backorders_allowed() ? '' : $product->get_stock_quantity();
				// $max_quantity = $product->is_sold_individually() ? 0 : $max_quantity;
				// $available    = $ticket->available();
				/**
				 * Filter classes on the row
				 *
				 * @since  4.5.5
				 *
				 * @param array $row_classes
				 * @param int $ticket->ID
				 */
				// $row_classes = (array) apply_filters( 'tribe_events_tickets_row_class', array( 'woocommerce', 'tribe-tickets-form-row' ), $ticket->ID );
				// echo '<tr class="' . esc_attr( implode( ' ', $row_classes ) ) . '" data-product-id="' . esc_attr( $ticket->ID ) . '">';
				// echo '<div class="" data-product-id="' . esc_attr( $ticket->ID ) . '">';
				/**
				 * Filter classes on the Price column
				 *
				 * @since  4.3.2
				 *
				 * @param array $column_classes
				 */
				// $column_classes = (array) apply_filters( 'tribe_events_tickets_woo_quantity_column_class', array( 'woocommerce' ) );
				// echo '<td class="" data-product-id="' . esc_attr( $ticket->ID ) . '">';
				echo '<h4 class="tickets_name">' . $ticket->name . '</h4>';
				echo '<div class="row"><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" data-product-id="' . esc_attr( $ticket->ID ) . '">';
							if ( 0 !== $available ) {
							// Max quantity will be left open if backorders allowed, restricted to 1 if the product is
							// constrained to be sold individually or else set to the available stock quantity
							$max_quantity = $ticket->stock();
							$max_quantity = $product->backorders_allowed() ? '' : $stock;
							$max_quantity = $product->is_sold_individually() ? 1 : $max_quantity;
							$available    = $ticket->available();
							if($max_quantity > 25)
							{
								$max_quantity = 25;
							}
							$input = woocommerce_quantity_input( array(
								'input_name'  => 'quantity_' . $ticket->ID,
								'input_value' => 0,
								'min_value'   => 0,
								'max_value'   => $max_quantity,
							), null, false );
							$is_there_any_product_to_sell = true;
							$disabled_attr = disabled( $must_login, true, false );
							$input = str_replace( '<input  type="number"', '<input type="number"' . $disabled_attr, $input );
				echo $input;
							do_action( 'wootickets_tickets_after_quantity_input', $ticket, $product );

				echo '</div>';
				
				echo '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tickets_price">';
				echo '<div class="tix-info">';

									if ( $available )
									{
						echo '<span class="tribe-tickets-remaining">';
									$readable_amount = tribe_tickets_get_readable_amount( $available, null, false );
						echo sprintf( esc_html__( '%1$s available', 'event-tickets-plus' ),
						'<span class="available-stock" data-product-id="' . esc_attr( $ticket->ID ) . '">' . esc_html( $readable_amount ) . '</span>'
						);
						echo '</span>';
									}
									}
									else
									{
						echo '<span class="tickets_nostock">' . esc_html__( 'Out of stock!', 'event-tickets-plus' ) . '</span>';
									}

						echo '<div class="tix-price">';
							if ( method_exists( $product, 'get_price' ) && $product->get_price() ) {
						echo $this->get_price_html( $product );
							} else {
						esc_html_e( 'Free', 'event-tickets-plus' );
							}
						echo '</div>';
				echo '<div class="tix-desc">' . ( $ticket->show_description() ? $ticket->description : '' ) . '</div>';
				echo '</div>';
				echo '</div>';
				echo '</div>';
				echo '<br style="clear:both" clear="all" />';
							
			}
    		endforeach; ?>		
    <?php if ( $is_there_any_product_to_sell ) : ?>			
    <tr>				
      <td colspan="4" class="woocommerce add-to-cart">					
        <?php if ( $must_login ) : ?>						
        <?php include Tribe__Tickets_Plus__Main::instance()->get_template_hierarchy( 'login-to-purchase' ); ?>					
        <?php else: ?>						
        <button  							type="submit"  							name="wootickets_process"  							value="1"    						>							
          <?php esc_html_e( 'Add to cart', 'event-tickets-plus' );?>						
        </button>					
        <?php endif; ?>				</td>			
	</tr>	
	
	
    <?php endif; ?>		
    <noscript>				
		<div class="no-javascript-msg">
		<?php esc_html_e( 'You must have JavaScript activated to purchase tickets. Please enable JavaScript in your browser.', 'event-tickets-plus' ); ?>
		</div>				
    </noscript>	
</div>
</form>
<?php
$content = ob_get_clean();
if ( $is_there_any_product_to_sell ) {
	echo $content;
	// @todo remove safeguard in 4.3 or later
	if ( $unavailability_messaging ) {
		// If we have rendered tickets there is generally no need to display a 'tickets unavailable' message
		// for this post
		$this->do_not_show_tickets_unavailable_message();
	}
} else {
	// @todo remove safeguard in 4.3 or later
	if ( $unavailability_messaging ) {
		$unavailability_message = $this->get_tickets_unavailable_message( $tickets );
		echo $unavailability_message;
		// if there isn't an unavailability message, bail
		if ( ! $unavailability_message ) {
			return;
		}
	}
}