<?php
/**
 * Main functions file
 *
 * @package Unitix
 * @subpackage Frontend
 * @since 0.1
 */

// create defines
define( 'THEME_NAME', 'Unitix' );
define( 'THEME_SHORT', 'unitix' );

define( 'OXY_THEME_DIR', get_template_directory() . '/' );
define( 'OXY_THEME_URI', get_template_directory_uri() . '/' );

// include extra theme specific code
include OXY_THEME_DIR . 'inc/frontend.php';
include OXY_THEME_DIR . 'inc/woocommerce.php';
include OXY_THEME_DIR . 'vendor/oxygenna/oxygenna-framework/inc/OxygennaTheme.php';
include OXY_THEME_DIR . 'vendor/oxygenna/oxygenna-mega-menu/oxygenna-mega-menu.php';

// create theme

global $oxy_theme;
$oxy_theme = new OxygennaTheme(
    array(
        'text_domain'       => 'unitix-td',
        'admin_text_domain' => 'unitix-admin-td',
        'min_wp_ver'        => '3.4',
        'sidebars' => array(
            'sidebar' => array( 'Sidebar', '' ),
        ),
        'widgets' => array(
            'Swatch_twitter'                => 'swatch_twitter.php',
            'Swatch_social'                 => 'swatch_social.php',
            'Swatch_wpml_language_selector' => 'swatch_wpml_language_selector.php',
        ),
        'shortcodes' => false
    )
);

include OXY_THEME_DIR . 'inc/custom-posts.php';
include OXY_THEME_DIR . 'inc/options/shortcodes/shortcodes.php';
include OXY_THEME_DIR . 'inc/options/widgets/default_overrides.php';

if( is_admin() ) {
    include OXY_THEME_DIR . 'inc/options/shortcodes/create-shortcode-options.php';
    include OXY_THEME_DIR . 'inc/backend.php';
    include OXY_THEME_DIR . 'inc/theme-metaboxes.php';
    include OXY_THEME_DIR . 'inc/visual-composer-extend.php';
    include OXY_THEME_DIR . 'inc/visual-composer.php';
    // include OXY_THEME_DIR . 'inc/install-plugins.php';
    // include OXY_THEME_DIR . 'inc/one-click-import.php';
    // include OXY_THEME_DIR . 'vendor/oxygenna/oxygenna-one-click/inc/OxygennaOneClick.php';
    include OXY_THEME_DIR . 'vendor/oxygenna/oxygenna-typography/oxygenna-typography.php';
}

function oxy_activate_theme( $theme ) {
    // if no swatches are installed then install the default swatches
    // remove old default swatches
    $swatches = get_posts( array(
        'post_type'      => 'oxy_swatch',
        'meta_key'       => THEME_SHORT . '_default_swatch',
        'posts_per_page' => '-1'
    ));
    if( empty( $swatches ) ) {
        update_option( THEME_SHORT . '_install_swatches', true );
    }
}
add_action( 'after_switch_theme', 'oxy_activate_theme' );

/********************************************************************/
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '32M');

function cart_update_qty_script() 
{
    if (is_cart())
    {
        echo"<script>
            jQuery('div.woocommerce').on('change', '.qty', function()
            {
                jQuery('[name='update_cart']').removeAttr('disabled');
                jQuery('[name='update_cart']').trigger('click');
            });
        </script>";
    }
}
add_action( 'wp_footer', 'cart_update_qty_script' );

// function tribe_prevent_ajax_paging() {
// 	echo "
// 	<script>
// 	jQuery(document).ready(function(){
// 		// Tribe Bar
// 		jQuery( 'form#tribe-bar-form' ).off( 'submit' );
// 		jQuery( '#tribe-events' )
// 			.off( 'click', '.tribe-events-nav-previous, .tribe-events-nav-next' ) // Month and Week View
// 			.off( 'click', '.tribe-events-nav-previous a, .tribe-events-nav-next a') // Day View
// 			.off( 'click', '#tribe-events-footer .tribe-events-nav-previous a, #tribe-events-footer .tribe-events-nav-next a' ); // Day View
// 		// Month View
// 		jQuery( 'body' ).off( 'click', '#tribe-events-footer .tribe-events-nav-previous, #tribe-events-footer .tribe-events-nav-next' );
// 		// List View
// 		jQuery( '#tribe-events-content-wrapper' )
// 			.off( 'click', 'ul.tribe-events-sub-nav a[rel=\"next\"]' )
// 			.off( 'click', 'ul.tribe-events-sub-nav a[rel=\"prev\"]' )
// 			.off( 'click', '#tribe-events-footer .tribe-events-nav-previous a, #tribe-events-footer .tribe-events-nav-next a' );
// 		/* This breaks map and Photo view as of v4.0, because their nonJS href='#'
// 		// Map and Photo View
// 		jQuery( '#tribe-events' )
// 			.off( 'click', 'li.tribe-events-nav-next a')
// 			.off( 'click', 'li.tribe-events-nav-previous a');
// 		*/
// 	});
// 	</script>";,
// }
// add_action( 'wp_footer', 'tribe_prevent_ajax_paging', 99 );

/**
 * @snippet       Plus Minus Quantity Buttons @ WooCommerce Single Product Page
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=90052
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.5.1
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
// -------------

 
// -------------
// 2. Trigger jQuery script
 
add_action( 'wp_footer', 'unitix_add_cart_quantity_plus_minus' );
 
function unitix_add_cart_quantity_plus_minus() {
    // Only run this on the single product page
    if ( ! is_product() ) return;
    ?>
        <script type="text/javascript">
             
        jQuery(document).ready(function($){ 
             
            $('form.cart').on( 'click', 'input.plus, input.minus', function() {
 
                // Get current quantity values
                var qty = $( this ).closest( 'form.cart' ).find( '.qty' );
                var val = parseFloat(qty.val());
                var max = parseFloat(qty.attr( 'max' ));
                var min = parseFloat(qty.attr( 'min' ));
                var step = parseFloat(qty.attr( 'step' ));
 
                // Change the value if plus or minus
                if ( $( this ).is( '.plus' ) ) {
                    if ( max && ( max <= val ) ) {
                        qty.val( max );
                    } else {
                        qty.val( val + step );
                    }
                } else {
                    if ( min && ( min >= val ) ) {
                        qty.val( min );
                    } else if ( val > 1 ) {
                        qty.val( val - step );
                    }
                }
                 
            });
             
        });
             
        </script>
    <?php
}