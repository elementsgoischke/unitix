<?php
/**
 * Stores options for themes quick uploaders
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

return array(
    // slideshoe quick upload
    'oxy_slideshow_image' => array(
        'menu_title' => esc_html__('Quick Slideshow', 'unitix-admin-td'),
        'page_title' => esc_html__('Quick Slideshow Creator', 'unitix-admin-td'),
        'item_singular'  => esc_html__('Slideshow Image', 'unitix-admin-td'),
        'item_plural'  => esc_html__('Slideshow Images', 'unitix-admin-td'),
        'taxonomies' => array(
            'oxy_slideshow_categories'
        )
    ),
    // services quick upload
    'oxy_service' => array(
        'menu_title' => esc_html__('Quick Services', 'unitix-admin-td'),
        'page_title' => esc_html__('Quick Services Creator', 'unitix-admin-td'),
        'item_singular'  => esc_html__('Services', 'unitix-admin-td'),
        'item_plural'  => esc_html__('Service', 'unitix-admin-td'),
        'show_editor' => true,
    ),
    // portfolio quick upload
    'oxy_portfolio_image' => array(
        'menu_title' => esc_html__('Quick Portfolio', 'unitix-admin-td'),
        'page_title' => esc_html__('Quick Portfolio Creator', 'unitix-admin-td'),
        'item_singular'  => esc_html__('Portfolio Image', 'unitix-admin-td'),
        'item_plural'  => esc_html__('Portfolio Images', 'unitix-admin-td'),
        'show_editor' => true,
        'taxonomies' => array(
            'oxy_portfolio_categories'
        )
    ),
    // staff quick upload
    'oxy_staff' => array(
        'menu_title' => esc_html__('Quick Staff', 'unitix-admin-td'),
        'page_title' => esc_html__('Quick Staff Creator', 'unitix-admin-td'),
        'item_singular'  => esc_html__('Staff Member', 'unitix-admin-td'),
        'item_plural'  => esc_html__('Staff', 'unitix-admin-td'),
        'show_editor' => true,
        'taxonomies' => array(
            'oxy_staff_skills'
        )
    )
);