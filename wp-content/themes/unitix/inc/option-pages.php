<?php
/**
 * Registers all theme option pages
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license **LICENSE**
 * @version 1.18.7
 */

// setup blog header options
$blog_extra_header_options = array(
     array(
        'name' => esc_html__('Show Header', 'unitix-admin-td'),
        'desc' => esc_html__('Show or hide the header.', 'unitix-admin-td'),
        'id'   => 'show_header',
        'type' => 'select',
        'default' => 'show',
        'options' => array(
            'hide' => esc_html__('Hide', 'unitix-admin-td'),
            'show' => esc_html__('Show', 'unitix-admin-td'),
        ),
    ),
    array(
        'name'    => esc_html__('Header Height', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose the amount of padding added to the height of the header', 'unitix-admin-td'),
        'id'      => 'height',
        'type'    => 'select',
        'options' => array(
            'normal'     => esc_html__('Normal', 'unitix-admin-td'),
            'short'      => esc_html__('Short', 'unitix-admin-td'),
            'tiny'       => esc_html__('Tiny', 'unitix-admin-td'),
            'nopadding' => esc_html__('No Padding', 'unitix-admin-td'),
        ),
        'default' => 'normal',
    ),
);
$blog_header_options = include OXY_THEME_DIR . 'inc/options/global-options/section-header-text.php';
$blog_header_background_options = include OXY_THEME_DIR . 'inc/options/global-options/section-background-image.php';
// change defaults
$blog_header_options[0]['default'] = esc_html__('Blog', 'unitix-admin-td');
$blog_header_options[1]['default'] = esc_html__('Latest News and Updates', 'unitix-admin-td');

global $oxy_theme;
if( isset($oxy_theme) ) {
    $oxy_theme->register_option_page( array(
        'page_title' => esc_html__('General', 'unitix-admin-td'),
        'menu_title' => esc_html__('General', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-general',
        'main_menu'  => true,
        'main_menu_title' => THEME_NAME,
        'main_menu_icon'  => 'dashicons-marker',
        'icon'       => 'tools',
        // 'javascripts' => array(
        //     array(
        //         'handle' => 'header_options_script',
        //         'src'    => OXY_THEME_URI . 'inc/options/javascripts/pages/header-options.js',
        //         'deps'   => array( 'jquery'),
        //         'localize' => array(
        //             'object_handle' => 'theme',
        //             'data'          => THEME_SHORT
        //         ),
        //     ),
        // ),
        'sections'   => array(
            'logo-section' => array(
                'title'   => esc_html__('Logo', 'unitix-admin-td'),
                'header'  => esc_html__('These options allow you to configure the site logo, you can select a logo type and then create a text logo, image logo or both image and text.  There is also an option to use retina sized images.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Logo Text', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add your logo text here works with Logo Type (Text, Text & Image)', 'unitix-admin-td'),
                        'id'      => 'logo_text',
                        'type'    => 'text',
                        'default' => 'Unitix',
                    ),
                    array(
                        'name'    => esc_html__('Logo Image', 'unitix-admin-td'),
                        'desc'    => esc_html__('Upload a logo for your site, works with Logo Type (Image, Text & Image)', 'unitix-admin-td'),
                        'id'      => 'logo_image',
                        'store'   => 'url',
                        'type'    => 'upload',
                        'default' => OXY_THEME_URI . 'assets/images/logo.png',
                    ),
                )
            ),
            'loader-section' => array(
                'title'  => esc_html__('Page Loader', 'unitix-admin-td'),
                'header' => esc_html__('Toggle an animation when each page loads.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Loading Animation', 'unitix-admin-td'),
                        'desc'      => esc_html__('Show a loader whenever a page is loaded', 'unitix-admin-td'),
                        'id'        => 'site_loader',
                        'type'      => 'radio',
                        'options'   => array(
                            'on'    => esc_html__('Enable', 'unitix-admin-td'),
                            'off'   => esc_html__('Disable', 'unitix-admin-td'),
                        ),
                        'default'   => 'off',
                    ),
                    array(
                        'name'    => esc_html__('Page Loader Style', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style of page loader to show at the start of loading a page', 'unitix-admin-td'),
                        'id'      => 'site_loader_style',
                        'type'    => 'radio',
                        'options' => array(
                            'dot'     => esc_html__('Dot', 'unitix-admin-td'),
                            'minimal' => esc_html__('Minimal', 'unitix-admin-td'),
                            'counter' => esc_html__('Counter', 'unitix-admin-td'),
                        ),
                        'default' => 'minimal',
                    )
                )
            ),
            'header-section' => array(
                'title'   => esc_html__('Header Options', 'unitix-admin-td'),
                'header'  => esc_html__('This section will allow you to setup your site header.  You can choose from three different types of header to use on your site, and adjust the header height to allow room for your logo.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Header Type', 'unitix-admin-td'),
                        'desc'    => esc_html__("Sets the type of header to use at the top of your site and its behaviour.", 'unitix-admin-td'),
                        'id'      => 'header_type',
                        'type'    => 'select',
                        'options' => array(
                            'navbar-sticky'     => esc_html__('Nav Bar Fixed - Navigation bar that scrolls with the page.', 'unitix-admin-td'),
                            'navbar-not-sticky' => esc_html__('Nav Bar Static - Navigation bar with regular scrolling.', 'unitix-admin-td'),
                        ),
                        'default' => 'navbar-sticky',
                    ),
                    array(
                        'name'      => esc_html__('Menu Height', 'unitix-admin-td'),
                        'desc'      => esc_html__('Use this slider to adjust the menu height.  Ideal if you want to adjust the height to fit your logo.', 'unitix-admin-td'),
                        'id'        => 'navbar_height',
                        'type'      => 'slider',
                        'default'   => 90,
                        'attr'      => array(
                            'max'       => 300,
                            'min'       => 24,
                            'step'      => 1
                        )
                    ),
                     array(
                        'name'      => esc_html__('Menu Change Scroll Point', 'unitix-admin-td'),
                        'desc'      => esc_html__('Point in pixels after the page scrolls that will trigger the menu to change shape / colour.', 'unitix-admin-td'),
                        'id'        => 'navbar_scrolled_point',
                        'type'      => 'slider',
                        'default'   => 200,
                        'attr'      => array(
                            'max'       => 1000,
                            'min'       => 0,
                            'step'      => 1
                        )
                    ),
                    array(
                        'name'      => esc_html__('Menu Height After Scroll', 'unitix-admin-td'),
                        'desc'      => esc_html__('Use this slider to adjust the menu height after menu has scrolled.', 'unitix-admin-td'),
                        'id'        => 'navbar_scrolled',
                        'type'      => 'slider',
                        'default'   => 70,
                        'attr'      => array(
                            'max'       => 300,
                            'min'       => 24,
                            'step'      => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Hover Menu', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose between menu that will open when you click or hover (desktop only option since mobile devices will always use touch)', 'unitix-admin-td'),
                        'id'      => 'hover_menu',
                        'type'    => 'radio',
                        'options' => array(
                            'off'  => esc_html__('Click', 'unitix-admin-td'),
                            'on'     => esc_html__('Hover', 'unitix-admin-td'),
                        ),
                        'default' => 'off',
                    ),
                    array(
                        'name'    => esc_html__('Hover Menu Delay', 'unitix-admin-td'),
                        'desc'    => esc_html__('Delay in seconds before the hover menu closes after moving mouse off the menu.', 'unitix-admin-td'),
                        'id'      => 'hover_menu_delay',
                        'type'      => 'slider',
                        'default'   => 200,
                        'attr'      => array(
                            'max'       => 1000,
                            'min'       => 0,
                            'step'      => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Hover Menu Fade Delay', 'unitix-admin-td'),
                        'desc'    => esc_html__('Delay of the Fade In/Fade Out animation .', 'unitix-admin-td'),
                        'id'      => 'hover_menu_fade_delay',
                        'type'      => 'slider',
                        'default'   => 200,
                        'attr'      => array(
                            'max'       => 1000,
                            'min'       => 0,
                            'step'      => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Header Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the header', 'unitix-admin-td'),
                        'id'      => 'header_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'      => esc_html__('Dropdown Menu Width', 'unitix-admin-td'),
                        'desc'      => esc_html__('Use this slider to adjust the dropdown width.  ', 'unitix-admin-td'),
                        'id'        => 'dropdown_width',
                        'type'      => 'slider',
                        'default'   => 200,
                        'attr'      => array(
                            'max'       => 300,
                            'min'       => 200,
                            'step'      => 10
                        )
                    ),

                    array(
                        'name'    => esc_html__('Top Bar Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the Top Bar when you have a Header Type Top Bar or Combo', 'unitix-admin-td'),
                        'id'      => 'top_bar_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Capitalization', 'unitix-admin-td'),
                        'desc'    => esc_html__('Enable-disable automatic capitalization in header logo and menus', 'unitix-admin-td'),
                        'id'      => 'header_capitalization',
                        'type'    => 'select',
                        'options' => array(
                            'on'          => esc_html__('Uppercase', 'unitix-admin-td'),
                            'lowercase'   => esc_html__('Lowercase', 'unitix-admin-td'),
                            'capitalize'  => esc_html__('Capitalize', 'unitix-admin-td'),
                            'off'         => esc_html__('None', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                )
            ),
            'layout-options' => array(
                'title'   => esc_html__('Layout Options', 'unitix-admin-td'),
                'header'  => esc_html__('This section will allow you to setup the layout of your site.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                         'name'    => esc_html__('Layout Type', 'unitix-admin-td'),
                         'desc'    => esc_html__('Sets the site layout (Normal - site will use all the width of the page, Boxed - Site will be surrounded by a background )', 'unitix-admin-td'),
                         'id'      => 'layout_type',
                         'type'    => 'radio',
                         'options' => array(
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'boxed'  => esc_html__('Boxed', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    )
                )
            ),
            'upper-footer-section' => array(
                'title'   => esc_html__('Upper Footer', 'unitix-admin-td'),
                'header'  => esc_html__('This footer is above the bottom footer of your site.  Here you can set the footer to use 1-4 columns, you can add Widgets to your footer in the Appearance -> Widgets page', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Upper Footer Columns', 'unitix-admin-td'),
                        'desc'    => esc_html__('Select how many columns will the upper footer consist of.', 'unitix-admin-td'),
                        'id'      => 'upper_footer_columns',
                        'type'    => 'radio',
                        'options' => array(
                            1  => esc_html__('1', 'unitix-admin-td'),
                            2  => esc_html__('2', 'unitix-admin-td'),
                            3  => esc_html__('3', 'unitix-admin-td'),
                            4  => esc_html__('4', 'unitix-admin-td'),
                        ),
                        'default' => 2,
                    ),
                    array(
                        'name'    => esc_html__('Upper Footer Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the upper footer', 'unitix-admin-td'),
                        'id'      => 'upper_footer_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Upper Footer Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the Upper Footer decoration.', 'unitix-admin-td'),
                        'id'      => 'upper_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    ),
                    array(
                        'name'    => esc_html__('Upper Footer Height', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the amount of padding added to the height of the Upper Footer', 'unitix-admin-td'),
                        'id'      => 'upper_footer_height',
                        'type'    => 'select',
                        'options' => array(
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'short'  => esc_html__('Short', 'unitix-admin-td'),
                            'tiny'   => esc_html__('Tiny', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    )
                )
            ),
            'footer-section' => array(
                'title'   => esc_html__('Footer', 'unitix-admin-td'),
                'header'  => esc_html__('The footer is the bottom bar of your site.  Here you can set the footer to use 1-4 columns, you can add Widgets to your footer in the Appearance -> Widgets page', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Footer Columns', 'unitix-admin-td'),
                        'desc'    => esc_html__('Select how many columns will the footer consist of.', 'unitix-admin-td'),
                        'id'      => 'footer_columns',
                        'type'    => 'radio',
                        'options' => array(
                            1  => esc_html__('1', 'unitix-admin-td'),
                            2  => esc_html__('2', 'unitix-admin-td'),
                            3  => esc_html__('3', 'unitix-admin-td'),
                            4  => esc_html__('4', 'unitix-admin-td'),
                        ),
                        'default' => 2,
                    ),
                    array(
                        'name'    => esc_html__('Footer Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the footer', 'unitix-admin-td'),
                        'id'      => 'footer_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Footer Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the Footer decoration.', 'unitix-admin-td'),
                        'id'      => 'footer_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    ),
                    array(
                        'name'    => esc_html__('Footer Height', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the amount of padding added to the height of the Footer', 'unitix-admin-td'),
                        'id'      => 'footer_height',
                        'type'    => 'select',
                        'options' => array(
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'short'  => esc_html__('Short', 'unitix-admin-td'),
                            'tiny'   => esc_html__('Tiny', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    ),
                    array(
                        'name'    => esc_html__('Back to top', 'unitix-admin-td'),
                        'desc'    => esc_html__('Enable the back-to-top button', 'unitix-admin-td'),
                        'id'      => 'back_to_top',
                        'type'    => 'radio',
                        'options' => array(
                            'enable'  => esc_html__('Enable', 'unitix-admin-td'),
                            'disable'  => esc_html__('Disable', 'unitix-admin-td'),
                        ),
                        'default' => 'enable',
                    ),
                )
            ),
            'google-map-section' => array(
                'title'   => esc_html__('Google Maps API key', 'unitix-admin-td'),
                'header'  => esc_html__('Activates the Google Maps JavaScript API.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'     => esc_html__('Google Maps API key', 'unitix-admin-td'),
                        'desc'     => esc_html__(' Necessary for the Google Map shortcode.', 'unitix-admin-td'),
                        'id'       => 'api_key',
                        'type'     => 'text',
                        'default' =>  '',
                    ),
                )
            ),
        )
    ));
    $oxy_theme->register_option_page( array(
        'page_title' => esc_html__('Blog', 'unitix-admin-td'),
        'menu_title' => esc_html__('Blog', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-blog',
        'main_menu'  => false,
        'icon'       => 'tools',
        'sections'   => array(
            'blog-header-section' => array(
                'title'   => esc_html__('Blog Header Options', 'unitix-admin-td'),
                'header'  => esc_html__('Change how your blog header looks.', 'unitix-admin-td'),
                'fields' => array_merge( $blog_extra_header_options, $blog_header_options, $blog_header_background_options )
            ),
            'blog-section' => array(
                'title'   => esc_html__('General Blog Options', 'unitix-admin-td'),
                'header'  => esc_html__('Here you can setup the blog options that are used on all the main blog list pages', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Blog Layout', 'unitix-admin-td'),
                        'desc'    => esc_html__('Layout of your blog page. Choose right sidebar, left sidebar, fullwidth layout', 'unitix-admin-td'),
                        'id'      => 'blog_layout',
                        'type'    => 'radio',
                        'options' => array(
                            'sidebar-right' => esc_html__('Right Sidebar', 'unitix-admin-td'),
                            'full-width'    => esc_html__('Full Width', 'unitix-admin-td'),
                            'sidebar-left'  => esc_html__('Left Sidebar', 'unitix-admin-td'),
                        ),
                        'default' => 'sidebar-right',
                    ),
                    array(
                        'name'    => esc_html__('Post Icons', 'unitix-admin-td'),
                        'desc'    => esc_html__('Toggle post icons on/off in post', 'unitix-admin-td'),
                        'id'      => 'blog_post_icons',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Show Comments On', 'unitix-admin-td'),
                        'desc'    => esc_html__('Where to allow comments. All (show all), Pages (only on pages), Posts (only on posts), Off (all comments are off)', 'unitix-admin-td'),
                        'id'      => 'site_comments',
                        'type'    => 'radio',
                        'options' => array(
                            'all'   => esc_html__('All', 'unitix-admin-td'),
                            'pages' => esc_html__('Pages', 'unitix-admin-td'),
                            'posts' => esc_html__('Posts', 'unitix-admin-td'),
                            'Off'   => esc_html__('Off', 'unitix-admin-td')
                        ),
                        'default' => 'posts',
                    ),
                    array(
                        'name'    => esc_html__('Show Read More', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show or hide the readmore link in list view', 'unitix-admin-td'),
                        'id'      => 'blog_show_readmore',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name' => esc_html__('Blog read more link', 'unitix-admin-td'),
                        'desc' => esc_html__('The text that will be used for your read more links', 'unitix-admin-td'),
                        'id' => 'blog_readmore',
                        'type' => 'text',
                        'default' => 'read more',
                    ),
                    array(
                        'name'    => esc_html__('Strip teaser', 'unitix-admin-td'),
                        'desc'    => esc_html__('Strip the content before the <--more--> tag in single post view', 'unitix-admin-td'),
                        'id'      => 'blog_stripteaser',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'off',
                    ),
                    array(
                        'name'    => esc_html__('Pagination Style', 'unitix-admin-td'),
                        'desc'    => esc_html__('How your pagination will be shown', 'unitix-admin-td'),
                        'id'      => 'blog_pagination',
                        'type'    => 'radio',
                        'options' => array(
                            'pages'     => esc_html__('Pages', 'unitix-admin-td'),
                            'next_prev' => esc_html__('Next & Previous', 'unitix-admin-td'),
                        ),
                        'default' => 'pages',
                    ),
                )
            ),
            'blog-single-section' => array(
                'title'   => esc_html__('Blog Single Page', 'unitix-admin-td'),
                'header'  => esc_html__('This section allows you to set up how your single post will look.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Display categories', 'unitix-admin-td'),
                        'desc'    => esc_html__('Toggle categories on/off in post', 'unitix-admin-td'),
                        'id'      => 'blog_categories',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Display tags', 'unitix-admin-td'),
                        'desc'    => esc_html__('Toggle tags on/off in post', 'unitix-admin-td'),
                        'id'      => 'blog_tags',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Display comment count', 'unitix-admin-td'),
                        'desc'    => esc_html__('Toggle comment count on/off in post', 'unitix-admin-td'),
                        'id'      => 'blog_comment_count',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Show related posts', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show Related Posts after the post content', 'unitix-admin-td'),
                        'id'      => 'related_posts',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Number of related posts', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose how many related posts are displayed in the related posts section after the post content', 'unitix-admin-td'),
                        'id'      => 'related_posts_number',
                        'type'    => 'radio',
                        'options' => array(
                            '3'   => esc_html__('3', 'unitix-admin-td'),
                            '4'   => esc_html__('4', 'unitix-admin-td'),
                            '6'   => esc_html__('6', 'unitix-admin-td'),
                            '8'   => esc_html__('8', 'unitix-admin-td'),
                        ),
                        'default' => '3',
                    ),
                    array(
                        'name'    => esc_html__('Related posts per slide', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose how many related posts are displayed in each slide', 'unitix-admin-td'),
                        'id'      => 'related_posts_per_slide',
                        'type'    => 'radio',
                        'options' => array(
                            '3'   => esc_html__('3', 'unitix-admin-td'),
                            '4'   => esc_html__('4', 'unitix-admin-td'),
                        ),
                        'default' => '3',
                    ),
                    array(
                        'name'    => esc_html__('Related posts section height', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the amount of padding added to the height of the Related posts section', 'unitix-admin-td'),
                        'id'      => 'related_posts_height',
                        'type'    => 'select',
                        'options' => array(
                            'section-normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'section-short'  => esc_html__('Short', 'unitix-admin-td'),
                            'section-tiny'   => esc_html__('Tiny', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    ),
                    array(
                        'name'    => esc_html__('Display avatar', 'unitix-admin-td'),
                        'desc'    => esc_html__('Toggle avatars on/off in Author Bio Section', 'unitix-admin-td'),
                        'id'      => 'site_avatars',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Open Featured Image in Magnific Popup', 'unitix-admin-td'),
                        'desc'    => esc_html__('Featured image in single post view will open in a large preview popup', 'unitix-admin-td'),
                        'id'      => 'blog_fancybox',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Show Facebook Button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show facebook share button on your single blog pages', 'unitix-admin-td'),
                        'id'      => 'fb_show',
                        'type'    => 'radio',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default' => 'show',
                    ),
                    array(
                        'name'    => esc_html__('Show Tweet Button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show tweet share button on your single blog pages', 'unitix-admin-td'),
                        'id'      => 'twitter_show',
                        'type'    => 'radio',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default' => 'show',
                    ),
                    array(
                        'name'    => esc_html__('Show Google+ Button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show G+ share button on your single blog pages', 'unitix-admin-td'),
                        'id'      => 'google_show',
                        'type'    => 'radio',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default' => 'show',
                    ),
                    array(
                        'name'    => esc_html__('Show Pinterest Button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show Pinterest share button on your single blog pages', 'unitix-admin-td'),
                        'id'      => 'pinterest_show',
                        'type'    => 'radio',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default' => 'show',
                    ),
                    array(
                        'name'    => esc_html__('Show LinkedIn Button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show LinkedIn share button on your single blog pages', 'unitix-admin-td'),
                        'id'      => 'linkedin_show',
                        'type'    => 'radio',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default' => 'show',
                    )
                )
            ),
            'swatches-section' => array(
                'title'   => esc_html__('Swatches', 'unitix-admin-td'),
                'header'  => esc_html__('All the blog sections can be swatched.  You can choose the colours of your blog header, posts, related posts and author bio.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Header Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Select the colour scheme to use for the header on this page.', 'unitix-admin-td'),
                        'id'      => 'blog_header_swatch',
                        'type' => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Blog Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the Blog page', 'unitix-admin-td'),
                        'id'      => 'blog_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-white-red',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Blog Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the blog decoration.', 'unitix-admin-td'),
                        'id'      => 'blog_header_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    ),
                    array(
                        'name'    => esc_html__('Related Posts Section Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the related posts section below post content', 'unitix-admin-td'),
                        'id'      => 'related_posts_section_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Related Posts Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the Related Posts decoration.', 'unitix-admin-td'),
                        'id'      => 'related_posts_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    ),
                    array(
                        'name'    => esc_html__('Related Posts Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for all the related posts below post content', 'unitix-admin-td'),
                        'id'      => 'related_posts_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-white-red',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Author Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the author bio section', 'unitix-admin-td'),
                        'id'      => 'author_bio_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-white-red',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Author Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the Author\'s Page decoration.', 'unitix-admin-td'),
                        'id'      => 'author_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    ),
                    array(
                        'name'    => esc_html__('Search Result Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the search result section', 'unitix-admin-td'),
                        'id'      => 'search_result_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-white-red',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Search Result Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the search result page decoration.', 'unitix-admin-td'),
                        'id'      => 'search_result_decoration',
                        'type'    => 'select',
                        'default' => '',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    )
                )
            ),
        )
    ));
    $oxy_theme->register_option_page( array(
        'page_title' => esc_html__('Flexslider Options', 'unitix-admin-td'),
        'menu_title' => esc_html__('Flexslider', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-flexslider',
        'header'  => esc_html__('Global options for flexsliders used in the site (gallery posts, gallery portfolio items).', 'unitix-admin-td'),
        'main_menu'  => false,
        'icon'       => 'tools',
        'sections'   => array(
            'slider-section' => array(
                'title' => esc_html__('Slideshow', 'unitix-admin-td'),
                'header'  => esc_html__('Setup your global default flexslider options.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      =>  esc_html__('Animation style', 'unitix-admin-td'),
                        'desc'      =>  esc_html__('Select how your slider animates', 'unitix-admin-td'),
                        'id'        => 'animation',
                        'type'      => 'select',
                        'options'   =>  array(
                            'slide' => esc_html__('Slide', 'unitix-admin-td'),
                            'fade'  => esc_html__('Fade', 'unitix-admin-td'),
                        ),
                        'attr'      =>  array(
                            'class'    => 'widefat',
                        ),
                        'default'   => 'slide',
                    ),
                    array(
                        'name'      => esc_html__('Direction', 'unitix-admin-td'),
                        'desc'      =>  esc_html__('Select the direction your slider slides', 'unitix-admin-td'),
                        'id'        => 'direction',
                        'type'      => 'select',
                        'default'   =>  'horizontal',
                        'options' => array(
                            'horizontal' => esc_html__('Horizontal', 'unitix-admin-td'),
                            'vertical'   => esc_html__('Vertical', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Speed', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the speed of the slideshow cycling, in milliseconds', 'unitix-admin-td'),
                        'id'        => 'speed',
                        'type'      => 'slider',
                        'default'   => 7000,
                        'attr'      => array(
                            'max'       => 15000,
                            'min'       => 2000,
                            'step'      => 1000
                        )
                    ),
                    array(
                        'name'      => esc_html__('Duration', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the speed of animations', 'unitix-admin-td'),
                        'id'        => 'duration',
                        'type'      => 'slider',
                        'default'   => 600,
                        'attr'      => array(
                            'max'       => 1500,
                            'min'       => 200,
                            'step'      => 100
                        )
                    ),
                    array(
                        'name'      => esc_html__('Auto start', 'unitix-admin-td'),
                        'id'        => 'autostart',
                        'type'      => 'radio',
                        'default'   =>  'true',
                        'desc'    => esc_html__('Start slideshow automatically', 'unitix-admin-td'),
                        'options' => array(
                            'true'  => esc_html__('On', 'unitix-admin-td'),
                            'false' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show navigation arrows', 'unitix-admin-td'),
                        'id'        => 'directionnav',
                        'type'      => 'radio',
                        'desc'    => esc_html__('Shows the navigation arrows at the sides of the flexslider.', 'unitix-admin-td'),
                        'default'   =>  'hide',
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Navigation arrows type', 'unitix-admin-td'),
                        'id'        => 'directionnavtype',
                        'type'      => 'radio',
                        'desc'      => esc_html__('Type of the direction arrows, fancy (with bg) or simple.', 'unitix-admin-td'),
                        'default'   =>  'simple',
                        'options' => array(
                            'simple' => esc_html__('Simple', 'unitix-admin-td'),
                            'fancy'  => esc_html__('Fancy', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show controls', 'unitix-admin-td'),
                        'id'        => 'showcontrols',
                        'type'      => 'radio',
                        'default'   =>  'show',
                        'desc'    => esc_html__('If you choose hide the option below will be ignored', 'unitix-admin-td'),
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Choose the place of the controls', 'unitix-admin-td'),
                        'id'        => 'controlsposition',
                        'type'      => 'radio',
                        'default'   =>  'inside',
                        'desc'    => esc_html__('Choose the position of the navigation controls', 'unitix-admin-td'),
                        'options' => array(
                            'inside'    => esc_html__('Inside', 'unitix-admin-td'),
                            'outside'   => esc_html__('Outside', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      =>  esc_html__('Choose the alignment of the controls', 'unitix-admin-td'),
                        'id'        => 'controlsalign',
                        'type'      => 'radio',
                        'desc'    => esc_html__('Choose the alignment of the navigation controls', 'unitix-admin-td'),
                        'options'   =>  array(
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                        ),
                        'attr'      =>  array(
                            'class'    => 'widefat',
                        ),
                        'default'   => 'center',
                    ),
                    array(
                        'name'      => esc_html__('Show tooltip', 'unitix-admin-td'),
                        'id'        => 'tooltip',
                        'type'      => 'radio',
                        'default'   =>  'hide',
                        'desc'    => esc_html__('Display the slide title as tooltip', 'unitix-admin-td'),
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Reverse', 'unitix-admin-td'),
                        'id'        => 'reverse',
                        'type'      => 'radio',
                        'default'   =>  'false',
                        'desc'    => esc_html__('Reverse the animation direction', 'unitix-admin-td'),
                        'options' => array(
                            'true'  => esc_html__('On', 'unitix-admin-td'),
                            'false' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Animation Loop', 'unitix-admin-td'),
                        'id'        => 'animationloop',
                        'type'      => 'radio',
                        'default'   =>  'true',
                        'desc'    => esc_html__('Gives the slider a seamless infinite loop', 'unitix-admin-td'),
                        'options' => array(
                            'true'  => esc_html__('On', 'unitix-admin-td'),
                            'false' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
            'captions-section' => array(
                'title' => esc_html__('Captions', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Show Captions', 'unitix-admin-td'),
                        'id'        => 'captions',
                        'type'      => 'radio',
                        'default'   =>  'hide',
                        'desc'    => esc_html__('If you choose hide the options below will be ignored', 'unitix-admin-td'),
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            ),
                    ),
                    array(
                        'name'      => esc_html__('Captions Vertical Position', 'unitix-admin-td'),
                        'desc'      => esc_html__('Choose between bottom and top positioning', 'unitix-admin-td'),
                        'id'        => 'captions_vertical',
                        'type'      => 'radio',
                        'default'   =>  'bottom',
                        'options' => array(
                            'top'    => esc_html__('Top', 'unitix-admin-td'),
                            'bottom' => esc_html__('Bottom', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Captions Horizontal Position', 'unitix-admin-td'),
                        'desc'      => esc_html__('Choose among left, right and alternate positioning', 'unitix-admin-td'),
                        'id'        => 'captions_horizontal',
                        'type'      => 'radio',
                        'default'   =>  'left',
                        'options' => array(
                            'left'      => esc_html__('Left', 'unitix-admin-td'),
                            'right'     => esc_html__('Right', 'unitix-admin-td'),
                            'alternate' => esc_html__('Alternate', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
        )
    ));

    $oxy_theme->register_option_page(   array(
        'page_title' => esc_html__('404 Page Options', 'unitix-admin-td'),
        'menu_title' => esc_html__('404', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-404',
        'main_menu'  => false,
        'icon'       => 'tools',
        'sections'   => array(
            '404-header-section' => array(
                'title'   => esc_html__('Header', 'unitix-admin-td'),
                'header'  => esc_html__('If someone goes to a invalid url this 404 page will be shown.  You can change the image, title, text and colour of the 404 page here.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('404 image', 'unitix-admin-td'),
                        'desc'    => esc_html__('Upload an image to show on your 404 page', 'unitix-admin-td'),
                        'id'      => '404_header_image',
                        'type'    => 'upload',
                        'store'   => 'url',
                        'default' => OXY_THEME_URI . 'assets/images/404.png',
                    ),
                    array(
                        'name'    => esc_html__('Page Title', 'unitix-admin-td'),
                        'desc'    => esc_html__('The title that appears on your 404 page', 'unitix-admin-td'),
                        'id'      => '404_title',
                        'type'    => 'text',
                        'default' => esc_html__('Page Not Found', 'unitix-admin-td')
                    ),
                    array(
                        'name'    => esc_html__('Header Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the 404 page', 'unitix-admin-td'),
                        'id'      => '404_header_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Page Text', 'unitix-admin-td'),
                        'desc'    => esc_html__('The content of your 404 page', 'unitix-admin-td'),
                        'id'      => '404_content',
                        'type'    => 'editor',
                        'settings' => array( 'media_buttons' => false ),
                        'default' => esc_html__('The page you requested could not be found.', 'unitix-admin-td')
                    ),
                    array(
                        'name'    => esc_html__('Page Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the 404 page', 'unitix-admin-td'),
                        'id'      => '404_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Page Top Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the top decoration.', 'unitix-admin-td'),
                        'id'      => '404_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    )
                )
            ),
        ),
    ));
    $oxy_theme->register_option_page( array(
        'page_title' => esc_html__('Portfolio Page Options', 'unitix-admin-td'),
        'menu_title' => esc_html__('Portfolio', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-portfolio',
        'main_menu'  => false,
        'sections'   => array(
            'portfolio-section' => array(
                'title'   => esc_html__('Portfolio Single Page', 'unitix-admin-td'),
                'header'  => esc_html__('When you click on a portfolio item you will be taken to its single page.  You can change how these pages look here.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Show related items', 'unitix-admin-td'),
                        'desc'    => esc_html__('Toggle the display of the related items section', 'unitix-admin-td'),
                        'id'      => 'portfolio_show_related',
                        'type'    => 'radio',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Related items title', 'unitix-admin-td'),
                        'desc'    => esc_html__('Related items title that is shown on single portfolio page above related items', 'unitix-admin-td'),
                        'id'      => 'portfolio_related_title',
                        'type'    => 'text',
                        'default' => esc_html__('Related Work', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Related items Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Swatch for the related items in single portfolio page', 'unitix-admin-td'),
                        'id'      => 'portfolio_related_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Related items Section Decoration', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a style to use as the top decoration.', 'unitix-admin-td'),
                        'id'      => 'portfolio_related_decoration',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                    ),
                    array(
                        'name'    => esc_html__('Related items section height', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the amount of padding added to the height of the Related items section', 'unitix-admin-td'),
                        'id'      => 'related_items_height',
                        'type'    => 'select',
                        'options' => array(
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'short'  => esc_html__('Short', 'unitix-admin-td'),
                            'tiny'   => esc_html__('Tiny', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    ),
                    array(
                        'name'      => esc_html__('Related items Shape', 'unitix-admin-td'),
                        'desc'      => esc_html__('Shape for the related items in single portfolio page', 'unitix-admin-td'),
                        'id'        => 'portfolio_related_shape',
                        'type'      => 'select',
                        'default'   => 'hex',
                        'options' => array(
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      =>  esc_html__('Related items Shadow', 'unitix-admin-td'),
                        'id'        => 'portfolio_related_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'hide',
                    ),
                )
            ),
            'portfolio-size-section' => array(
                'title'   => esc_html__('Portfolio Image Sizes', 'unitix-admin-td'),
                'header'  => esc_html__('When your portfolio images are uploaded they will be automatially saved using these dimensions.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Image Width', 'unitix-admin-td'),
                        'desc'    => esc_html__('Width of each portfolio item', 'unitix-admin-td'),
                        'id'      => 'portfolio_item_width',
                        'type'    => 'slider',
                        'default'   => 800,
                        'attr'      => array(
                            'max'       => 1200,
                            'min'       => 50,
                            'step'      => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Image Height', 'unitix-admin-td'),
                        'desc'    => esc_html__('Height of each portfolio item', 'unitix-admin-td'),
                        'id'      => 'portfolio_item_height',
                        'type'    => 'slider',
                        'default'   => 600,
                        'attr'      => array(
                            'max'       => 800,
                            'min'       => 50,
                            'step'      => 1
                        )
                    ),
                    array(
                        'name'      => esc_html__('Image Cropping', 'unitix-admin-td'),
                        'id'        => 'portfolio_item_crop',
                        'type'      => 'radio',
                        'default'   =>  'on',
                        'desc'    => esc_html__('Crop images to the exact proportions', 'unitix-admin-td'),
                        'options' => array(
                            'on' => esc_html__('Crop Images', 'unitix-admin-td'),
                            'off' => esc_html__('Do not crop', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
        )
    ));
    $oxy_theme->register_option_page( array(
        'page_title' => esc_html__('Post Types', 'unitix-admin-td'),
        'menu_title' => esc_html__('Post Types', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-post-types',
        'main_menu'  => false,
        'sections'   => array(
            'permalinks-section' => array(
                'title'   => esc_html__('Configure your permalinks here', 'unitix-admin-td'),
                'header'  => esc_html__('Some of the custom single pages ( Portfolios, Services, Staff ) can be configured to use their own special url.  This helps with SEO.  Set your permalinks here, save and then navigate to one of the items and you will see the url in the format below.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'prefix'  => '<code>' . get_home_url() . '/</code>',
                        'postfix' => '<code>/my-portfolio-item</code>',
                        'name'    => esc_html__('Portfolio URL slug', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the url you would like your portfolios to be shown on', 'unitix-admin-td'),
                        'id'      => 'portfolio_slug',
                        'type'    => 'text',
                        'default' => 'portfolio',
                    ),
                    array(
                        'prefix'  => '<code>' . get_home_url() . '/</code>',
                        'postfix' => '<code>/my-service</code>',
                        'name'    => esc_html__('Service URL slug', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the url you would like your services to use', 'unitix-admin-td'),
                        'id'      => 'services_slug',
                        'type'    => 'text',
                        'default' => 'our-services',
                    ),
                    array(
                        'prefix'  => '<code>' . get_home_url() . '/</code>',
                        'postfix' => '<code>/our-team</code>',
                        'name'    => esc_html__('Staff URL slug', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the url you would like your staff pages to use', 'unitix-admin-td'),
                        'id'      => 'staff_slug',
                        'type'    => 'text',
                        'default' => 'our-team',
                    ),
                )
            ),
            'posttypes-archives-section' => array(
                'title'   => esc_html__('Post Types Archive Pages', 'unitix-admin-td'),
                'header'  => esc_html__('Set your post types archives pages here', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Portfolio Archive Page', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the archive page for the portfolio post type', 'unitix-admin-td'),
                        'id'        => 'portfolio_archive_page',
                        'type'      => 'select',
                        'options'  => 'taxonomy',
                        'taxonomy' => 'pages',
                        'default' =>  '',
                        'blank' => esc_html__('None', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Services Archive Page', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the archive page for the services post type', 'unitix-admin-td'),
                        'id'        => 'services_archive_page',
                        'type'      => 'select',
                        'options'  => 'taxonomy',
                        'taxonomy' => 'pages',
                        'default' =>  '',
                        'blank' => esc_html__('None', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Staff Archive Page', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the archive page for the staff post type', 'unitix-admin-td'),
                        'id'        => 'staff_archive_page',
                        'type'      => 'select',
                        'options'  => 'taxonomy',
                        'taxonomy' => 'pages',
                        'default' =>  '',
                        'blank' => esc_html__('None', 'unitix-admin-td'),
                    ),
                )
            ),
        )
    ));
    $oxy_theme->register_option_page( array(
        'page_title' => esc_html__('Advanced Theme Options', 'unitix-admin-td'),
        'menu_title' => esc_html__('Advanced', 'unitix-admin-td'),
        'slug'       => THEME_SHORT . '-advanced',
        'main_menu'  => false,
        'sections'   => array(
            'css-section' => array(
                'title'   => esc_html__('CSS', 'unitix-admin-td'),
                'header'  => esc_html__('Here you can save some custom CSS that will be loaded in the header. This will allow you to override some of the default styling of the theme.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Extra CSS (loaded last in the header)', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add extra CSS rules to be included in all pages', 'unitix-admin-td'),
                        'id'      => 'extra_css',
                        'type'    => 'textarea',
                        'attr'    => array( 'rows' => '10', 'style' => 'width:100%' ),
                        'default' => '',
                    ),
                    array(
                        'name'    => esc_html__('Swatch CSS Loading', 'unitix-admin-td'),
                        'desc'    => esc_html__('Defines where the dynamic swatch css that is created by your swatch options is saved. If you are using a lot of swatches it is recommended to save them to a file.', 'unitix-admin-td'),
                        'id'      => 'swatch_css_save',
                        'type'    => 'select',
                        'options' => array(
                            'file'  => esc_html__('Save swatches to files', 'unitix-admin-td'),
                            'head'  => esc_html__('Inject swatches in page header', 'unitix-admin-td'),
                        ),
                        'default' => 'head',
                    ),
                )
            ),
            'js-section' => array(
                'title'   => esc_html__('Javascript', 'unitix-admin-td'),
                'header'  => esc_html__('Here you can modify the theme advanced JS options', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Extra Javascript (loaded last in the header)', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add extra Javascript rules to be included in all pages that will be loaded in the header.  Code will be wrapped in script tags by default.', 'unitix-admin-td'),
                        'id'      => 'extra_js',
                        'type'    => 'textarea',
                        'attr'    => array( 'rows' => '10', 'style' => 'width:100%' ),
                        'default' => '',
                    ),
                )
            ),
            'assets-section' => array(
                'title'   => esc_html__('Assets', 'unitix-admin-td'),
                'header'  => esc_html__('Here you can choose the type of asset files enqueued by the theme.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Load Minified CSS and JS Assets', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose between minified and not minified theme CSS and Javascript files. Minified files are smaller and faster to load, while non-minified are easier to edit and mofify because they are more readable. Minified assets are enqueued by default.', 'unitix-admin-td'),
                        'id'      => 'minified_assets',
                        'type'    => 'radio',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                )
            ),
            'atom-section' => array(
                'title'   => esc_html__('Enable Atom Meta', 'unitix-admin-td'),
                'header'  => esc_html__('Here you can enable atom meta for posts author, title and date (used by search engines).', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Author', 'unitix-admin-td'),
                        'desc'    => esc_html__('Enable atom meta for posts author', 'unitix-admin-td'),
                        'id'      => 'atom_author',
                        'type'    => 'radio',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Title', 'unitix-admin-td'),
                        'desc'    => esc_html__('Enable atom meta for posts title', 'unitix-admin-td'),
                        'id'      => 'atom_title',
                        'type'    => 'radio',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    ),
                    array(
                        'name'    => esc_html__('Date', 'unitix-admin-td'),
                        'desc'    => esc_html__('Enable atom meta for posts date', 'unitix-admin-td'),
                        'id'      => 'atom_date',
                        'type'    => 'radio',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'on',
                    )
                )
            ),
            'favicon-section' => array(
                'title'   => esc_html__('Site Fav Icon', 'unitix-admin-td'),
                'header'  => esc_html__('The site Fav Icon is the icon that appears in the top corner of the browser tab, it is also used when saving bookmarks.  Upload your own custom Fav Icon here, recommended resolutions are 16x16 or 32x32.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                      'name' => esc_html__('Fav Icon', 'unitix-admin-td'),
                      'id'   => 'favicon',
                      'type' => 'upload',
                      'store' => 'url',
                      'desc' => esc_html__('Upload a Fav Icon for your site here', 'unitix-admin-td'),
                      'default' => OXY_THEME_URI . 'assets/images/favicons/favicon.ico',
                    ),
                )
            ),
            'apple-section' => array(
                'title'   => esc_html__('Apple Icons', 'unitix-admin-td'),
                'header'  => esc_html__('If someone saves a bookmark to their desktop on an Apple device this is the icon that will be used.  Here you can upload the icon you would like to be used on the various Apple devices.', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name' => esc_html__('iPhone Icon (60x60)', 'unitix-admin-td'),
                        'id'   => 'iphone_icon',
                        'type' => 'upload',
                        'store' => 'url',
                        'desc' => esc_html__('Upload an icon to be used by iPhone as a bookmark (60 x 60 pixels)', 'unitix-admin-td'),
                        'default' => OXY_THEME_URI . 'assets/images/favicons/apple-touch-icon-60x60-precomposed.png',
                    ),
                    array(
                        'name'    => esc_html__('iPhone -  Apply Apple style', 'unitix-admin-td'),
                        'desc'    => esc_html__('Allow device to apply styling to the icon?', 'unitix-admin-td'),
                        'id'      => 'iphone_icon_pre',
                        'type'    => 'radio',
                        'default' => 'apple-touch-icon',
                        'options' => array(
                            'apple-touch-icon'             => esc_html__('Allow Styling', 'unitix-admin-td'),
                            'apple-touch-icon-precomposed' => esc_html__('Leave It Alone', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                      'name' => esc_html__('iPhone Retina Icon (114x114)', 'unitix-admin-td'),
                      'id'   => 'iphone_retina_icon',
                      'type' => 'upload',
                      'store' => 'url',
                      'desc' => esc_html__('Upload an icon to be used by iPhone Retina as a bookmark (114 x 114 pixels)', 'unitix-admin-td'),
                      'default' => OXY_THEME_URI . 'assets/images/favicons/apple-touch-icon-114x114-precomposed.png',
                    ),
                    array(
                        'name'    => esc_html__('iPhone Retina -  Apply Apple style', 'unitix-admin-td'),
                        'desc'    => esc_html__('Allow device to apply styling to the icon?', 'unitix-admin-td'),
                        'id'      => 'iphone_retina_icon_pre',
                        'type'    => 'radio',
                        'default' => 'apple-touch-icon',
                        'options' => array(
                            'apple-touch-icon'             => esc_html__('Allow Styling', 'unitix-admin-td'),
                            'apple-touch-icon-precomposed' => esc_html__('Leave It Alone', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                      'name' => esc_html__('iPad Icon (72x72)', 'unitix-admin-td'),
                      'id'   => 'ipad_icon',
                      'type' => 'upload',
                      'store' => 'url',
                      'desc' => esc_html__('Upload an icon to be used by iPad as a bookmark (72 x 72 pixels)', 'unitix-admin-td'),
                      'default' => OXY_THEME_URI . 'assets/images/favicons/apple-touch-icon-72x72-precomposed.png',
                    ),
                    array(
                        'name'    => esc_html__('iPad -  Apply Apple style', 'unitix-admin-td'),
                        'desc'    => esc_html__('Allow device to apply styling to the icon?', 'unitix-admin-td'),
                        'id'      => 'ipad_icon_pre',
                        'type'    => 'radio',
                        'default' => 'apple-touch-icon',
                        'options' => array(
                            'apple-touch-icon'             => esc_html__('Allow Styling', 'unitix-admin-td'),
                            'apple-touch-icon-precomposed' => esc_html__('Leave It Alone', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                      'name' => esc_html__('iPad Retina Icon (144x144)', 'unitix-admin-td'),
                      'id'   => 'ipad_icon_retina',
                      'type' => 'upload',
                      'store' => 'url',
                      'desc' => esc_html__('Upload an icon to be used by iPad Retina as a bookmark (144 x 144 pixels)', 'unitix-admin-td'),
                      'default' => OXY_THEME_URI . 'assets/images/favicons/apple-touch-icon-144x144-precomposed.png',
                    ),
                    array(
                        'name'    => esc_html__('iPad -  Apply Apple style', 'unitix-admin-td'),
                        'desc'    => esc_html__('Allow device to apply styling to the icon?', 'unitix-admin-td'),
                        'id'      => 'ipad_icon_retina_pre',
                        'type'    => 'radio',
                        'default' => 'apple-touch-icon',
                        'options' => array(
                            'apple-touch-icon'             => esc_html__('Allow Styling', 'unitix-admin-td'),
                            'apple-touch-icon-precomposed' => esc_html__('Leave It Alone', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
            'mobile-section' => array(
                'title'   => esc_html__('Mobile', 'unitix-admin-td'),
                'header'  => esc_html__('Here you can configure settings targeted at mobile devices', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Background Videos', 'unitix-admin-td'),
                        'desc'    => esc_html__('Here you can enable section background videos for mobile. By default it is set to off in order to save bandwidth. Section background image will be displayed as a fallback', 'unitix-admin-td'),
                        'id'      => 'mobile_videos',
                        'type'    => 'radio',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                        'default' => 'off',
                    ),
                )
            ),
            'google-anal-section' => array(
                'title'   => esc_html__('Google Analytics', 'unitix-admin-td'),
                'header'  => esc_html__('Set your Google Analytics Tracker and keep track of visitors to your site(UA-XXXXX-X).', 'unitix-admin-td'),
                'fields' => array(
                    'google_anal' => array(
                        'name' => esc_html__('Google Analytics', 'unitix-admin-td'),
                        'desc' => esc_html__('Paste your google analytics code here', 'unitix-admin-td'),
                        'id' => 'google_anal',
                        'type' => 'text',
                        'default' => '',
                    )
                )
            )
        )
    ));
}

$oxy_theme->register_option_page( array(
    'page_title' => esc_html__('WooCommerce', 'unitix-admin-td'),
    'menu_title' => esc_html__('WooCommerce', 'unitix-admin-td'),
    'slug'       => THEME_SHORT . '-woocommerce',
    'main_menu'  => false,
    'icon'       => 'tools',
    'sections'   => array(
        'woo-general' => array(
            'title'   => esc_html__('General WooCommerce Page Options', 'unitix-admin-td'),
            'header'  => esc_html__('Change the way your shop page looks with these options.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('General Shop Swatch', 'unitix-admin-td'),
                    'desc'    => esc_html__('Choose a general colour scheme to use for your WooCommerce site.', 'unitix-admin-td'),
                    'id'      => 'woocom_general_swatch',
                    'type'    => 'select',
                    'default' => 'swatch-white-red',
                    'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                ),
                array(
                    'name'    => esc_html__('General Shop Decoration', 'unitix-admin-td'),
                    'desc'    => esc_html__('Choose a decoration style to use at the top of your shop pages.', 'unitix-admin-td'),
                    'id'      => 'woocom_general_decoration',
                    'type'    => 'select',
                    'default' => 'none',
                    'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
                ),
            )
        ),
        'woo-shop-section' => array(
            'title'   => esc_html__('Shop Page', 'unitix-admin-td'),
            'header'  => esc_html__('Change the way your shop page looks with these options.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Shop Layout', 'unitix-admin-td'),
                    'desc'    => esc_html__('Layout of your shop page. Choose among right sidebar, left sidebar, fullwidth layout', 'unitix-admin-td'),
                    'id'      => 'shop_layout',
                    'type'    => 'radio',
                    'options' => array(
                        'sidebar-right' => esc_html__('Right Sidebar', 'unitix-admin-td'),
                        'full-width'    => esc_html__('Full Width', 'unitix-admin-td'),
                        'sidebar-left'  => esc_html__('Left Sidebar', 'unitix-admin-td'),
                    ),
                    'default' => 'full-width',
                )
            )
        ),
        'woo-shop-checkout-sidebar' => array(
            'title'   => esc_html__('Checkout Sidebar', 'unitix-admin-td'),
            'header'  => esc_html__('Change the way your shop page looks with these options.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Checkout Slide Sidebar Swatch', 'unitix-admin-td'),
                    'desc'    => esc_html__('Choose a color swatch for the cart that slides in from the side.', 'unitix-admin-td'),
                    'id'      => 'pageslide_cart_swatch',
                    'type'    => 'select',
                    'default' => 'swatch-white-red',
                    'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                ),
            )
        ),
        'product-single-section' => array(
            'title'   => esc_html__('Single Product Page', 'unitix-admin-td'),
            'header'  => esc_html__('This section allows you to set up your social icons for single products.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Show Facebook Button', 'unitix-admin-td'),
                    'desc'    => esc_html__('Show facebook share button on your single product', 'unitix-admin-td'),
                    'id'      => 'woo_fb_show',
                    'type'    => 'radio',
                    'options' => array(
                        'show' => esc_html__('Show', 'unitix-admin-td'),
                        'hide' => esc_html__('Hide', 'unitix-admin-td'),
                    ),
                    'default' => 'show',
                ),
                array(
                    'name'    => esc_html__('Show Tweet Button', 'unitix-admin-td'),
                    'desc'    => esc_html__('Show tweet share button on your single product', 'unitix-admin-td'),
                    'id'      => 'woo_twitter_show',
                    'type'    => 'radio',
                    'options' => array(
                        'show' => esc_html__('Show', 'unitix-admin-td'),
                        'hide' => esc_html__('Hide', 'unitix-admin-td'),
                    ),
                    'default' => 'show',
                ),
                array(
                    'name'    => esc_html__('Show Google+ Button', 'unitix-admin-td'),
                    'desc'    => esc_html__('Show G+ share button on your single product', 'unitix-admin-td'),
                    'id'      => 'woo_google_show',
                    'type'    => 'radio',
                    'options' => array(
                        'show' => esc_html__('Show', 'unitix-admin-td'),
                        'hide' => esc_html__('Hide', 'unitix-admin-td'),
                    ),
                    'default' => 'show',
                ),
                array(
                    'name'    => esc_html__('Show Pinterest Button', 'unitix-admin-td'),
                    'desc'    => esc_html__('Show Pinterest share button on your single product', 'unitix-admin-td'),
                    'id'      => 'woo_pinterest_show',
                    'type'    => 'radio',
                    'options' => array(
                        'show' => esc_html__('Show', 'unitix-admin-td'),
                        'hide' => esc_html__('Hide', 'unitix-admin-td'),
                    ),
                    'default' => 'show',
                ),
                array(
                    'name'    => esc_html__('Show LinkedIn Button', 'unitix-admin-td'),
                    'desc'    => esc_html__('Show LinkedIn share button on your single product', 'unitix-admin-td'),
                    'id'      => 'woo_linkedin_show',
                    'type'    => 'radio',
                    'options' => array(
                        'show' => esc_html__('Show', 'unitix-admin-td'),
                        'hide' => esc_html__('Hide', 'unitix-admin-td'),
                    ),
                    'default' => 'show',
                )
            )
        ),
        'woo-cart-widget' => array(
            'title'   => esc_html__('Cart Popup', 'unitix-admin-td'),
            'header'  => esc_html__('Change the way your cart popup behaves with these options.', 'unitix-admin-td'),
            'fields' => array(
                 array(
                    'name'      => esc_html__('Cart Popup', 'unitix-admin-td'),
                    'id'        => 'woo_cart_popup',
                    'type'      => 'radio',
                    'default'   =>  'show',
                    'desc'    => esc_html__('If you choose show, cart popup will display when you click on the cart widget', 'unitix-admin-td'),
                    'options' => array(
                        'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        'show' => esc_html__('Show', 'unitix-admin-td'),
                    ),
                ),
            )
        ),
    )
));
$oxy_theme->register_option_page( array(
    'page_title' => esc_html__('Default Site Colours', 'unitix-admin-td'),
    'menu_title' => esc_html__('Colours', 'unitix-admin-td'),
    'slug'       => THEME_SHORT . '-default-colours',
    'main_menu'  => false,
    'icon'       => 'tools',
    'javascripts' => array(
        array(
            'handle' => 'default-swatches',
            'src'    => OXY_THEME_URI . 'inc/options/javascripts/pages/default-swatches.js',
            'deps'   => array( 'jquery' ),
            'localize' => array(
                'object_handle' => 'localData',
                'data' => array(
                    'ajaxurl' => admin_url( 'admin-ajax.php' ),
                    'installDefaultsNonce'  => wp_create_nonce( 'install-defaults' )
                )
            ),
        ),
    ),
    'sections'   => array(
        'default-swatch-section' => array(
            'title' => esc_html__('Default Swatches Install', 'unitix-admin-td'),
            'header'  => esc_html__('Re-install the themes default swatches here. Warning this will remove any modifications you have made to the default swatches', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'      =>  esc_html__('Re-install Default Swatches', 'unitix-admin-td'),
                    'button-text' => esc_html__('Install Defaults', 'unitix-admin-td'),
                    'desc'    => esc_html__('This button will reinstall all the default swatches for the site.', 'unitix-admin-td'),
                    'id'        => 'install_defaults',
                    'type'      => 'button',
                    'attr'        => array(
                        'id'    => 'install-default-swatches',
                        'class' => 'button button-primary'
                    ),
                ),
            )
        ),
        'save-all-swatch-section' => array(
            'title' => esc_html__('Save all swatches', 'unitix-admin-td'),
            'header'  => esc_html__('This option will re-save all your enabled swatches.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'      =>  esc_html__('Save All Swatches', 'unitix-admin-td'),
                    'button-text' => esc_html__('Save Swatches', 'unitix-admin-td'),
                    'desc'    => esc_html__('This button will re-save all swatches.', 'unitix-admin-td'),
                    'id'        => 'save_all_swatches',
                    'type'      => 'button',
                    'attr'        => array(
                        'id'    => 'save-all-swatches',
                        'class' => 'button button-primary'
                    ),
                ),
            )
        ),
        'default-button-colours-section' => array(
            'title'   => esc_html__('Default Button Colours', 'unitix-admin-td'),
        'header'  => esc_html__('Set the default bootstrap button colours here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Text Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_default_button_text',
                    'desc'    => esc_html__('Text colour to use for the default button.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_default_button_background',
                    'desc'    => esc_html__('Background colour to use for the default button.', 'unitix-admin-td'),
                    'default' => '#777777',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Background Hover Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_default_button_background_hover',
                    'desc'    => esc_html__('Background colour when user hovers over the default button.', 'unitix-admin-td'),
                    'default' => '#8B8B8B',
                    'type'    => 'colour',
                ),
            )
        ),
        'warning-button-colours-section' => array(
            'title'   => esc_html__('Warning Button Colours', 'unitix-admin-td'),
            'header'  => esc_html__('Set the warning bootstrap button colours here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Warning Button - Text Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_warning_button_text',
                    'desc'    => esc_html__('Text colour to use for the warning button.', 'unitix-admin-td'),
                    'default' => '#FFFFFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Warning Button - Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_warning_button_background',
                    'desc'    => esc_html__('Background colour to use for the warning button.', 'unitix-admin-td'),
                    'default' => '#F18D38',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Warning Button - Background Hover Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_warning_button_background_hover',
                    'desc'    => esc_html__('Background colour when user hovers over the warning button.', 'unitix-admin-td'),
                    'default' => '#E57211',
                    'type'    => 'colour',
                ),
            )
        ),
        'danger-button-colours-section' => array(
            'title'   => esc_html__('Danger Button Colours', 'unitix-admin-td'),
            'header'  => esc_html__('Set the danger bootstrap button colours here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Danger Button - Text Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_danger_button_text',
                    'desc'    => esc_html__('Text colour to use for the danger button.', 'unitix-admin-td'),
                    'default' => '#FFFFFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Danger Button - Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_danger_button_background',
                    'desc'    => esc_html__('Background colour to use for the danger button.', 'unitix-admin-td'),
                    'default' => '#E74C3C',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Danger Button - Background Hover Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_danger_button_background_hover',
                    'desc'    => esc_html__('Background colour when user hovers over the danger button.', 'unitix-admin-td'),
                    'default' => '#D62C1A',
                    'type'    => 'colour',
                ),
            )
        ),
        'success-button-colours-section' => array(
            'title'   => esc_html__('Success Button Colours', 'unitix-admin-td'),
            'header'  => esc_html__('Set the success bootstrap button colours here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Success Button - Text Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_success_button_text',
                    'desc'    => esc_html__('Text colour to use for the success button.', 'unitix-admin-td'),
                    'default' => '#FFFFFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Success Button - Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_success_button_background',
                    'desc'    => esc_html__('Background colour to use for the success button.', 'unitix-admin-td'),
                    'default' => '#427E77',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Success Button - Background Hover Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_success_button_background_hover',
                    'desc'    => esc_html__('Background colour when user hovers over the success button.', 'unitix-admin-td'),
                    'default' => '#305D57',
                    'type'    => 'colour',
                ),
            )
        ),
        'info-button-colours-section' => array(
            'title'   => esc_html__('Info Button Colours', 'unitix-admin-td'),
            'header'  => esc_html__('Set the info bootstrap button colours here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Info Button - Text Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_info_button_text',
                    'desc'    => esc_html__('Text colour to use for the info button.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Info Button - Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_info_button_background',
                    'desc'    => esc_html__('Background colour to use for the info button.', 'unitix-admin-td'),
                    'default' => '#5D89AC',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Info Button - Background Hover Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_info_button_background_hover',
                    'desc'    => esc_html__('Background colour when user hovers over the info button.', 'unitix-admin-td'),
                    'default' => '#486F8E',
                    'type'    => 'colour',
                ),
            )
        ),
        'icon-button-colours-section' => array(
            'title'   => esc_html__('Button Icon Colours', 'unitix-admin-td'),
            'header'  => esc_html__('Set the colours used for icons when used in buttons.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Button Icon Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_button_icon',
                    'desc'    => esc_html__('Text colour to use for icons when used inside buttons.', 'unitix-admin-td'),
                    'default' => '#FFFFFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Button Icon Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_button_icon_background',
                    'desc'    => esc_html__('Background colour to be used in fancy buttons.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Button icon Background Opacity %', 'unitix-admin-td'),
                    'desc'    => esc_html__('How see through is the overlay in percentage.', 'unitix-admin-td'),
                    'id'      => 'default_css_button_icon_background_alpha',
                    'type'    => 'slider',
                    'default' => 20,
                    'attr'    => array(
                        'max'  => 100,
                        'min'  => 0,
                        'step' => 1
                    )
                ),
            )
        ),
        'overlays-colours-section' => array(
            'title'   => esc_html__('Overlay Colours', 'unitix-admin-td'),
            'header'  => esc_html__('Set the colours used in overlay areas.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Overlay Text', 'unitix-admin-td'),
                    'id'      => 'default_css_overlay',
                    'desc'    => esc_html__('Text colour to text inside overlay areas.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Overlay Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_overlay_background',
                    'desc'    => esc_html__('Background colour to be used in overlay areas.', 'unitix-admin-td'),
                    'default' => '#000',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Overlay Background Opacity %', 'unitix-admin-td'),
                    'desc'    => esc_html__('How see through is the overlay in percentage.', 'unitix-admin-td'),
                    'id'      => 'default_css_overlay_background_alpha',
                    'type'    => 'slider',
                    'default' => 80,
                    'attr'    => array(
                        'max'  => 100,
                        'min'  => 0,
                        'step' => 1
                    )
                ),

            )
        ),
        'magnific-colours-section' => array(
            'title'   => esc_html__('Magnific (image lightbox) Colours ', 'unitix-admin-td'),
            'header'  => esc_html__('Set the colours used in overlay when an image preview is clicked.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Preview Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_magnific_background',
                    'desc'    => esc_html__('Background colour to be used in overlay areas.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Preview Background Opacity %', 'unitix-admin-td'),
                    'desc'    => esc_html__('How see through is the overlay in percentage.', 'unitix-admin-td'),
                    'id'      => 'default_css_magnific_background_alpha',
                    'type'    => 'slider',
                    'default' => 95,
                    'attr'    => array(
                        'max'  => 100,
                        'min'  => 0,
                        'step' => 1
                    )
                ),
                array(
                    'name'    => esc_html__('Close Button Icon Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_magnific_close_icon',
                    'desc'    => esc_html__('Text colour to use for preview close button icon.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Close  Button Icon Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_magnific_close_icon_background',
                    'desc'    => esc_html__('Background colour to be used for preview close button.', 'unitix-admin-td'),
                    'default' => '#E74C3C',
                    'type'    => 'colour',
                ),
            )
        ),
        'portfolio-colours-section' => array(
            'title'   => esc_html__('Portfolio Hover Colours ', 'unitix-admin-td'),
            'header'  => esc_html__('Set the colours used in portfolios when you hover over an item.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Hover Text', 'unitix-admin-td'),
                    'id'      => 'default_css_portfolio_hover_text',
                    'desc'    => esc_html__('Text colour to use inside hover .', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Hover Button Icon Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_portfolio_hover_button_icon',
                    'desc'    => esc_html__('Icon colour to use for bottom buttons shown on hover.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Hover Button Icon Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_portfolio_hover_button_background',
                    'desc'    => esc_html__('Background colour to use for bottom buttons shown on hover.', 'unitix-admin-td'),
                    'default' => '#E74C3C',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Hover Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_portfolio_hover_background',
                    'desc'    => esc_html__('Background colour to be used when user hovers over item.', 'unitix-admin-td'),
                    'default' => '#000',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Hover Background Opacity %', 'unitix-admin-td'),
                    'desc'    => esc_html__('How see through is the hover overlay in percentage.', 'unitix-admin-td'),
                    'id'      => 'default_css_portfolio_hover_background_alpha',
                    'type'    => 'slider',
                    'default' => 80,
                    'attr'    => array(
                        'max'  => 100,
                        'min'  => 0,
                        'step' => 1
                    )
                ),
            )
        ),
        'go-to-top-colours-section' => array(
            'title'   => esc_html__('Go to top button Colours ', 'unitix-admin-td'),
            'header'  => esc_html__('Set the colours used in go to top button that appears on scrolling a page.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Button Icon Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_gototop_icon',
                    'desc'    => esc_html__('Icon colour to use for go to top button.', 'unitix-admin-td'),
                    'default' => '#FFF',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Button Background Colour', 'unitix-admin-td'),
                    'id'      => 'default_css_gototop_background',
                    'desc'    => esc_html__('Background colour to use for go to top button.', 'unitix-admin-td'),
                    'default' => '#E32F1C',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Button Background Opacity %', 'unitix-admin-td'),
                    'desc'    => esc_html__('How see through is the go to top button in percentage.', 'unitix-admin-td'),
                    'id'      => 'default_css_gototop_background_alpha',
                    'type'    => 'slider',
                    'default' => 100,
                    'attr'    => array(
                        'max'  => 100,
                        'min'  => 0,
                        'step' => 1
                    )
                ),
            )
        ),
        'loader-colours-section' => array(
            'title'   => esc_html__('Loader Colours ', 'unitix-admin-td'),
            'header'  => esc_html__('Set the colours of the loader.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('Loader Colour', 'unitix-admin-td'),
                    'id'      => 'loader_color',
                    'desc'    => esc_html__('Color of the loader', 'unitix-admin-td'),
                    'default' => '#ffffff',
                    'type'    => 'colour',
                ),
                array(
                    'name'    => esc_html__('Loader background', 'unitix-admin-td'),
                    'id'      => 'loader_bg',
                    'desc'    => esc_html__('Background colour of the loader.', 'unitix-admin-td'),
                    'default' => '#e74c3c',
                    'format' => 'rgba',
                    'type'    => 'colour',
                )
            )
        )
    )
));
$oxy_theme->register_option_page(array(
    'page_title' => esc_html__('Typography Settings', 'unitix-admin-td'),
    'menu_title' => esc_html__('Typography', 'unitix-admin-td'),
    'slug'       => THEME_SHORT . '-typography',
    'main_menu'  => false,
    'icon'       => 'tools',
    'stylesheets' => array(
        array(
            'handle' => 'typography-page',
            'src'    => OXY_THEME_URI . 'vendor/oxygenna/oxygenna-typography/assets/css/typography-page.css',
            'deps'   => array('oxy-typography-select2', 'thickbox'),
        ),
    ),
    'javascripts' => array(
        array(
            'handle' => 'typography-page',
            'src'    => OXY_THEME_URI . 'vendor/oxygenna/oxygenna-typography/assets/javascripts/typography-page.js',
            'deps'   => array('jquery', 'underscore', 'thickbox', 'oxy-typography-select2', 'jquery-ui-dialog'),
            'localize' => array(
                'object_handle' => 'typographyPage',
                'data' => array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'listNonce'  => wp_create_nonce('list-fontstack'),
                    'fontModal'  => wp_create_nonce('font-modal'),
                    'updateNonce'  => wp_create_nonce('update-fontstack'),
                    'defaultFontsNonce' => wp_create_nonce('default-fonts'),
                )
            )
        ),
    ),
    // include a font stack option to enqueue select 2 ok
    'sections'   => array(
        'font-section' => array(
            'title'   => esc_html__('Fonts settings section', 'unitix-admin-td'),
            'header'  => esc_html__('Setup Fonts settings here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name' => esc_html__('Font Stack:', 'unitix-admin-td'),
                    'id' => 'font_list',
                    'type' => 'fontlist',
                    'class-file' => OXY_THEME_DIR . 'vendor/oxygenna/oxygenna-typography/inc/options/font-list.php',
                ),
            )
        )
    )
));

$oxy_theme->register_option_page(array(
    'page_title' => esc_html__('Fonts', 'unitix-admin-td'),
    'menu_title' => esc_html__('Fonts', 'unitix-admin-td'),
    'slug'       => THEME_SHORT . '-fonts',
    'main_menu'  => false,
    'icon'       => 'tools',
    'sections'   => array(
        'google-fonts-section' => array(
            'title'   => esc_html__('Google Fonts', 'unitix-admin-td'),
            // 'header'  => esc_html__('Setup Your Google Fonts Here.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'        => esc_html__('Fetch Google Fonts', 'unitix-admin-td'),
                    'button-text' => esc_html__('Update Fonts', 'unitix-admin-td'),
                    'id'          => 'google_update_fonts_button',
                    'type'        => 'button',
                    'desc'        => esc_html__('Click this button to fetch the latest fonts from Google and update your Google Fonts list.', 'unitix-admin-td'),
                    'attr'        => array(
                        'id'    => 'google-update-fonts-button',
                        'class' => 'button button-primary'
                    ),
                    'javascripts' => array(
                        array(
                            'handle' => 'google-font-updater',
                            'src'    => OXY_THEME_URI . 'vendor/oxygenna/oxygenna-typography/assets/javascripts/options/google-font-updater.js',
                            'deps'   => array('jquery'),
                            'localize' => array(
                                'object_handle' => 'googleUpdate',
                                'data' => array(
                                    'ajaxurl'   => admin_url('admin-ajax.php'),
                                    // generate a nonce with a unique ID "myajax-post-comment-nonce"
                                    // so that you can check it later when an AJAX request is sent
                                    'nonce'     => wp_create_nonce('google-fetch-fonts-nonce'),
                                )
                            )
                        ),
                    ),
                )
            )
        ),
        'typekit-provider-options' => array(
            'title'   => esc_html__('TypeKit Fonts', 'unitix-admin-td'),
            'header'  => esc_html__('If you have a TypeKit account and would like to use it in your site.  Enter your TypeKit API key below and then click the Update your kits button.', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name' => esc_html__('Typekit API Token', 'unitix-admin-td'),
                    'desc' => esc_html__('Add your typekit api token here', 'unitix-admin-td'),
                    'id'   => 'typekit_api_token',
                    'type' => 'text',
                    'attr'        => array(
                        'id'    => 'typekit-api-key',
                    )
                ),
                array(
                    'name'        => esc_html__('TypeKit Kits', 'unitix-admin-td'),
                    'button-text' => esc_html__('Update your kits', 'unitix-admin-td'),
                    'desc' => esc_html__('Click this button to update your typography list with the kits available from your TypeKit account.', 'unitix-admin-td'),
                    'id'          => 'typekit_kits_button',
                    'type'        => 'button',
                    'attr'        => array(
                        'id'    => 'typekit-kits-button',
                        'class' => 'button button-primary'
                    ),
                    'javascripts' => array(
                        array(
                            'handle' => 'typekit-kit-updater',
                            'src'    => OXY_THEME_URI . 'vendor/oxygenna/oxygenna-typography/assets/javascripts/options/typekit-updater.js',
                            'deps'   => array('jquery' ),
                            'localize' => array(
                                'object_handle' => 'typekitUpdate',
                                'data' => array(
                                    'ajaxurl'   => admin_url('admin-ajax.php'),
                                    'nonce'     => wp_create_nonce('typekit-kits-nonce'),
                                )
                            )
                        ),
                    ),
                )
            )
        )
    )
));
