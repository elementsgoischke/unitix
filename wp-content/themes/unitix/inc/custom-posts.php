<?php
/**
 */

function oxy_fetch_custom_columns($column) {
    global $post;
    switch( $column ) {
        case 'menu_order':
            echo esc_html($post->menu_order);
            echo '<input id="qe_slide_order_"' . $post->ID . '" type="hidden" value="' . $post->menu_order . '" />';
        break;

        case 'featured-image':
            $editlink = get_edit_post_link( $post->ID );
            echo '<a href="' . $editlink . '">' . get_the_post_thumbnail( $post->ID, 'thumbnail' ) . '</a>';
        break;

        case 'slideshows-category':
            echo get_the_term_list( $post->ID, 'oxy_slideshow_categories', '', ', ' );
        break;

        case 'service-category':
            echo get_the_term_list( $post->ID, 'oxy_service_category', '', ', ' );
        break;

        case 'departments-category':
            echo get_the_term_list( $post->ID, 'oxy_staff_department', '', ', ' );
        break;

        case 'job-title':
            echo get_post_meta( $post->ID, THEME_SHORT . '_position', true );
        break;

        case 'portfolio-category':
            echo get_the_term_list( $post->ID, 'oxy_portfolio_categories', '', ', ' );
        break;

        case 'swatch-status':
            $status = get_post_meta( $post->ID, THEME_SHORT . '_status', true );
            if( $status === 'enabled' ) {
                echo '<h4 class="swatch-status enabled">Swatch Enabled</h4>';
            }
            else {
                echo '<h4 class="swatch-status disabled">Swatch Disabled</h4>';
            }
        break;
        case 'testimonial-group':
            echo get_the_term_list( $post->ID, 'oxy_testimonial_group', '', ', ' );
        break;
        case 'testimonial-citation':
            echo get_post_meta( $post->ID, THEME_SHORT . '_citation', true );
        break;

        case 'swatch-preview':
            $header                 = get_post_meta( $post->ID, THEME_SHORT . '_header', true );
            $background             = get_post_meta( $post->ID, THEME_SHORT . '_background', true );
            $text                   = get_post_meta( $post->ID, THEME_SHORT . '_text', true );
            $icon                   = get_post_meta( $post->ID, THEME_SHORT . '_icon', true );
            $link                   = get_post_meta( $post->ID, THEME_SHORT . '_link', true );
            $link_hover             = get_post_meta( $post->ID, THEME_SHORT . '_link_hover', true );
            $foreground             = get_post_meta( $post->ID, THEME_SHORT . '_foreground', true );
            $overlay                = get_post_meta( $post->ID, THEME_SHORT . '_overlay', true );
            $form_background        = get_post_meta( $post->ID, THEME_SHORT . '_form_background', true );
            $form_text              = get_post_meta( $post->ID, THEME_SHORT . '_form_text', true );
            $form_active            = get_post_meta( $post->ID, THEME_SHORT . '_form_active', true );
            $form_button_background = get_post_meta( $post->ID, THEME_SHORT . '_primary_button_background', true );
            $form_button_text       = get_post_meta( $post->ID, THEME_SHORT . '_primary_button_text', true );
            ?>

            <div class="swatch-preview" style="background:<?php echo esc_attr($background); ?>">
                <h2 style="color:<?php echo esc_attr($header); ?>; text-align:center;">This is how a title will look</h2>
                <p style="color:<?php echo esc_attr($text); ?>; line-height: 1.5em;">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Perspiciatis, <a href="#" style="color:<?php echo esc_attr($link); ?>;" onmouseover="this.style.color='<?php echo esc_attr($link_hover); ?>'" onmouseout="this.style.color='<?php echo esc_attr($link); ?>'">Links will look like this neque quis cumque nobis</a> dolore provident unde hic aspernatur porro accusantium.
                    Ratione odit iste ducimus excepturi cupiditate amet similique laborum molestiae!
                </p>
                <p style="color:<?php echo esc_attr($text);?>; text-align:center; margin-bottom: 12px;">Icons will look like this <span class="icons" style="color: <?php echo esc_attr($icon); ?>; font-size: 2em; display: block; margin-top: 6px;">&hearts; &spades; &clubs; &diams;</span></p>
                <div style="background:<?php echo esc_attr($foreground); ?>; padding: 10px; margin-bottom: 12px;">
                    <p style="color:<?php echo esc_attr($background); ?>; margin-bottom: 0;">Foreground items will look like this</p>
                </div>
                <form style="padding: 0;">
                    <input type="text" value="This is a text field" style="background:<?php echo esc_attr($form_background); ?>; color:<?php echo esc_attr($form_text); ?>; padding: 5px; border: 0; border-radius: 0;"  />
                    <button style="color:<?php echo esc_attr($form_button_text); ?>; text-align:center; padding: 6px; border: 0; border-radius: 0; display: block; margin: 12px 0 0 0; background:<?php echo esc_attr($form_button_background); ?>;" >Thats a button</button>
                </form>
            </div>
        <?php
        break;

        default:
            // do nothing
        break;
    }
}
add_action( 'manage_posts_custom_column', 'oxy_fetch_custom_columns' );

/**
 * Slideshow Custom Post
 */

$labels = array(
    'name'               => esc_html__( 'Slideshow Images', 'unitix-admin-td' ),
    'singular_name'      => esc_html__( 'Slideshow Image', 'unitix-admin-td' ),
    'add_new'            => esc_html__( 'Add New', 'unitix-admin-td' ),
    'add_new_item'       => esc_html__( 'Add New Image', 'unitix-admin-td' ),
    'edit_item'          => esc_html__( 'Edit Image', 'unitix-admin-td' ),
    'new_item'           => esc_html__( 'New Image', 'unitix-admin-td' ),
    'view_item'          => esc_html__( 'View Image', 'unitix-admin-td' ),
    'search_items'       => esc_html__( 'Search Images', 'unitix-admin-td' ),
    'not_found'          => esc_html__( 'No images found', 'unitix-admin-td' ),
    'not_found_in_trash' => esc_html__( 'No images found in Trash', 'unitix-admin-td' ),
    'menu_name'          => esc_html__( 'Slider Images', 'unitix-admin-td' )
);

$args = array(
    'labels'    => $labels,
    'public'    => false,
    'show_ui'   => true,
    'query_var' => false,
    'rewrite'   => false,
    'menu_icon' => 'dashicons-slides',
    'supports'  => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
);

// create custom post
register_post_type( 'oxy_slideshow_image', $args );

// Register slideshow taxonomy
$labels = array(
    'name'          => esc_html__( 'Slideshows', 'unitix-admin-td' ),
    'singular_name' => esc_html__( 'Slideshow', 'unitix-admin-td' ),
    'search_items'  => esc_html__( 'Search Slideshows', 'unitix-admin-td' ),
    'all_items'     => esc_html__( 'All Slideshows', 'unitix-admin-td' ),
    'edit_item'     => esc_html__( 'Edit Slideshow', 'unitix-admin-td'),
    'update_item'   => esc_html__( 'Update Slideshow', 'unitix-admin-td'),
    'add_new_item'  => esc_html__( 'Add New Slideshow', 'unitix-admin-td'),
    'new_item_name' => esc_html__( 'New Slideshow Name', 'unitix-admin-td')
);

register_taxonomy(
    'oxy_slideshow_categories',
    'oxy_slideshow_image',
    array(
        'hierarchical' => true,
        'labels'       => $labels,
        'show_ui'      => true,
        'query_var'    => false,
        'rewrite'      => false
    )
);

// move featured image box on slideshow
function oxy_move_slideshow_meta_box() {
    remove_meta_box( 'postimagediv', 'oxy_slideshow_image', 'side' );
    add_meta_box('postimagediv', esc_html__('Slideshow Image', 'unitix-admin-td'), 'post_thumbnail_meta_box', 'oxy_slideshow_image', 'advanced', 'low');
}
add_action('do_meta_boxes', 'oxy_move_slideshow_meta_box');

function oxy_edit_columns_slideshow($columns) {
    $columns = array(
        'cb'                  => '<input type="checkbox" />',
        'title'               => esc_html__('Image Title', 'unitix-admin-td'),
        'featured-image'      => esc_html__('Image', 'unitix-admin-td'),
        'menu_order'          => esc_html__('Order', 'unitix-admin-td'),
        'slideshows-category' => esc_html__('Slideshows', 'unitix-admin-td'),
    );
    return $columns;
}
add_filter( 'manage_edit-oxy_slideshow_image_columns', 'oxy_edit_columns_slideshow' );


/* --------------------- SERVICES ------------------------*/

$labels = array(
    'name'               => esc_html__('Services', 'unitix-admin-td'),
    'singular_name'      => esc_html__('Service', 'unitix-admin-td'),
    'add_new'            => esc_html__('Add New', 'unitix-admin-td'),
    'add_new_item'       => esc_html__('Add New Service', 'unitix-admin-td'),
    'edit_item'          => esc_html__('Edit Service', 'unitix-admin-td'),
    'new_item'           => esc_html__('New Service', 'unitix-admin-td'),
    'all_items'          => esc_html__('All Services', 'unitix-admin-td'),
    'view_item'          => esc_html__('View Service', 'unitix-admin-td'),
    'search_items'       => esc_html__('Search Services', 'unitix-admin-td'),
    'not_found'          => esc_html__('No Service found', 'unitix-admin-td'),
    'not_found_in_trash' => esc_html__('No Service found in Trash', 'unitix-admin-td'),
    'menu_name'          => esc_html__('Services', 'unitix-admin-td')
);

// fetch service slug
$service_slug = trim( _x(oxy_get_option( 'services_slug' ), 'URL slug', 'unitix-admin-td' ));
if( empty($service_slug) ) {
    $service_slug = _x('our-services', 'URL slug', 'unitix-admin-td');
}

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'          => 'dashicons-flag',
    'supports'           => array( 'title', 'excerpt', 'editor', 'thumbnail', 'page-attributes', 'revisions' ),
    'rewrite'            => array( 'slug' => $service_slug, 'with_front' => true, 'pages' => true, 'feeds'=>false ),
);
register_post_type( 'oxy_service', $args );

$labels = array(
    'name'          => esc_html__( 'Categories', 'unitix-admin-td' ),
    'singular_name' => esc_html__( 'Category', 'unitix-admin-td' ),
    'search_items'  => esc_html__( 'Search Categories', 'unitix-admin-td' ),
    'all_items'     => esc_html__( 'All Categories', 'unitix-admin-td' ),
    'edit_item'     => esc_html__( 'Edit Category', 'unitix-admin-td'),
    'update_item'   => esc_html__( 'Update Category', 'unitix-admin-td'),
    'add_new_item'  => esc_html__( 'Add New Category', 'unitix-admin-td'),
    'new_item_name' => esc_html__( 'New Category Name', 'unitix-admin-td')
);

register_taxonomy(
    'oxy_service_category',
    'oxy_service',
    array(
        'hierarchical' => true,
        'labels'       => $labels,
        'show_ui'      => true,
    )
);

function oxy_edit_columns_services($columns) {
   // $columns['featured_image']= 'Featured Image';
    $columns = array(
        'cb'             => '<input type="checkbox" />',
        'title'          => esc_html__('Service', 'unitix-admin-td'),
        'featured-image' => esc_html__('Image', 'unitix-admin-td'),
        'service-category'     => esc_html__('Category', 'unitix-admin-td')
    );
    return $columns;
}
add_filter( 'manage_edit-oxy_service_columns', 'oxy_edit_columns_services' );

/* ------------------ TESTIMONIALS -----------------------*/

$labels = array(
    'name'               => esc_html__('Testimonial', 'unitix-admin-td'),
    'singular_name'      => esc_html__('Testimonial', 'unitix-admin-td'),
    'add_new'            => esc_html__('Add New', 'unitix-admin-td'),
    'add_new_item'       => esc_html__('Add New Testimonial', 'unitix-admin-td'),
    'edit_item'          => esc_html__('Edit Testimonial', 'unitix-admin-td'),
    'new_item'           => esc_html__('New Testimonial', 'unitix-admin-td'),
    'all_items'          => esc_html__('All Testimonial', 'unitix-admin-td'),
    'view_item'          => esc_html__('View Testimonial', 'unitix-admin-td'),
    'search_items'       => esc_html__('Search Testimonial', 'unitix-admin-td'),
    'not_found'          => esc_html__('No Testimonial found', 'unitix-admin-td'),
    'not_found_in_trash' => esc_html__('No Testimonial found in Trash', 'unitix-admin-td'),
    'menu_name'          => esc_html__('Testimonials', 'unitix-admin-td')
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'          => 'dashicons-format-quote',
    'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
);
register_post_type('oxy_testimonial', $args);

$labels = array(
    'name'          => esc_html__( 'Groups', 'unitix-admin-td' ),
    'singular_name' => esc_html__( 'Group', 'unitix-admin-td' ),
    'search_items'  => esc_html__( 'Search Groups', 'unitix-admin-td' ),
    'all_items'     => esc_html__( 'All Groups', 'unitix-admin-td' ),
    'edit_item'     => esc_html__( 'Edit Group', 'unitix-admin-td'),
    'update_item'   => esc_html__( 'Update Group', 'unitix-admin-td'),
    'add_new_item'  => esc_html__( 'Add New Group', 'unitix-admin-td'),
    'new_item_name' => esc_html__( 'New Group Name', 'unitix-admin-td')
);

register_taxonomy(
    'oxy_testimonial_group',
    'oxy_testimonial',
    array(
        'hierarchical' => true,
        'labels'       => $labels,
        'show_ui'      => true,
        'query_var'    => true,
    )
);

function oxy_edit_columns_testimonial($columns) {
   // $columns['featured_image']= 'Featured Image';
    $columns = array(
        'cb'                   => '<input type="checkbox" />',
        'title'                => esc_html__('Author', 'unitix-admin-td'),
        'featured-image'       => esc_html__('Image', 'unitix-admin-td'),
        'testimonial-citation' => esc_html__('Citation', 'unitix-admin-td'),
        'testimonial-group'    => esc_html__('Group', 'unitix-admin-td')
    );
    return $columns;
}
add_filter( 'manage_edit-oxy_testimonial_columns', 'oxy_edit_columns_testimonial' );


/* --------------------- STAFF ------------------------*/

// $labels = array(
//     'name'               => esc_html__('Staff', 'unitix-admin-td'),
//     'singular_name'      => esc_html__('Staff', 'unitix-admin-td'),
//     'add_new'            => esc_html__('Add New', 'unitix-admin-td'),
//     'add_new_item'       => esc_html__('Add New Staff', 'unitix-admin-td'),
//     'edit_item'          => esc_html__('Edit Staff', 'unitix-admin-td'),
//     'new_item'           => esc_html__('New Staff', 'unitix-admin-td'),
//     'all_items'          => esc_html__('All Staff', 'unitix-admin-td'),
//     'view_item'          => esc_html__('View Staff', 'unitix-admin-td'),
//     'search_items'       => esc_html__('Search Staff', 'unitix-admin-td'),
//     'not_found'          => esc_html__('No Staff found', 'unitix-admin-td'),
//     'not_found_in_trash' => esc_html__('No Staff found in Trash', 'unitix-admin-td'),
//     'menu_name'          => esc_html__('Staff', 'unitix-admin-td')
// );

// // fetch portfolio slug
// $staff_slug = trim( _x( oxy_get_option( 'staff_slug' ), 'URL slug', 'unitix-admin-td' )  );
// if( empty($staff_slug) ) {
//     $staff_slug = _x('staff', 'URL slug', 'unitix-admin-td');
// }

// $args = array(
//     'labels'             => $labels,
//     'public'             => true,
//     'publicly_queryable' => true,
//     'show_ui'            => true,
//     'show_in_menu'       => true,
//     'query_var'          => true,
//     'capability_type'    => 'post',
//     'has_archive'        => true,
//     'hierarchical'       => false,
//     'menu_position'      => null,
//     'menu_icon'          => 'dashicons-businessman',
//     'supports'           => array( 'title', 'excerpt', 'editor', 'thumbnail', 'page-attributes', 'revisions' ),
//     'rewrite' => array( 'slug' => $staff_slug, 'with_front' => true, 'pages' => true, 'feeds'=>false ),
// );
// register_post_type('oxy_staff', $args);

// $labels = array(
//     'name'          => esc_html__( 'Departments', 'unitix-admin-td' ),
//     'singular_name' => esc_html__( 'Department', 'unitix-admin-td' ),
//     'search_items'  =>  esc_html__( 'Search Departments', 'unitix-admin-td' ),
//     'all_items'     => esc_html__( 'All Departments', 'unitix-admin-td' ),
//     'edit_item'     => esc_html__( 'Edit Department', 'unitix-admin-td'),
//     'update_item'   => esc_html__( 'Update Department', 'unitix-admin-td'),
//     'add_new_item'  => esc_html__( 'Add New Department', 'unitix-admin-td'),
//     'new_item_name' => esc_html__( 'New Department Name', 'unitix-admin-td')
// );

// register_taxonomy(
//     'oxy_staff_department',
//     'oxy_staff',
//     array(
//         'hierarchical' => true,
//         'labels'       => $labels,
//         'show_ui'      => true,
//     )
// );

// function oxy_edit_columns_staff($columns) {
//    // $columns['featured_image']= 'Featured Image';
//     $columns = array(
//         'cb'                   => '<input type="checkbox" />',
//         'title'                => esc_html__('Name', 'unitix-admin-td'),
//         'featured-image'       => esc_html__('Image', 'unitix-admin-td'),
//         'job-title'            => esc_html__('Job Title', 'unitix-admin-td'),
//         'departments-category' => esc_html__('Department', 'unitix-admin-td')
//     );
//     return $columns;
// }
// add_filter( 'manage_edit-oxy_staff_columns', 'oxy_edit_columns_staff' );


/***************** PORTFOLIO *******************/

// $labels = array(
//     'name'               => esc_html__('Portfolio Items', 'unitix-admin-td'),
//     'singular_name'      => esc_html__('Portfolio Item', 'unitix-admin-td'),
//     'add_new'            => esc_html__('Add New', 'unitix-admin-td'),
//     'add_new_item'       => esc_html__('Add New Portfolio Item', 'unitix-admin-td'),
//     'edit_item'          => esc_html__('Edit Portfolio Item', 'unitix-admin-td'),
//     'new_item'           => esc_html__('New Portfolio Item', 'unitix-admin-td'),
//     'view_item'          => esc_html__('View Portfolio Item', 'unitix-admin-td'),
//     'search_items'       => esc_html__('Search Portfolio Items', 'unitix-admin-td'),
//     'not_found'          =>  esc_html__('No images found', 'unitix-admin-td'),
//     'not_found_in_trash' => esc_html__('No images found in Trash', 'unitix-admin-td'),
//     'parent_item_colon'  => '',
//     'menu_name'          => esc_html__('Portfolio Items', 'unitix-admin-td')
// );

// // fetch portfolio slug
// $permalink_slug = trim( _x( oxy_get_option( 'portfolio_slug' ), 'URL slug', 'unitix-admin-td' ) );
// if( empty($permalink_slug) ) {
//     $permalink_slug = _x('portfolio', 'URL slug', 'unitix-admin-td' );
// }

// $args = array(
//     'labels'             => $labels,
//     'public'             => true,
//     'publicly_queryable' => true,
//     'show_ui'            => true,
//     'query_var'          => true,
//     'has_archive'        => true,
//     'capability_type'    => 'post',
//     'hierarchical'       => false,
//     'menu_position'      => null,
//     'menu_icon'          => 'dashicons-portfolio',
//     'supports'           => array('title', 'excerpt', 'editor', 'thumbnail', 'page-attributes', 'post-formats' ),
//     'rewrite' => array( 'slug' => $permalink_slug, 'with_front' => true, 'pages' => true, 'feeds'=>false ),
// );

// // create custom post
// register_post_type( 'oxy_portfolio_image', $args );

// // Register portfolio taxonomy
// $labels = array(
//     'name'          => esc_html__( 'Categories', 'unitix-admin-td' ),
//     'singular_name' => esc_html__( 'Category', 'unitix-admin-td' ),
//     'search_items'  =>  esc_html__( 'Search Categories', 'unitix-admin-td' ),
//     'all_items'     => esc_html__( 'All Categories', 'unitix-admin-td' ),
//     'edit_item'     => esc_html__( 'Edit Category', 'unitix-admin-td'),
//     'update_item'   => esc_html__( 'Update Category', 'unitix-admin-td'),
//     'add_new_item'  => esc_html__( 'Add New Category', 'unitix-admin-td'),
//     'new_item_name' => esc_html__( 'New Category Name', 'unitix-admin-td')
// );

// register_taxonomy(
//     'oxy_portfolio_categories',
//     'oxy_portfolio_image',
//     array(
//         'hierarchical' => true,
//         'labels'       => $labels,
//         'show_ui'      => true,
//         'query_var'    => true,
//     )
// );

// $labels = array(
//     'name'          => esc_html__( 'Features', 'unitix-admin-td' ),
//     'singular_name' => esc_html__( 'Feature', 'unitix-admin-td' ),
//     'search_items'  =>  esc_html__( 'Search Features', 'unitix-admin-td' ),
//     'all_items'     => esc_html__( 'All Features', 'unitix-admin-td' ),
//     'edit_item'     => esc_html__( 'Edit Feature', 'unitix-admin-td' ),
//     'update_item'   => esc_html__( 'Update Feature', 'unitix-admin-td' ),
//     'add_new_item'  => esc_html__( 'Add New Feature', 'unitix-admin-td' ),
//     'new_item_name' => esc_html__( 'New Feature Name', 'unitix-admin-td' )
// );

// register_taxonomy(
//     'oxy_portfolio_features',
//     'oxy_portfolio_image',
//     array(
//         'hierarchical' => true,
//         'labels'       => $labels,
//         'show_ui'      => true,
//         'query_var'    => true,
//     )
// );

// function oxy_edit_columns_portfolio($columns) {
//    // $columns['featured_image']= 'Featured Image';
//     $columns = array(
//         'cb'                 => '<input type="checkbox" />',
//         'title'              => esc_html__('Item', 'unitix-admin-td'),
//         'featured-image'     => esc_html__('Image', 'unitix-admin-td'),
//         'menu_order'         => esc_html__('Order', 'unitix-admin-td'),
//         'portfolio-category' => esc_html__('Categories', 'unitix-admin-td')
//     );
//     return $columns;
// }
// add_filter( 'manage_edit-oxy_portfolio_image_columns', 'oxy_edit_columns_portfolio' );

// $labels = array(
//     'name'               => esc_html__('Mega Menu', 'unitix-admin-td'),
//     'singular_name'      => esc_html__('Mega Menu', 'unitix-admin-td'),
// );

// $args = array(
//     'labels'             => $labels,
//     'public'             => false,
//     'publicly_queryable' => false,
//     'show_ui'            => false,
//     'show_in_menu'       => true,
//     'query_var'          => false,
//     'show_in_nav_menus'  => true,
//     'capability_type'    => 'post',
//     'has_archive'        => false,
//     'hierarchical'       => false,
//     'menu_position'      => null,
// );
// register_post_type('oxy_mega_menu', $args);

// $labels = array(
//     'name'               => esc_html__('Mega Menu Columns', 'unitix-admin-td'),
//     'singular_name'      => esc_html__('Mega Menu Columns', 'unitix-admin-td'),
// );

// $args = array(
//     'labels'             => $labels,
//     'public'             => false,
//     'publicly_queryable' => false,
//     'show_ui'            => false,
//     'show_in_menu'       => false,
//     'query_var'          => false,
//     'show_in_nav_menus'  => true,
//     'capability_type'    => 'post',
//     'has_archive'        => false,
//     'hierarchical'       => false,
//     'menu_position'      => null,
// );
// register_post_type('oxy_mega_columns', $args);

/* --------------------- SWATCHES ------------------------*/

$labels = array(
    'name'               => esc_html__('Swatches', 'unitix-admin-td'),
    'singular_name'      => esc_html__('Swatch', 'unitix-admin-td'),
    'add_new'            => esc_html__('Add New', 'unitix-admin-td'),
    'add_new_item'       => esc_html__('Add New Swatch', 'unitix-admin-td'),
    'edit_item'          => esc_html__('Edit Swatch', 'unitix-admin-td'),
    'new_item'           => esc_html__('New Swatch', 'unitix-admin-td'),
    'all_items'          => esc_html__('All Swatches', 'unitix-admin-td'),
    'view_item'          => esc_html__('View Swatch', 'unitix-admin-td'),
    'search_items'       => esc_html__('Search Swatch', 'unitix-admin-td'),
    'not_found'          => esc_html__('No Swatch found', 'unitix-admin-td'),
    'not_found_in_trash' => esc_html__('No Swatch found in Trash', 'unitix-admin-td'),
    'menu_name'          => esc_html__('Swatches', 'unitix-admin-td')
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'exclude_from_search'=> true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'          => 'dashicons-admin-settings',
    'supports'           => array( 'title' )
);
register_post_type('oxy_swatch', $args);

function oxy_edit_columns_swatch($columns) {
   // $columns['featured_image']= 'Featured Image';
    $columns = array(
        'cb'             => '<input type="checkbox" />',
        'title'          => esc_html__('Swatch', 'unitix-admin-td'),
        'swatch-preview' => esc_html__('Preview', 'unitix-admin-td'),
        'swatch-status'  => esc_html__('Status', 'unitix-admin-td'),
    );
    return $columns;
}
add_filter( 'manage_edit-oxy_swatch_columns', 'oxy_edit_columns_swatch' );

/* --------------------- MODALS -----------------------*/

$labels = array(
    'name'               => esc_html__('Modal', 'unitix-admin-td'),
    'singular_name'      => esc_html__('Modal', 'unitix-admin-td'),
    'add_new'            => esc_html__('Add New', 'unitix-admin-td'),
    'add_new_item'       => esc_html__('Add New Modal', 'unitix-admin-td'),
    'edit_item'          => esc_html__('Edit Modal', 'unitix-admin-td'),
    'new_item'           => esc_html__('New Modal', 'unitix-admin-td'),
    'all_items'          => esc_html__('All Modals', 'unitix-admin-td'),
    'view_item'          => esc_html__('View Modal', 'unitix-admin-td'),
    'search_items'       => esc_html__('Search Modal', 'unitix-admin-td'),
    'not_found'          => esc_html__('No Modals found', 'unitix-admin-td'),
    'not_found_in_trash' => esc_html__('No Modals found in Trash', 'unitix-admin-td'),
    'menu_name'          => esc_html__('Modals', 'unitix-admin-td')
);

$args = array(
    'labels'             => $labels,
    'public'             => true,
    'publicly_queryable' => true,
    'exclude_from_search'=> true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'menu_icon'          => 'dashicons-lightbulb',
    'supports'           => array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions')
);
register_post_type('oxy_modal', $args);
