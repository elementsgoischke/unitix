<?php
/**
 * Test Options Page
 *
 * @package Unitix
 * @subpackage options-pages
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

return array(
    'sections'   => array(
        'twitter-section' => array(
            'fields' => array(
                array(
                    'name' => esc_html__('Show language as', 'unitix-admin-td'),
                    'id' => 'display',
                    'type' => 'select',
                    'default' => 'name',
                    'options' => array(
                        'name'     => esc_html__('Name', 'unitix-admin-td'),
                        'code'     => esc_html__('Country Code', 'unitix-admin-td'),
                        'flag'     => esc_html__('Flag', 'unitix-admin-td'),
                        'nameflag' => esc_html__('Name & Flag', 'unitix-admin-td')
                    )
                ),
                array(
                    'name' => esc_html__('Order languages by', 'unitix-admin-td'),
                    'id' => 'order',
                    'type' => 'select',
                    'default' => 'id',
                    'options' => array(
                        'id'   => esc_html__('ID', 'unitix-admin-td'),
                        'code' => esc_html__('Code', 'unitix-admin-td'),
                        'name' => esc_html__('Name', 'unitix-admin-td')
                    ),
                ),
                array(
                    'name' => esc_html__('Order direction', 'unitix-admin-td'),
                    'id' => 'orderby',
                    'type' => 'select',
                    'default' => 'id',
                    'options' => array(
                        'asc'   => esc_html__('Ascending', 'unitix-admin-td'),
                        'desc' => esc_html__('Decending', 'unitix-admin-td'),
                    ),
                ),
                array(
                    'name' => esc_html__('Skip Missing Languages', 'unitix-admin-td'),
                    'id' => 'skip_missing',
                    'type' => 'select',
                    'default' => '1',
                    'options' => array(
                        '1' => esc_html__('Skip', 'unitix-admin-td'),
                        '0' => esc_html__('Dont Skip', 'unitix-admin-td'),
                    ),
                    'desc' => esc_html__('Skip languages with no translations.', 'unitix-admin-td')
                ),
            )//fields
        )//section
    )//sections
);//array
