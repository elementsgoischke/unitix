<?php
/**
 * Test Options Page
 *
 * @package Unitix
 * @subpackage options-pages
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

return array(
    'sections'   => array(
        'twitter-section' => array(
            'title'   => esc_html__('Twitter', 'unitix-admin-td'),
            'header'  => esc_html__('Twitter feed options','unitix-admin-td'),
            'fields' => array(
                'title' => array(
                    'name' => esc_html__('Widget Title', 'unitix-admin-td'),
                    'id' => 'title',
                    'type' => 'text',
                    'default' => "",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),
                'account' => array(
                    'name' => esc_html__('Twitter username', 'unitix-admin-td'),
                    'id' => 'account',
                    'type' => 'text',
                    'default' => "envato",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),
                'consumer_key' => array(
                    'name' => esc_html__('Consumer Key', 'unitix-admin-td'),
                    'id' => 'consumer_key',
                    'type' => 'text',
                    'default' => "",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),
                'consumer_secret' => array(
                    'name' => esc_html__('Consumer Secret', 'unitix-admin-td'),
                    'id' => 'consumer_secret',
                    'type' => 'text',
                    'default' => "",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),
                'access_token' => array(
                    'name' => esc_html__('Access Token', 'unitix-admin-td'),
                    'id' => 'access_token',
                    'type' => 'text',
                    'default' => "",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),
                'access_token_secret' => array(
                    'name' => esc_html__('Access Token Secret', 'unitix-admin-td'),
                    'id' => 'access_token_secret',
                    'type' => 'text',
                    'default' => "",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),

                'show'   => array(
                    'name'       =>  esc_html__('Maximum number of tweets to show', 'unitix-admin-td'),
                    'id'         => 'show',
                    'type'       => 'select',
                    'options'    =>  array(
                              1  => 1,
                              2  => 2,
                              3  => 3,
                              4  => 4,
                              5  => 5,
                              6  => 6,
                              7  => 7,
                              8  => 8,
                              9  => 9,
                              10 => 10
                    ),
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                    'default'   => 5,
                ),


                'hidereplies' => array(
                    'name'      => esc_html__('Hide replies', 'unitix-admin-td'),
                    'id'        => 'hidereplies',
                    'type'      => 'radio',
                    'default'   =>  'on',
                    'options' => array(
                        'on'   => esc_html__('Hide', 'unitix-admin-td'),
                        'off'  => esc_html__('Show', 'unitix-admin-td'),
                    ),
                ),

                'hidepublicized' => array(
                    'name'      => esc_html__('Hide Tweets pushed by Publicize', 'unitix-admin-td'),
                    'id'        => 'hidepublicized',
                    'type'      => 'radio',
                    'default'   =>  'on',
                    'options' => array(
                        'on'   => esc_html__('Hide', 'unitix-admin-td'),
                        'off'  => esc_html__('Show', 'unitix-admin-td'),
                    ),
                ),

                'includeretweets' => array(
                    'name'      => esc_html__('Include retweets', 'unitix-admin-td'),
                    'id'        => 'include_retweets',
                    'type'      => 'radio',
                    'default'   =>  'on',
                    'options' => array(
                        'off' => esc_html__('No', 'unitix-admin-td'),
                        'on'  => esc_html__('Yes', 'unitix-admin-td'),
                    ),
                ),

                'followbutton' => array(
                    'name'      => esc_html__('Display Follow Button', 'unitix-admin-td'),
                    'id'        => 'follow_button',
                    'type'      => 'radio',
                    'default'   =>  'on',
                    'options' => array(
                        'off' => esc_html__('Hide', 'unitix-admin-td'),
                        'on'  => esc_html__('Show', 'unitix-admin-td'),
                    ),
                ),

                'beforetimesince' => array(
                    'name' => esc_html__('Text to display between Tweet and timestamp:', 'unitix-admin-td'),
                    'id' => 'beforetimesince',
                    'type' => 'text',
                    'default' => "",
                    'attr'      =>  array(
                        'class'    => 'widefat',
                    ),
                ),

            )//fields
        )//section
    )//sections
);//array
