<?php
/**
 * Themes animations go here
 *
 * @package Unitix
 * @subpackage Core
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */


return array(
    'none'               => esc_html__('No Animation', 'unitix-admin-td'),
    'bounce'             => esc_html__('Bounce', 'unitix-admin-td'),
    'bounceIn'           => esc_html__('BounceIn', 'unitix-admin-td'),
    'bounceInDown'       => esc_html__('BounceInDown', 'unitix-admin-td'),
    'bounceInLeft'       => esc_html__('BounceInLeft', 'unitix-admin-td'),
    'bounceInRight'      => esc_html__('BounceInRight', 'unitix-admin-td'),
    'bounceInUp'         => esc_html__('BounceInUp', 'unitix-admin-td'),
    'bounceOut'          => esc_html__('BounceOut', 'unitix-admin-td'),
    'bounceOutDown'      => esc_html__('BounceOutDown', 'unitix-admin-td'),
    'bounceOutLeft'      => esc_html__('BounceOutLeft', 'unitix-admin-td'),
    'bounceOutRight'     => esc_html__('BounceOutRight', 'unitix-admin-td'),
    'bounceOutUp'        => esc_html__('BounceOutUp', 'unitix-admin-td'),
    'fadeIn'             => esc_html__('FadeIn', 'unitix-admin-td'),
    'fadeInDown'         => esc_html__('FadeInDown', 'unitix-admin-td'),
    'fadeInDownBig'      => esc_html__('FadeInDownBig', 'unitix-admin-td'),
    'fadeInLeft'         => esc_html__('FadeInLeft', 'unitix-admin-td'),
    'fadeInLeftBig'      => esc_html__('FadeInLeftBig', 'unitix-admin-td'),
    'fadeInRight'        => esc_html__('FadeInRight', 'unitix-admin-td'),
    'fadeInRightBig'     => esc_html__('FadeInRightBig', 'unitix-admin-td'),
    'fadeInUp'           => esc_html__('FadeInUp', 'unitix-admin-td'),
    'fadeInUpBig'        => esc_html__('FadeInUpBig', 'unitix-admin-td'),
    'fadeOut'            => esc_html__('FadeOut', 'unitix-admin-td'),
    'fadeOutDown'        => esc_html__('FadeOutDown', 'unitix-admin-td'),
    'fadeOutDownBig'     => esc_html__('FadeOutDownBig', 'unitix-admin-td'),
    'fadeOutLeft'        => esc_html__('FadeOutLeft', 'unitix-admin-td'),
    'fadeOutLeftBig'     => esc_html__('FadeOutLeftBig', 'unitix-admin-td'),
    'fadeOutRight'       => esc_html__('FadeOutRight', 'unitix-admin-td'),
    'fadeOutRightBig'    => esc_html__('FadeOutRightBig', 'unitix-admin-td'),
    'fadeOutUp'          => esc_html__('FadeOutUp', 'unitix-admin-td'),
    'fadeOutUpBig'       => esc_html__('FadeOutUpBig', 'unitix-admin-td'),
    'flash'              => esc_html__('Flash', 'unitix-admin-td'),
    'flip'               => esc_html__('Flip', 'unitix-admin-td'),
    'flipInX'            => esc_html__('FlipInX', 'unitix-admin-td'),
    'flipInY'            => esc_html__('FlipInY', 'unitix-admin-td'),
    'flipOutX'           => esc_html__('FlipOutX', 'unitix-admin-td'),
    'flipOutY'           => esc_html__('FlipOutY', 'unitix-admin-td'),
    'hinge'              => esc_html__('Hinge', 'unitix-admin-td'),
    'lightSpeedIn'       => esc_html__('LightSpeedIn', 'unitix-admin-td'),
    'lightSpeedOut'      => esc_html__('LightSpeedOut', 'unitix-admin-td'),
    'pulse'              => esc_html__('Pulse', 'unitix-admin-td'),
    'rollIn'             => esc_html__('RollIn', 'unitix-admin-td'),
    'rollOut'            => esc_html__('PollOut', 'unitix-admin-td'),
    'rotateIn'           => esc_html__('RotateIn', 'unitix-admin-td'),
    'rotateInDownLeft'   => esc_html__('RotateInDownLeft', 'unitix-admin-td'),
    'rotateInDownRight'  => esc_html__('RotateInDownRight', 'unitix-admin-td'),
    'rotateInUpLeft'     => esc_html__('RotateInUpLeft', 'unitix-admin-td'),
    'rotateInUpRight'    => esc_html__('RotateInUpRight', 'unitix-admin-td'),
    'rotateOut'          => esc_html__('RotateOut', 'unitix-admin-td'),
    'rotateOutDownLeft'  => esc_html__('RotateOutDownLeft', 'unitix-admin-td'),
    'rotateOutDownRight' => esc_html__('RotateOutDownRight', 'unitix-admin-td'),
    'rotateOutUpLeft'    => esc_html__('RotateOutUpLeft', 'unitix-admin-td'),
    'rotateOutUpRight'   => esc_html__('RotateOutUpRight', 'unitix-admin-td'),
    'rubberBand'         => esc_html__('RubberBand', 'unitix-admin-td'),
    'shake'              => esc_html__('Shake', 'unitix-admin-td'),
    'swing'              => esc_html__('Swing', 'unitix-admin-td'),
    'tada'               => esc_html__('Tada', 'unitix-admin-td'),
    'wiggle'             => esc_html__('Wiggle', 'unitix-admin-td'),
    'wobble'             => esc_html__('Wobble', 'unitix-admin-td'),
    'zoomIn'             => esc_html__('ZoomIn', 'unitix-admin-td'),
    'zoomInDown'         => esc_html__('ZoomInDown', 'unitix-admin-td'),
    'zoomInLeft'         => esc_html__('ZoomInLeft', 'unitix-admin-td'),
    'zoomInRight'        => esc_html__('ZoomInRight', 'unitix-admin-td'),
    'zoomInUp'           => esc_html__('ZoomInUp', 'unitix-admin-td'),
    'zoomOut'            => esc_html__('ZoomOut', 'unitix-admin-td'),
    'zoomOutDown'        => esc_html__('ZoomOutDown', 'unitix-admin-td'),
    'zoomOutLeft'        => esc_html__('ZoomOutLeft', 'unitix-admin-td'),
    'zoomOutRight'       => esc_html__('ZoomOutRight', 'unitix-admin-td'),
    'zoomOutUp'          => esc_html__('ZoomOutUp', 'unitix-admin-td')
);