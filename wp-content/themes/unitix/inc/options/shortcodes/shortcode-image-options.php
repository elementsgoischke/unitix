<?php
/**
 * Themes shortcode image options go here
 *
 * @package Unitix
 * @subpackage Core
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

return array(
        'title' => esc_html__('Image options', 'unitix-admin-td'),
        'fields' => array(
            array(
                'name'    => esc_html__('Image Shape', 'unitix-admin-td'),
                'desc'    => esc_html__('Choose the shape of the image', 'unitix-admin-td'),
                'id'      => 'image_shape',
                'type'    => 'select',
                'options' => array(
                    'box-round'    => esc_html__('Round', 'unitix-admin-td'),
                    'box-rect'     => esc_html__('Rectunitix', 'unitix-admin-td'),
                    'box-square'   => esc_html__('Square', 'unitix-admin-td'),
                ),
                'default' => 'box-round',
            ),
            array(
                'name'    => esc_html__('Image Size', 'unitix-admin-td'),
                'desc'    => esc_html__('Choose the size of the image', 'unitix-admin-td'),
                'id'      => 'image_size',
                'type'    => 'select',
                'options' => array(
                    'box-mini'    => esc_html__('Mini', 'unitix-admin-td'),
                    'no-small'    => esc_html__('Small', 'unitix-admin-td'),
                    'box-medium'  => esc_html__('Medium', 'unitix-admin-td'),
                    'box-big'     => esc_html__('Big', 'unitix-admin-td'),
                    'box-huge'    => esc_html__('Huge', 'unitix-admin-td'),
                ),
                'default' => 'box-medium',
            ),
        )
);