<?php

/**

 * Themes shortcode options go here

 *

 * @package Unitix

 * @subpackage Core

 * @since 1.0

 *

 * @copyright (c) 2014 Oxygenna.com

 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/

 * @version 1.18.7

 */



return array(

    'title' => esc_html__('Row Options', 'unitix-admin-td'),

    'fields' => array(

        array(

            'name'    => esc_html__('Swatch', 'unitix-admin-td'),

            'desc'    => esc_html__('Choose a color swatch for the section', 'unitix-admin-td'),

            'id'      => 'swatch',

            'type'    => 'select',

            'default' => 'swatch-white-red',

            'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php',

            'admin_label' => true,

        ),

        array(

            'name'    => esc_html__('Top Decoration', 'unitix-admin-td'),

            'desc'    => esc_html__('Choose a style to use as the top decoration.', 'unitix-admin-td'),

            'id'      => 'top_decoration',

            'type'    => 'select',

            'default' => 'none',

            'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',

        ),

        array(

            'name'    => esc_html__('Bottom Decoration', 'unitix-admin-td'),

            'desc'    => esc_html__('Choose a style to use as the bottom decoration.', 'unitix-admin-td'),

            'id'      => 'bottom_decoration',

            'type'    => 'select',

            'default' => 'none',

            'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',

        ),

        //hidden for now

        array(

            'name'    => '',

            'id'      => 'content',

            'type'    => 'hiddentext',

            'default' => '',

            'desc'    => ''

        ),

        array(

            'name'    => esc_html__('Section Padding', 'unitix-admin-td'),

            'desc'    => esc_html__('Choose the amount of padding added to the height of this section', 'unitix-admin-td'),

            'id'      => 'height',

            'type'    => 'select',

            'options' => array(

                'normal'     => esc_html__('Normal', 'unitix-admin-td'),

                'short'      => esc_html__('Short', 'unitix-admin-td'),

                'tiny'       => esc_html__('Tiny', 'unitix-admin-td'),

                'nopadding' => esc_html__('No Padding', 'unitix-admin-td'),

            ),

            'default' => 'normal',

        ),

        array(

            'name'      =>  esc_html__('Section height', 'unitix-admin-td'),

            'desc'    => esc_html__('Make the section fullheight( have a height at minimum as the screen size )', 'unitix-admin-td'),

            'id'        => 'fullheight',

            'type'      => 'select',

            'options'   =>  array(

                'on'  => esc_html__('Enabled', 'unitix-admin-td'),

                'off' => esc_html__('Disabled', 'unitix-admin-td'),

            ),

            'default'   => 'off',

        ),

        array(

            'name'    => esc_html__('Section Width', 'unitix-admin-td'),

            'desc'    => esc_html__('Choose between padded-non padded section', 'unitix-admin-td'),

            'id'      => 'width',

            'type'    => 'select',

            'options' => array(

                'padded'    => esc_html__('Padded', 'unitix-admin-td'),

                'no-padded' => esc_html__('Full Width', 'unitix-admin-td'),

            ),

            'default' => 'padded',

        ),

        array(

            'name'    => esc_html__('Optional class', 'unitix-admin-td'),

            'id'      => 'class',

            'type'    => 'text',

            'default' => '',

            'desc'    => esc_html__('Add an optional class to the section', 'unitix-admin-td'),

        ),

        array(

            'name'    => esc_html__('Optional id', 'unitix-admin-td'),

            'id'      => 'id',

            'type'    => 'text',

            'default' => '',

            'desc'    => esc_html__('Add an optional id to the section', 'unitix-admin-td'),

        ),

        array(

            'name'    => esc_html__('Section Content Vertical Alignment', 'unitix-admin-td'),

            'desc'    => esc_html__('Align the content of the section vertically', 'unitix-admin-td'),

            'id'      => 'vertical_alignment',

            'type'    => 'radio',

            'options' => array(

                'top'       => esc_html__('Top', 'unitix-admin-td'),

                'middle'    => esc_html__('Middle', 'unitix-admin-td'),

                'bottom'    => esc_html__('Bottom', 'unitix-admin-td'),

            ),

            'default' => 'top',

        )

    )

);

