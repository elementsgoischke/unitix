<?php
/**
 * Creates theme shortcode options
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license **LICENSE**
 * @version 1.18.7
 */

global $oxy_theme;
if( isset( $oxy_theme ) ) {
    $shortcode_options = include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-options.php';
    // add options to accordion shortcode
    foreach( $shortcode_options as &$shortcode ) {
        if( isset($shortcode['shortcode']) && $shortcode['shortcode'] == 'socialicons') {
            for( $i = 0 ; $i < 10 ; $i++ ) {
                $shortcode['sections'][0]['fields'][] =  array(
                    'name'    => sprintf( esc_html__('Social Icon %s', 'unitix-admin-td'), $i+1 ),
                    'id'      => sprintf( esc_html__('social_icon_%s', 'unitix-admin-td'), $i+1 ),
                    'type'    => 'select',
                    'options' => 'social_icons',
                    'default' => '',
                    'blank'   => esc_html__('Choose a social network icon', 'unitix-admin-td'),
                    'attr'    =>  array(
                        'class'    => 'widefat',
                    ),
                );
                $shortcode['sections'][0]['fields'][] =  array(
                    'name'    => sprintf( esc_html__('Social Icon %s URL', 'unitix-admin-td'), $i+1 ),
                    'id'      => sprintf( esc_html__('social_icon_%s_url', 'unitix-admin-td'), $i+1 ),
                    'type'    => 'text',
                    'default' => '',
                    'attr'    =>  array(
                        'class'    => 'widefat',
                    ),
                );
            }
        }
    }

    $oxy_theme->register_shortcode_options($shortcode_options);
}