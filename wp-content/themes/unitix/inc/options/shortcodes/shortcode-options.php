<?php
/**
 * Sets up all theme shortcode options
 *
 * @package Unitix
 * @subpackage Frontend
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license **LICENSE**
 * @version 1.18.7
 */
$heading_options = include OXY_THEME_DIR . 'inc/options/global-options/header-options.php';
$heading_options[0]['name'] = esc_html__('Heading', 'unitix-admin-td');
$heading_options[0]['id'] = 'content';

return array(
    // SECTION
    'vc_row' => array(
        'shortcode'     => 'vc_row',
        'title'         => esc_html__('Row', 'unitix-admin-td'),
        'desc'          => esc_html__('A Horizontal section to add content to.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => true,
        'sections'      => array(
            array(
                'title' => esc_html__('Row Header', 'unitix-admin-td'),
                'fields' => include OXY_THEME_DIR . 'inc/options/global-options/section-header-text.php',
            ),
            include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-section-options.php',
            array(
                'title' => esc_html__('Row Background', 'unitix-admin-td'),
                'fields' => include OXY_THEME_DIR . 'inc/options/global-options/section-background-image.php'
            )
        )
    ),
    'vc_row_inner' => array(
        'shortcode'     => 'vc_row_inner',
        'title'         => esc_html__('Row Inner', 'unitix-admin-td'),
        'desc'          => esc_html__('A Nested Horizontal section to add content to.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => true,
        'sections'      => array(
            array(
                'title' => esc_html__('General', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Extra Classes', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add any extra classes you need to add to this column. ( space separated )', 'unitix-admin-td'),
                        'id'      => 'extra_classes',
                        'default'     =>  '',
                        'type'        => 'text',
                    ),
                )
            )
        )
    ),
    // SECTION
    'vc_column' => array(
        'shortcode'     => 'vc_column',
        'title'         => esc_html__('Column', 'unitix-admin-td'),
        'desc'          => esc_html__('Column shortcode for use inside a row.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => true,
        'sections'      => array(
            array(
                'title' => esc_html__('General', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Column Alignment', 'unitix-admin-td'),
                        'id'        => 'align',
                        'type'      => 'select',
                        'default'   => 'default',
                        'options' => array(
                            'Default' => esc_html__('Default (no class)', 'unitix-admin-td'),
                            'left'    => esc_html__('Left', 'unitix-admin-td'),
                            'center'  => esc_html__('Center', 'unitix-admin-td'),
                            'right'   => esc_html__('Right', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Sets the alignment items in the column.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Extra Classes', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add any extra classes you need to add to this column. ( space separated )', 'unitix-admin-td'),
                        'id'      => 'extra_classes',
                        'default'     =>  '',
                        'type'        => 'text',
                    ),
                )
            ),
            array(
                'title' => esc_html__('On Scroll Animation', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      =>  esc_html__('On Scroll Animation', 'unitix-admin-td'),
                        'id'        => 'os_animation_enabled',
                        'type'      => 'select',
                        'options'   =>  array(
                            'on'  => esc_html__('Enabled', 'unitix-admin-td'),
                            'off' => esc_html__('Disabled', 'unitix-admin-td'),
                        ),
                        'default'   => 'off',
                    ),
                    array(
                        'name'      => esc_html__('Animation Delay', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the speed of animations', 'unitix-admin-td'),
                        'id'        => 'os_animation_delay',
                        'type'      => 'slider',
                        'default'   => '0.1',
                        'attr'      => array(
                            'max'       => 4.0,
                            'min'       => 0.1,
                            'step'      => 0.1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Animation Effect', 'unitix-admin-td'),
                        'desc'    => esc_html__('Set the effect of the animation', 'unitix-admin-td'),
                        'id'      => 'os_animation',
                        'type'    => 'select',
                        'default' => 'bounce',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
                    ),
                )
            )
        )
    ),
    'heading' => array(
        'shortcode'     => 'heading',
        'title'         => esc_html__('Heading', 'unitix-admin-td'),
        'desc'          => esc_html__('Creates a heading tag H1-H6', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => true,
        'sections'      => array(
            array(
                'title' => esc_html__('Header', 'unitix-admin-td'),
                'fields' => $heading_options
            ),
        )
    ),
    'services' =>array(
        'shortcode'     => 'services',
        'title'         => esc_html__('Services', 'unitix-admin-td'),
        'desc'          => esc_html__('Displays a horizontal / vertical list of services.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Services', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Choose a category', 'unitix-admin-td'),
                        'desc'    => esc_html__('Category of services to show', 'unitix-admin-td'),
                        'id'      => 'category',
                        'default' =>  '',
                        'admin_label' => true,
                        'type'    => 'select',
                        'options' => 'taxonomy',
                        'taxonomy' => 'oxy_service_category',
                        'blank_label' => esc_html__('All Categories', 'unitix-admin-td')
                    ),
                    array(
                        'name'    => esc_html__('Services Count', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of services to show(set to 0 to show all)', 'unitix-admin-td'),
                        'id'      => 'count',
                        'type'    => 'slider',
                        'default' => '3',
                        'admin_label' => true,
                        'attr'    => array(
                            'max'  => 100,
                            'min'  => 0,
                            'step' => 1
                        )
                    ),
                    array(
                        'name'      => esc_html__('Service Style', 'unitix-admin-td'),
                        'id'        => 'style',
                        'type'      => 'select',
                        'default'   =>  'horizontal',
                        'admin_label' => true,
                        'options' => array(
                            'horizontal' => esc_html__('Horizontal', 'unitix-admin-td'),
                            'vertical' => esc_html__('Vertical', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Services will be shown horizontallly or vertically on the page.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Columns (horizontal style)', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of columns to show the services in', 'unitix-admin-td'),
                        'id'      => 'columns',
                        'type'    => 'select',
                        'options' => array(
                            2 => esc_html__('Two columns', 'unitix-admin-td'),
                            3 => esc_html__('Three columns', 'unitix-admin-td'),
                            4 => esc_html__('Four columns', 'unitix-admin-td'),
                            6 => esc_html__('Six columns', 'unitix-admin-td'),
                        ),
                        'default' => '3',
                    ),
                    array(
                        'name'      =>  esc_html__('Image Shape', 'unitix-admin-td'),
                        'id'        => 'image_shape',
                        'type'      => 'select',
                        'admin_label' => true,
                        'options'   =>  array(
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        ),
                        'default'   => 'round',
                    ),
                    array(
                        'name'      =>  esc_html__('Shape Size', 'unitix-admin-td'),
                        'id'        => 'image_size',
                        'type'      => 'select',
                        'options'   =>  array(
                            'big'    => esc_html__('Big', 'unitix-admin-td'),
                            'huge'   => esc_html__('Huge', 'unitix-admin-td'),
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'medium' => esc_html__('Medium', 'unitix-admin-td'),
                            'small'  => esc_html__('Small', 'unitix-admin-td'),
                        ),
                        'default'   => 'big',
                    ),
                    array(
                        'name'      =>  esc_html__('Shadow', 'unitix-admin-td'),
                        'id'        => 'image_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'hide',
                    ),
                    array(
                        'name'      => esc_html__('Show Connections', 'unitix-admin-td'),
                        'id'        => 'connected',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Services will be connected with a dotted line.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Show Titles', 'unitix-admin-td'),
                        'id'        => 'show_titles',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Link Title', 'unitix-admin-td'),
                        'id'        => 'link_titles',
                        'type'      => 'select',
                        'default'   =>  'on',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show Image', 'unitix-admin-td'),
                        'id'        => 'show_images',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show'  => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Link Image', 'unitix-admin-td'),
                        'id'        => 'link_images',
                        'type'      => 'select',
                        'default'   =>  'on',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show Excerpt', 'unitix-admin-td'),
                        'id'        => 'show_excerpts',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Excerpt & More Text Alignment', 'unitix-admin-td'),
                        'id'        => 'align_excerpts',
                        'type'      => 'select',
                        'default'   => 'center',
                        'options' => array(
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                            'justify'  => esc_html__('Justify', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the excerpt text and the read more link.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Show Readmore Link', 'unitix-admin-td'),
                        'id'        => 'show_readmores',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Readmore Link Text', 'unitix-admin-td'),
                        'id'      => 'readmore_text',
                        'type'    => 'text',
                        'default' => 'Read more',
                        'desc'    => esc_html__('Customize your readmore link', 'unitix-admin-td'),
                    ),
                    array(
                        'name'        => esc_html__('Order by', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose the way service items will be ordered.', 'unitix-admin-td'),
                        'id'          => 'orderby',
                        'type'        => 'select',
                        'default'     =>  'none',
                        'options'     => array(
                            'none'        => esc_html__('None', 'unitix-admin-td'),
                            'title'       => esc_html__('Title', 'unitix-admin-td'),
                            'date'        => esc_html__('Date', 'unitix-admin-td'),
                            'ID'          => esc_html__('ID', 'unitix-admin-td'),
                            'author'      => esc_html__('Author', 'unitix-admin-td'),
                            'modified'    => esc_html__('Last Modified', 'unitix-admin-td'),
                            'menu_order'  => esc_html__('Menu Order', 'unitix-admin-td'),
                            'rand'        => esc_html__('Random', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'        => esc_html__('Order', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose how services will be ordered.', 'unitix-admin-td'),
                        'id'          => 'order',
                        'type'        => 'select',
                        'default'     =>  'ASC',
                        'options'     => array(
                            'ASC'     => esc_html__('Ascending', 'unitix-admin-td'),
                            'DESC'    => esc_html__('Descending', 'unitix-admin-td'),
                        )
                    )
                )
            ),
        )
    ),
     // TESTIMONIALS SHORTCODE SECTION
    'testimonials' => array(
        'shortcode' => 'testimonials',
        'title'     => esc_html__('Testimonials', 'unitix-admin-td'),
        'desc'      => esc_html__('Displays a list / slideshow of testimonials.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'has_content'   => false,
        'sections'   => array(
            array(
                'title' => esc_html__('Testimonials', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Choose a group', 'unitix-admin-td'),
                        'desc'    => esc_html__('Group of testimonials to show', 'unitix-admin-td'),
                        'id'      => 'group',
                        'default' =>  '',
                        'type'    => 'select',
                        'admin_label' => true,
                        'admin_label' => true,
                        'options' => 'taxonomy',
                        'taxonomy' => 'oxy_testimonial_group',
                        'blank_label' => esc_html__('All Testimonials', 'unitix-admin-td')
                    ),
                    array(
                        'name'    => esc_html__('Number Of Testimonials', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of Testimonials to display(set to 0 to show all)', 'unitix-admin-td'),
                        'id'      => 'count',
                        'type'    => 'slider',
                        'admin_label' => true,
                        'default' => '3',
                        'attr'    => array(
                            'max'   => 10,
                            'min'   => 0,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Show avatars', 'unitix-admin-td'),
                        'desc'    => esc_html__('Display the featured image as avatar', 'unitix-admin-td'),
                        'id'      => 'show_image',
                        'type'    => 'select',
                        'default' => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Display as Slideshow', 'unitix-admin-td'),
                        'desc'    => esc_html__('Display the testimonials as a slideshow', 'unitix-admin-td'),
                        'id'      => 'slideshow',
                        'type'    => 'select',
                        'default' => 'on',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Speed', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the speed of the slideshow cycling, in milliseconds', 'unitix-admin-td'),
                        'id'        => 'speed',
                        'type'      => 'slider',
                        'default'   => '7000',
                        'attr'      => array(
                            'max'       => 15000,
                            'min'       => 2000,
                            'step'      => 1000
                        )
                    ),
                    array(
                        'name'    => esc_html__('Randomize', 'unitix-admin-td'),
                        'desc'    => esc_html__('Randomize the ordering of the testimonials', 'unitix-admin-td'),
                        'id'      => 'randomize',
                        'type'    => 'select',
                        'default' => 'off',
                        'options' => array(
                            'on'   => esc_html__('On', 'unitix-admin-td'),
                            'off'  => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    )
                )
            ),
        )
    ),
     /* Staff Shortcodes */
    'staff_list' =>  array(
        'shortcode'     => 'staff_list',
        'title'         => esc_html__('Staff List', 'unitix-admin-td'),
        'desc'          => esc_html__('Displays a list of staff members in columns.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Staff members list', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Choose a department', 'unitix-admin-td'),
                        'desc'    => esc_html__('Populate your list from a department', 'unitix-admin-td'),
                        'id'      => 'department',
                        'default' =>  '',
                        'type'    => 'select',
                        'admin_label' => true,
                        'options' => 'taxonomy',
                        'taxonomy' => 'oxy_staff_department',
                        'blank_label' => esc_html__('Select a department', 'unitix-admin-td')
                    ),
                    array(
                        'name'    => esc_html__('Number Of members', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of members to display(set to 0 to show all)', 'unitix-admin-td'),
                        'id'      => 'count',
                        'type'    => 'slider',
                        'admin_label' => true,
                        'default' => '3',
                        'attr'    => array(
                            'max'  => 100,
                            'min'  => 0,
                            'step' => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('List Columns', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of columns to show staff in', 'unitix-admin-td'),
                        'id'      => 'columns',
                        'type'    => 'select',
                        'admin_label' => true,
                        'options' => array(
                            2 => esc_html__('Two columns', 'unitix-admin-td'),
                            3 => esc_html__('Three columns', 'unitix-admin-td'),
                            4 => esc_html__('Four columns', 'unitix-admin-td'),
                            6 => esc_html__('Six columns', 'unitix-admin-td'),
                        ),
                        'default' => '3',
                    ),
                    array(
                        'name'      =>  esc_html__('Image Shape', 'unitix-admin-td'),
                        'id'        => 'image_shape',
                        'type'      => 'select',
                        'admin_label' => true,
                        'options'   =>  array(
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        ),
                        'default'   => 'round',
                    ),
                    array(
                        'name'      =>  esc_html__('Shape Size', 'unitix-admin-td'),
                        'id'        => 'image_size',
                        'type'      => 'select',
                        'options'   =>  array(
                            'big'    => esc_html__('Big', 'unitix-admin-td'),
                            'huge'   => esc_html__('Huge', 'unitix-admin-td'),
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'medium' => esc_html__('Medium', 'unitix-admin-td'),
                            'small'  => esc_html__('Small', 'unitix-admin-td'),
                        ),
                        'default'   => 'big',
                    ),
                    array(
                        'name'      =>  esc_html__('Shadow', 'unitix-admin-td'),
                        'id'        => 'image_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'hide',
                    ),
                    array(
                        'name'    => esc_html__('Link in name', 'unitix-admin-td'),
                        'desc'    => esc_html__('Make the name link to the url specified in staff page', 'unitix-admin-td'),
                        'id'      => 'link_name',
                        'type'    => 'select',
                        'default' => 'on',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Show Position', 'unitix-admin-td'),
                        'desc'    => esc_html__('Display the staff position below the name', 'unitix-admin-td'),
                        'id'      => 'show_name',
                        'type'    => 'select',
                        'default' => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show Excerpt', 'unitix-admin-td'),
                        'id'        => 'show_excerpts',
                        'type'      => 'select',
                        'default'   => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Excerpt & More Text Alignment', 'unitix-admin-td'),
                        'id'        => 'align_excerpts',
                        'type'      => 'select',
                        'default'   => 'center',
                        'options' => array(
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                            'justify'  => esc_html__('Justify', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the excerpt text and the read more link.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Show Social Links', 'unitix-admin-td'),
                        'id'        => 'show_social',
                        'type'      => 'select',
                        'default'   => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Open Social Links In', 'unitix-admin-td'),
                        'id'      => 'link_target',
                        'type'    => 'select',
                        'default' => '_self',
                        'options' => array(
                            '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                            '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                            '_parent' => esc_html__('Open the linked document in the parent frameset', 'unitix-admin-td'),
                            '_top'    => esc_html__('Open the linked document in the full body of the window', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Where the social links open to', 'unitix-admin-td'),
                    ),
                    array(
                        'name'        => esc_html__('Order by', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose the way staff items will be ordered.', 'unitix-admin-td'),
                        'id'          => 'orderby',
                        'type'        => 'select',
                        'default'     =>  'none',
                        'options'     => array(
                            'none'        => esc_html__('None', 'unitix-admin-td'),
                            'title'       => esc_html__('Title', 'unitix-admin-td'),
                            'date'        => esc_html__('Date', 'unitix-admin-td'),
                            'ID'          => esc_html__('ID', 'unitix-admin-td'),
                            'author'      => esc_html__('Author', 'unitix-admin-td'),
                            'modified'    => esc_html__('Last Modified', 'unitix-admin-td'),
                            'menu_order'  => esc_html__('Menu Order', 'unitix-admin-td'),
                            'rand'        => esc_html__('Random', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'        => esc_html__('Order', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose how staff will be ordered.', 'unitix-admin-td'),
                        'id'          => 'order',
                        'type'        => 'select',
                        'default'     =>  'ASC',
                        'options'     => array(
                            'ASC'     => esc_html__('Ascending', 'unitix-admin-td'),
                            'DESC'    => esc_html__('Descending', 'unitix-admin-td'),
                        )
                    )
                )
            ),
        ),
    ),
    'staff_featured' => array(
        'shortcode'     => 'staff_featured',
        'title'         => esc_html__('Featured Staff', 'unitix-admin-td'),
        'desc'          => esc_html__('Displays a featured section about one member of staff.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Staff', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Featured member', 'unitix-admin-td'),
                        'desc'    => esc_html__('Select the staff member that will be featured', 'unitix-admin-td'),
                        'id'      => 'member',
                        'default' =>  '',
                        'type'    => 'select',
                        'options' => 'staff_featured',
                    ),
                    array(
                        'name'      =>  esc_html__('Image Shape', 'unitix-admin-td'),
                        'id'        => 'image_shape',
                        'type'      => 'select',
                        'admin_label' => true,
                        'options'   =>  array(
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        ),
                        'default'   => 'round',
                    ),
                    array(
                        'name'      =>  esc_html__('Image Size', 'unitix-admin-td'),
                        'id'        => 'image_size',
                        'type'      => 'select',
                        'options'   =>  array(
                            'big'    => esc_html__('Big', 'unitix-admin-td'),
                            'huge'   => esc_html__('Huge', 'unitix-admin-td'),
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'medium' => esc_html__('Medium', 'unitix-admin-td'),
                            'small'  => esc_html__('Small', 'unitix-admin-td'),
                        ),
                        'default'   => 'big',
                    ),
                    array(
                        'name'      =>  esc_html__('Shadow', 'unitix-admin-td'),
                        'id'        => 'image_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'hide',
                    ),
                    array(
                        'name'    => esc_html__('Show Position', 'unitix-admin-td'),
                        'desc'    => esc_html__('Display the staff position below the name', 'unitix-admin-td'),
                        'id'      => 'show_position',
                        'type'    => 'select',
                        'default' => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show Content', 'unitix-admin-td'),
                        'id'        => 'show_content',
                        'type'      => 'select',
                        'default'   => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Content Font Size', 'unitix-admin-td'),
                        'id'        => 'content_size',
                        'type'      => 'select',
                        'default'   => 'big',
                        'options' => array(
                            'big'   => esc_html__('Big', 'unitix-admin-td'),
                            'small' => esc_html__('Small', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Content Alignment', 'unitix-admin-td'),
                        'id'        => 'align_content',
                        'type'      => 'select',
                        'default'   => 'center',
                        'options' => array(
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                            'justify'  => esc_html__('Justify', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the excerpt text and the read more link.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Show Social Links', 'unitix-admin-td'),
                        'id'        => 'show_social',
                        'type'      => 'select',
                        'default'   => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Open Social Links In', 'unitix-admin-td'),
                        'id'      => 'link_target',
                        'type'    => 'select',
                        'default' => '_self',
                        'options' => array(
                            '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                            '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                            '_parent' => esc_html__('Open the linked document in the parent frameset', 'unitix-admin-td'),
                            '_top'    => esc_html__('Open the linked document in the full body of the window', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Where the social links open to', 'unitix-admin-td'),
                    ),
                )
            ),
        )
    ),
    // PORTFOLIO SHORTCODE OPTIONS
    'portfolio' => array(
        'shortcode'     => 'portfolio',
        'title'         => esc_html__('Portfolio', 'unitix-admin-td'),
        'desc'          => esc_html__('Displays a set of portfolio items in columns.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Portfolio', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Category', 'unitix-admin-td'),
                        'desc'    => esc_html__('Categories to show (leave blank to show all)', 'unitix-admin-td'),
                        'id'      => 'categories',
                        'default' =>  '',
                        'type'    => 'select',
                        'options' => 'taxonomy',
                        'taxonomy' => 'oxy_portfolio_categories',
                        'admin_label' => true,
                        'attr' => array(
                            'multiple' => '',
                            'style' => 'height:100px'
                        )
                    ),
                    array(
                        'name'    => esc_html__('Number of portfolio items to display  (per page if pagination is on )', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of portfolio items to display(per page)', 'unitix-admin-td'),
                        'id'      => 'count',
                        'type'    => 'slider',
                        'default' => '3',
                        'admin_label' => true,
                        'attr'    => array(
                            'max'   => 24,
                            'min'   => 1,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Portfolio Columns', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of columns to show the portfolio in', 'unitix-admin-td'),
                        'id'      => 'columns',
                        'type'    => 'slider',
                        'default' => '3',
                        'admin_label' => true,
                        'attr'    => array(
                            'max'   => 4,
                            'min'   => 2,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'      => esc_html__('Use pagination', 'unitix-admin-td'),
                        'desc'      => esc_html__('Split your portfolio items in pages', 'unitix-admin-td'),
                        'id'        => 'pagination',
                        'type'      => 'select',
                        'default'   => 'off',
                        'options' => array(
                            'off'       => esc_html__('Off', 'unitix-admin-td'),
                            'simple'    => esc_html__('Simple', 'unitix-admin-td'),
                            'infinite'  => esc_html__('Infinite', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Shape', 'unitix-admin-td'),
                        'desc'      => esc_html__('Choose the portfolio image shape', 'unitix-admin-td'),
                        'id'        => 'shape',
                        'type'      => 'select',
                        'default'   => 'round',
                        'admin_label' => true,
                        'options' => array(
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      =>  esc_html__('Shadow', 'unitix-admin-td'),
                        'id'        => 'show_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'show',
                    ),
                    array(
                        'name'    => esc_html__('Show Title', 'unitix-admin-td'),
                        'desc'    => esc_html__('Display the portfolio title', 'unitix-admin-td'),
                        'id'      => 'show_title',
                        'type'    => 'select',
                        'default' => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show Excerpt', 'unitix-admin-td'),
                        'desc'      => esc_html__('Display the portfolio excerpt', 'unitix-admin-td'),
                        'id'        => 'show_excerpt',
                        'type'      => 'select',
                        'default'   => 'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      =>  esc_html__('Overlay', 'unitix-admin-td'),
                        'desc'      => esc_html__('Show overlay on hover', 'unitix-admin-td'),
                        'id'        => 'show_overlay',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'show',
                    ),
                    array(
                        'name'      =>  esc_html__('Magnific on hidden overlay', 'unitix-admin-td'),
                        'desc'      => esc_html__('Open portfolio links in Magnific popup when overlay is hidden', 'unitix-admin-td'),
                        'id'        => 'show_magnific',
                        'type'      => 'select',
                        'options'   =>  array(
                            'show'   => esc_html__('Show', 'unitix-admin-td'),
                            'hide'   => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'default'   => 'show',
                    ),
                    array(
                        'name'    => esc_html__('Magnific Overlay Link Type', 'unitix-admin-td'),
                        'desc'    => esc_html__('Select the magnific link type to use for the item.', 'unitix-admin-td'),
                        'id'      => 'magnific_link_type',
                        'type'    => 'select',
                        'default' => 'magnific',
                        'options' => array(
                            'magnific'      => esc_html__('Magnific Single Item', 'unitix-admin-td'),
                            'magnific-all'  => esc_html__('Magnific All Items', 'unitix-admin-td')
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Magnific Popup Captions', 'unitix-admin-td'),
                        'id'        => 'magnific_caption',
                        'type'      => 'select',
                        'default'   => 'post_title_caption',
                        'options' => array(
                            'image_caption'         => esc_html__('Media Image Caption', 'unitix-admin-td'),
                            'post_title_caption'    => esc_html__('Post Title', 'unitix-admin-td'),
                            'off'                   => esc_html__('No Captions', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('What the caption below the magnific popup image will be.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Portfolio Filters', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show filter navigation', 'unitix-admin-td'),
                        'id'      => 'show_filters',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options'  => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            )
                    ),
                    array(
                        'name'        => esc_html__('Order by', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose the way portfolio items will be ordered.', 'unitix-admin-td'),
                        'id'          => 'orderby',
                        'type'        => 'select',
                        'default'     =>  'none',
                        'options'     => array(
                            'none'        => esc_html__('None', 'unitix-admin-td'),
                            'title'       => esc_html__('Title', 'unitix-admin-td'),
                            'date'        => esc_html__('Date', 'unitix-admin-td'),
                            'ID'          => esc_html__('ID', 'unitix-admin-td'),
                            'author'      => esc_html__('Author', 'unitix-admin-td'),
                            'modified'    => esc_html__('Last Modified', 'unitix-admin-td'),
                            'menu_order'  => esc_html__('Menu Order', 'unitix-admin-td'),
                            'rand'        => esc_html__('Random', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'        => esc_html__('Order', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose how portfolio will be ordered.', 'unitix-admin-td'),
                        'id'          => 'order',
                        'type'        => 'select',
                        'default'     =>  'ASC',
                        'options'     => array(
                            'ASC'     => esc_html__('Ascending', 'unitix-admin-td'),
                            'DESC'    => esc_html__('Descending', 'unitix-admin-td'),
                        )
                    )
                )
            ),
        )
    ),
     'recent_posts' => array(
        'shortcode' => 'recent_posts',
        'title'     => esc_html__('Recent Posts', 'unitix-admin-td'),
        'desc'       => esc_html__('Displays a list of recent posts.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'has_content'   => false,
        'sections'   => array(
            array(
                'title' => esc_html__('Recent Posts', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'        => esc_html__('Layout', 'unitix-admin-td'),
                        'desc'        => esc_html__('Section Layout', 'unitix-admin-td'),
                        'id'          => 'layout',
                        'type'        => 'select',
                        'default'     => 'simple',
                        'admin_label' => true,
                        'options'     => array(
                            'simple'    => esc_html__('Simple', 'unitix-admin-td'),
                            'slideshow' => esc_html__('Slideshow', 'unitix-admin-td'),
                            'masonry'   => esc_html__('Masonry', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'        => esc_html__('Pagination', 'unitix-admin-td'),
                        'desc'        => esc_html__('Toggle Pagination on/off', 'unitix-admin-td'),
                        'id'          => 'pagination',
                        'type'        => 'select',
                        'default'     => 'on',
                        'admin_label' => true,
                        'options'     => array(
                            'on'    => esc_html__('On', 'unitix-admin-td'),
                            'off'   => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Number of posts per page', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of posts to display per page ( if pagination is on ) or total number of posts ( if pagination is off/slideshow )', 'unitix-admin-td'),
                        'id'      => 'count',
                        'type'    => 'slider',
                        'default' => '3',
                        'attr'    => array(
                            'max'   => 10,
                            'min'   => 1,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Post category', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose posts from a specific category', 'unitix-admin-td'),
                        'id'      => 'cat',
                        'default' =>  '',
                        'type'    => 'select',
                        'options' => 'categories',
                        'attr' => array(
                            'multiple' => '',
                            'style' => 'height:100px'
                        )
                    ),
                    array(
                        'name'    => esc_html__('Post Swatch', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a color swatch for the posts', 'unitix-admin-td'),
                        'id'      => 'post_swatch',
                        'type'    => 'select',
                        'default' => 'swatch-red-white',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                    ),
                    array(
                        'name'    => esc_html__('Columns', 'unitix-admin-td'),
                        'desc'    => esc_html__('Number of columns to show posts in', 'unitix-admin-td'),
                        'id'      => 'columns',
                        'type'    => 'select',
                        'default' => '3',
                        'options' => array(
                            '1' => esc_html__('1 Column', 'unitix-admin-td'),
                            '2' => esc_html__('2 Columns', 'unitix-admin-td'),
                            '3' => esc_html__('3 Columns', 'unitix-admin-td'),
                            '4' => esc_html__('4 Columns', 'unitix-admin-td'),
                        ),
                    ),
                     array(
                        'name'      => esc_html__('Use infinite Scroll in Masonry', 'unitix-admin-td'),
                        'desc'    => esc_html__('pagination is not displayed in masonry if infinite scrolling is on', 'unitix-admin-td'),
                        'id'        => 'infinite_scroll',
                        'type'      => 'select',
                        'default'   => 'on',
                        'options' => array(
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
        )
    ),
    // MAP SHORTCODE OPTIONS
    'map' => array(
        'shortcode'     => 'map',
        'title'         => esc_html__('Google Map', 'unitix-admin-td'),
        'desc'          => esc_html__('Adds a Google Map to the page.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Map', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Map Type', 'unitix-admin-td'),
                        'id'        => 'map_type',
                        'type'      => 'select',
                        'default'   =>  'ROADMAP',
                        'options' => array(
                            'ROADMAP'   => esc_html__('Roadmap', 'unitix-admin-td'),
                            'SATELLITE' => esc_html__('Satellite', 'unitix-admin-td'),
                            'TERRAIN'   => esc_html__('Terrain', 'unitix-admin-td'),
                            'HYBRID'    => esc_html__('Hybrid', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Map Zoom', 'unitix-admin-td'),
                        'id'        => 'map_zoom',
                        'type'      => 'slider',
                        'default' => '15',
                        'attr'    => array(
                            'max'   => 20,
                            'min'   => 1,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'      => esc_html__('Mouse Wheel Scrolling', 'unitix-admin-td'),
                        'id'        => 'map_scroll',
                        'type'      => 'select',
                        'default'   => 'off',
                        'options' => array(
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Draggable', 'unitix-admin-td'),
                        'id'        => 'map_draggable',
                        'type'      => 'select',
                        'default'   => 'off',
                        'options' => array(
                            'off' => esc_html__('Off', 'unitix-admin-td'),
                            'on'  => esc_html__('On', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Map Style', 'unitix-admin-td'),
                        'id'        => 'map_style',
                        'type'      => 'select',
                        'default'   =>  'flat',
                        'options' => array(
                            'flat'    => esc_html__('Flat', 'unitix-admin-td'),
                            'regular' => esc_html__('Regular', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
            array(
                'title' => esc_html__('Marker', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Show Marker', 'unitix-admin-td'),
                        'id'        => 'marker',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Marker Address', 'unitix-admin-td'),
                        'desc'    => esc_html__('Address to show marker', 'unitix-admin-td'),
                        'id'      => 'address',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Marker Latitude', 'unitix-admin-td'),
                        'desc'    => esc_html__('Latitude of marker (if you dont want to use address)', 'unitix-admin-td'),
                        'id'      => 'lat',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Marker Longitude', 'unitix-admin-td'),
                        'desc'    => esc_html__('Longitude of marker (if you dont want to use address)', 'unitix-admin-td'),
                        'id'      => 'lng',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Marker Label', 'unitix-admin-td'),
                        'desc'    => esc_html__('Label to show above the marker', 'unitix-admin-td'),
                        'id'      => 'label',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                )
            ),
            array(
                'title' => esc_html__('Section', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Map Section Height', 'unitix-admin-td'),
                        'id'        => 'height',
                        'type'      => 'slider',
                        'default' => '500',
                        'attr'    => array(
                            'max'   => 800,
                            'min'   => 50,
                            'step'  => 1
                        )
                    ),
                )
            )
        )
    ),
    // PIECHART SHORTCODE
    'pie' => array(
        'shortcode' => 'pie',
        'title'     => esc_html__('Pie Chart', 'unitix-admin-td'),
        'desc'      => esc_html__('Creates a circular chart to show a % value.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'has_content'   => false,
        'sections'   => array(
            array(
                'title' => esc_html__('Pie Chart', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Track Colour', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the chart track colour', 'unitix-admin-td'),
                        'id'      => 'track_colour',
                        'default' =>  '',
                        'type'    => 'colour',
                    ),
                    array(
                        'name'    => esc_html__('Bar Colour', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the chart bar colour', 'unitix-admin-td'),
                        'id'      => 'bar_colour',
                        'default' =>  '',
                        'type'    => 'colour',
                    ),
                    array(
                        'name'    => esc_html__('Icon', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a chart icon', 'unitix-admin-td'),
                        'id'      => 'icon',
                        'default' =>  '',
                        'type'    => 'select',
                        'options' => 'icons',
                    ),
                    array(
                        'name'    => esc_html__('Icon Animation', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose an icon animation', 'unitix-admin-td'),
                        'id'      => 'icon_animation',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
                    ),
                    array(
                        'name'    => esc_html__('Percentage', 'unitix-admin-td'),
                        'desc'    => esc_html__('Percentage of the pie chart', 'unitix-admin-td'),
                        'id'      => 'percentage',
                        'admin_label' => true,
                        'type'    => 'slider',
                        'default' => '50',
                        'attr'    => array(
                            'max'   => 100,
                            'min'   => 1,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Set the size of the chart', 'unitix-admin-td'),
                        'id'      => 'size',
                        'type'    => 'slider',
                        'default' => '200',
                        'attr'    => array(
                            'max'   => 400,
                            'min'   => 50,
                            'step'  => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Line Width', 'unitix-admin-td'),
                        'desc'    => esc_html__('Set the width of the charts line', 'unitix-admin-td'),
                        'id'      => 'line_width',
                        'type'    => 'slider',
                        'default' => '20',
                        'attr'    => array(
                            'max'   => 30,
                            'min'   => 5,
                            'step'  => 1
                        )
                    ),
                )
            ),
        )
    ),
     // COUNTER SHORTCODE
    'counter' => array(
        'shortcode' => 'counter',
        'title'     => esc_html__('Counter', 'unitix-admin-td'),
        'desc'      => esc_html__('Creates a circular counter to show an integer value.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'has_content'   => false,
        'sections'   => array(
            array(
                'title' => esc_html__('Counter', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Value', 'unitix-admin-td'),
                        'desc'    => esc_html__('Value of the circular counter', 'unitix-admin-td'),
                        'id'      => 'value',
                        'admin_label' => true,
                        'default'     =>  '',
                        'type'        => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Counter Font Size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the size of the font to use in the counter', 'unitix-admin-td'),
                        'id'      => 'counter_size',
                        'type'    => 'select',
                        'options' => array(
                            'normal'      => esc_html__('Normal', 'unitix-admin-td'),
                            'super' => esc_html__('Super (60px)', 'unitix-admin-td'),
                            'hyper' => esc_html__('Hyper (96px)', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    ),
                    array(
                        'name'    => esc_html__('Counter Font Weight', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the weight of the font to use in the counter', 'unitix-admin-td'),
                        'id'      => 'counter_weight',
                        'type'    => 'select',
                        'options' => array(
                            'regular'  => esc_html__('Regular', 'unitix-admin-td'),
                            'black'    => esc_html__('Black', 'unitix-admin-td'),
                            'bold'     => esc_html__('Bold', 'unitix-admin-td'),
                            'light'    => esc_html__('Light', 'unitix-admin-td'),
                            'hairline' => esc_html__('Hairline', 'unitix-admin-td'),
                        ),
                        'default' => 'regular',
                    ),
                    array(
                        'name'    => esc_html__('Underline', 'unitix-admin-td'),
                        'desc'    => esc_html__('Underline the number.', 'unitix-admin-td'),
                        'id'      => 'underline',
                        'type'    => 'select',
                        'options' => array(
                            'underline'    => esc_html__('Underline', 'unitix-admin-td'),
                            'no-underline' => esc_html__('No Underline', 'unitix-admin-td')
                        ),
                        'default' => 'underline',
                    ),
                )
            ),
        )
    ),
    'pricing' => array(
        'title'       => esc_html__('Pricing Column', 'unitix-admin-td'),
        'desc'        => esc_html__('Displays a price info column.', 'unitix-admin-td'),
        'shortcode'   => 'pricing',
        'insert_with' => 'dialog',
        'has_content' => false,
        'sections'   => array(
            array(
                'title' => esc_html__('Pricing Table', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'        => esc_html__('Heading', 'unitix-admin-td'),
                        'desc'        => esc_html__('Heading text of the column', 'unitix-admin-td'),
                        'id'          => 'heading',
                        'admin_label' => true,
                        'default'     =>  '',
                        'type'        => 'text',
                    ),
                    array(
                        'name'      => esc_html__('Featured Column', 'unitix-admin-td'),
                        'id'        => 'featured',
                        'type'      => 'select',
                        'default'   =>  'false',
                        'options' => array(
                            'false' => esc_html__('Not Featured', 'unitix-admin-td'),
                            'true'  => esc_html__('Featured', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show price', 'unitix-admin-td'),
                        'id'        => 'show_price',
                        'type'      => 'select',
                        'default'   =>  'true',
                        'options' => array(
                            'true' => esc_html__('Show', 'unitix-admin-td'),
                            'false' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Price', 'unitix-admin-td'),
                        'desc'    => esc_html__('Price to show at top of the column.', 'unitix-admin-td'),
                        'id'      => 'price',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Price Currency', 'unitix-admin-td'),
                        'desc'    => esc_html__('Price currency to show next to the price.', 'unitix-admin-td'),
                        'id'      => 'currency',
                        'default' =>  '&#36;',
                        'type'    => 'select',
                        'options' => array(
                            '&#36;'   => esc_html__('Dollar', 'unitix-admin-td'),
                            '&#128;'  => esc_html__('Euro', 'unitix-admin-td'),
                            '&#163;'  => esc_html__('Pound', 'unitix-admin-td'),
                            '&#165;'  => esc_html__('Yen', 'unitix-admin-td'),
                            'custom' => esc_html__('Custom', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'    => esc_html__('Custom Currency', 'unitix-admin-td'),
                        'desc'    => esc_html__('If custom currency selected enter the html code here.', 'unitix-admin-td'),
                        'id'      => 'custom_currency',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('After Price Text', 'unitix-admin-td'),
                        'desc'    => esc_html__('Text to show after the price.', 'unitix-admin-td'),
                        'id'      => 'per',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Item List', 'unitix-admin-td'),
                        'desc'    => esc_html__('List of items to put in the column under the price. Divide categories with linebreaks (Enter)', 'unitix-admin-td'),
                        'id'      => 'list',
                        'default' =>  '',
                        'type'    => 'exploded_textarea',
                    ),
                    array(
                        'name'      => esc_html__('Show button', 'unitix-admin-td'),
                        'id'        => 'show_button',
                        'type'      => 'select',
                        'default'   =>  'true',
                        'options' => array(
                            'true' => esc_html__('Show', 'unitix-admin-td'),
                            'false' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Button Text', 'unitix-admin-td'),
                        'desc'    => esc_html__('Text to inside the button at the bottom of the column.', 'unitix-admin-td'),
                        'id'      => 'button_text',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => esc_html__('Button Link', 'unitix-admin-td'),
                        'desc'    => esc_html__('Link that the button will point to.', 'unitix-admin-td'),
                        'id'      => 'button_link',
                        'default' =>  '',
                        'type'    => 'text',
                    ),
                )
            )
        ),
    ),
    'feature' => array(
        'shortcode'   => 'feature',
        'title'       => esc_html__('Feature', 'unitix-admin-td'),
        'desc'        => esc_html__('Displays a shape with an icon alongside some text.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'has_content'   => false,
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'      => esc_html__('Show Icon', 'unitix-admin-td'),
                        'id'        => 'show_icon',
                        'type'      => 'select',
                        'default'   =>  'true',
                        'options' => array(
                            'true' => esc_html__('Show', 'unitix-admin-td'),
                            'false' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Show / Hide the icon on the left.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Icon', 'unitix-admin-td'),
                        'id'      => 'icon',
                        'type'    => 'select',
                        'admin_label' => true,
                        'options' => 'icons',
                        'default' => '',
                        'desc'    => esc_html__('Choose an icon to use in your featured icon', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Icon Shape', 'unitix-admin-td'),
                        'desc'    => esc_html__('Set shape used for the icon.', 'unitix-admin-td'),
                        'id'      => 'shape',
                        'type'    => 'select',
                        'default' => 'round',
                        'options'    => array(
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'    => esc_html__('Icon Animation', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose an icon animation', 'unitix-admin-td'),
                        'id'      => 'animation',
                        'type'    => 'select',
                        'default' => '',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
                    ),
                    array(
                        'name'        => esc_html__('Title', 'unitix-admin-td'),
                        'id'          => 'title',
                        'type'        => 'text',
                        'admin_label' => true,
                        'default'     => '',
                        'desc'        => esc_html__('Choose a title for your featured item.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'        => esc_html__('Content', 'unitix-admin-td'),
                        'id'          => 'content',
                        'type'        => 'textarea',
                        'default'     => '',
                        'desc'        => esc_html__('Content to show below title.', 'unitix-admin-td'),
                    ),
                )
            )
        )
    ),
    'slideshow' => array(
        'shortcode'     => 'slideshow',
        'title'         => esc_html__('Slideshow', 'unitix-admin-td'),
        'desc'          => esc_html__('Adds a Flexslider slideshow to the page.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Slideshow', 'unitix-admin-td'),
                'javascripts' => array(
                    array(
                        'handle' => 'header_options_script',
                        'src'    => OXY_THEME_URI . 'inc/options/javascripts/dialogs/slider-options.js',
                        'deps'   => array( 'jquery'),
                        'localize' => array(
                            'object_handle' => 'theme',
                            'data'          => THEME_SHORT
                        ),
                    ),
                ),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Choose a Flexslider', 'unitix-admin-td'),
                        'desc'    => esc_html__('Populate your slider with one of the slideshows you created', 'unitix-admin-td'),
                        'id'      => 'flexslider',
                        'default' =>  '',
                        'type'    => 'select',
                        'options' => 'slideshow',
                    ),
                    array(
                        'name'      =>  esc_html__('Animation style', 'unitix-admin-td'),
                        'desc'      =>  esc_html__('Select how your slider animates', 'unitix-admin-td'),
                        'id'        => 'animation',
                        'type'      => 'select',
                        'options'   =>  array(
                            'slide' => esc_html__('Slide', 'unitix-admin-td'),
                            'fade'  => esc_html__('Fade', 'unitix-admin-td'),
                        ),
                        'default'   => 'slide',
                    ),
                    array(
                        'name'      => esc_html__('Direction', 'unitix-admin-td'),
                        'desc'      =>  esc_html__('Select the direction your slider slides', 'unitix-admin-td'),
                        'id'        => 'direction',
                        'type'      => 'select',
                        'default'   =>  'horizontal',
                        'options' => array(
                            'horizontal' => esc_html__('Horizontal', 'unitix-admin-td'),
                            'vertical'   => esc_html__('Vertical', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Speed', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the speed of the slideshow cycling, in milliseconds', 'unitix-admin-td'),
                        'id'        => 'speed',
                        'type'      => 'slider',
                        'default'   => '7000',
                        'attr'      => array(
                            'max'       => 15000,
                            'min'       => 2000,
                            'step'      => 1000
                        )
                    ),
                    array(
                        'name'      => esc_html__('Duration', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set the speed of animations', 'unitix-admin-td'),
                        'id'        => 'duration',
                        'type'      => 'slider',
                        'default'   => '600',
                        'attr'      => array(
                            'max'       => 1500,
                            'min'       => 200,
                            'step'      => 100
                        )
                    ),
                    array(
                        'name'      => esc_html__('Auto start', 'unitix-admin-td'),
                        'id'        => 'autostart',
                        'type'      => 'select',
                        'default'   =>  'true',
                        'desc'    => esc_html__('Start slideshow automatically', 'unitix-admin-td'),
                        'options' => array(
                            'true'  => esc_html__('On', 'unitix-admin-td'),
                            'false' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show navigation arrows', 'unitix-admin-td'),
                        'id'        => 'directionnav',
                        'type'      => 'select',
                        'default'   =>  'hide',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Navigation arrows type', 'unitix-admin-td'),
                        'id'        => 'directionnavtype',
                        'type'      => 'select',
                        'default'   =>  'simple',
                        'options' => array(
                            'simple' => esc_html__('Simple', 'unitix-admin-td'),
                            'fancy'  => esc_html__('Fancy', 'unitix-admin-td'),
                        ),
                    ),
                     array(
                        'name'      => esc_html__('Item width', 'unitix-admin-td'),
                        'desc'      => esc_html__('Set width of the slider items( leave blank for full )', 'unitix-admin-td'),
                        'id'        => 'itemwidth',
                        'type'      => 'text',
                        'default'   => '',
                        'attr'      =>  array(
                            'size'    => 8,
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show controls', 'unitix-admin-td'),
                        'id'        => 'showcontrols',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Choose the place of the controls', 'unitix-admin-td'),
                        'id'        => 'controlsposition',
                        'type'      => 'select',
                        'default'   =>  'inside',
                        'options' => array(
                            'inside'    => esc_html__('Inside', 'unitix-admin-td'),
                            'outside'   => esc_html__('Outside', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      =>  esc_html__('Choose the alignment of the controls', 'unitix-admin-td'),
                        'id'        => 'controlsalign',
                        'type'      => 'select',
                        'options'   =>  array(
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                        ),
                        'default'   => 'center',
                    ),
                    array(
                        'name'      => esc_html__('Reverse', 'unitix-admin-td'),
                        'id'        => 'reverse',
                        'type'      => 'radio',
                        'default'   =>  'false',
                        'desc'    => esc_html__('Reverse the animation direction', 'unitix-admin-td'),
                        'options' => array(
                            'false' => esc_html__('Off', 'unitix-admin-td'),
                            'true'  => esc_html__('On', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Animation Loop', 'unitix-admin-td'),
                        'id'        => 'animationloop',
                        'type'      => 'radio',
                        'default'   =>  'true',
                        'desc'    => esc_html__('Gives the slider a seamless infinite loop', 'unitix-admin-td'),
                        'options' => array(
                            'true'  => esc_html__('On', 'unitix-admin-td'),
                            'false' => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
            array(
                'title' => esc_html__('Captions', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Show Captions', 'unitix-admin-td'),
                        'id'        => 'captions',
                        'type'      => 'select',
                        'default'   =>  'show',
                        'options' => array(
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Captions Vertical Position', 'unitix-admin-td'),
                        'id'        => 'captions_vertical',
                        'type'      => 'select',
                        'default'   =>  'bottom',
                        'options' => array(
                            'top'    => esc_html__('Top', 'unitix-admin-td'),
                            'bottom' => esc_html__('Bottom', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Captions Horizontal Position', 'unitix-admin-td'),
                        'desc'      => esc_html__('Choose among left, right and alternate positioning', 'unitix-admin-td'),
                        'id'        => 'captions_horizontal',
                        'type'      => 'select',
                        'default'   =>  'left',
                        'options' => array(
                            'left'      => esc_html__('Left', 'unitix-admin-td'),
                            'right'     => esc_html__('Right', 'unitix-admin-td'),
                            'alternate' => esc_html__('Alternate', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Show Tooltip', 'unitix-admin-td'),
                        'id'        => 'tooltip',
                        'type'      => 'select',
                        'default'   =>  'hide',
                        'desc'    => esc_html__('Show tooltip', 'unitix-admin-td'),
                        'options' => array(
                            'show'  => esc_html__('Show', 'unitix-admin-td'),
                            'hide'  => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                )
            ),
        )
    ),
    'image' => array(
        'shortcode'     => 'image',
        'title'         => esc_html__('Image', 'unitix-admin-td'),
        'desc'          => esc_html__('Displays an image.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Image', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Image Source', 'unitix-admin-td'),
                        'id'      => 'images',
                        'type'    => 'upload',
                        'store'   => 'id',
                        'default' => '',
                        'desc'    => esc_html__('Place the source path of the image here', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Image size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the size that the image will have', 'unitix-admin-td'),
                        'id'      => 'size',
                        'admin_label' => true,
                        'type'    => 'select',
                        'default' => 'medium',
                        'options' => array(
                            'thumbnail' => esc_html__('Thumbnail', 'unitix-admin-td'),
                            'medium'    => esc_html__('Medium', 'unitix-admin-td'),
                            'large'     => esc_html__('Large', 'unitix-admin-td'),
                            'full'      => esc_html__('Full', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Align', 'unitix-admin-td'),
                        'id'        => 'align',
                        'type'      => 'select',
                        'default'   => 'none',
                        'options' => array(
                            'none'       => esc_html__('None', 'unitix-admin-td'),
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                            'justify'  => esc_html__('Justify', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the image.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Image Alt', 'unitix-admin-td'),
                        'id'      => 'alt',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Place the alt of the image here', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Link', 'unitix-admin-td'),
                        'id'      => 'link',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Place a link here', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Open Link In', 'unitix-admin-td'),
                        'id'      => 'link_target',
                        'type'    => 'select',
                        'default' => '_self',
                        'options' => array(
                            '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                            '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Where the link will open.', 'unitix-admin-td'),
                    ),
                )
            )
        )
    ),
    'shapedimage' => array(
        'shortcode'     => 'shapedimage',
        'title'         => esc_html__('Shaped Image', 'unitix-admin-td'),
        'desc'          => esc_html__('Displays an image that is clipped to a shape.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Image', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Image Source', 'unitix-admin-td'),
                        'id'      => 'images',
                        'type'    => 'upload',
                        'store'   => 'id',
                        'admin_label' => true,
                        'default' => '',
                        'desc'    => esc_html__('Place the source path of the image here', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Image size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the size that the image will have', 'unitix-admin-td'),
                        'id'      => 'shape_size',
                        'type'    => 'select',
                        'default' => 'medium',
                        'options' => array(
                            'small'  => esc_html__('Small', 'unitix-admin-td'),
                            'medium' => esc_html__('Medium', 'unitix-admin-td'),
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'big'    => esc_html__('Big', 'unitix-admin-td'),
                            'huge'   => esc_html__('Huge', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Shape', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose if the image will be roundrd or not', 'unitix-admin-td'),
                        'id'      => 'shape',
                        'type'    => 'select',
                        'default' => 'round',
                        'options'    => array(
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'      =>  esc_html__('Shadow', 'unitix-admin-td'),
                        'id'        => 'shape_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                        'default'   => 'show'
                    ),
                    array(
                        'name'      => esc_html__('Align', 'unitix-admin-td'),
                        'id'        => 'align',
                        'type'      => 'select',
                        'default'   => 'none',
                        'options' => array(
                            'none'   => esc_html__('None', 'unitix-admin-td'),
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the image.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Hover Animation', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose a hover animation', 'unitix-admin-td'),
                        'id'      => 'animation',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
                    ),
                    array(
                        'name'    => esc_html__('Open In Magnific Popup', 'unitix-admin-td'),
                        'desc'    => esc_html__('Open the original image in magnific on click (overrides link option)', 'unitix-admin-td'),
                        'id'      => 'magnific',
                        'type'    => 'select',
                        'default' => 'off',
                        'options' => array(
                            'on'    => esc_html__('On', 'unitix-admin-td'),
                            'off'   => esc_html__('Off', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Image Alt', 'unitix-admin-td'),
                        'id'      => 'alt',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Place the alt of the image here', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Link', 'unitix-admin-td'),
                        'id'      => 'link',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Place a link here', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Open Links In', 'unitix-admin-td'),
                        'id'      => 'link_target',
                        'type'    => 'select',
                        'default' => '_self',
                        'options' => array(
                            '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                            '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Where the link will open.', 'unitix-admin-td'),
                    ),
                )
            )
        )
    ),
    'featuredicon' => array(
        'shortcode'     => 'featuredicon',
        'title'         => esc_html__('Featured Icon', 'unitix-admin-td'),
        'desc'          => esc_html__('Creates a shape with an icon in the middle.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Icon', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'    => esc_html__('Icon', 'unitix-admin-td'),
                        'id'      => 'icon',
                        'type'    => 'select',
                        'options' => 'icons',
                        'default' => 'glass',
                        'desc'    => esc_html__('Choose an icon to use in your featured icon', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Featured Icon Size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose the size that the image will have', 'unitix-admin-td'),
                        'id'      => 'shape_size',
                        'type'    => 'select',
                        'default' => 'medium',
                        'options' => array(
                            'small'  => esc_html__('Small', 'unitix-admin-td'),
                            'medium' => esc_html__('Medium', 'unitix-admin-td'),
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'big'    => esc_html__('Big', 'unitix-admin-td'),
                            'huge'   => esc_html__('Huge', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'        => esc_html__('Shape', 'unitix-admin-td'),
                        'desc'        => esc_html__('Choose if the image will be roundrd or not', 'unitix-admin-td'),
                        'id'          => 'shape',
                        'type'        => 'select',
                        'default'     => 'round',
                        'admin_label' => true,
                        'options'     => array(
                            'rect'   => esc_html__('Rectunitix', 'unitix-admin-td'),
                            'square' => esc_html__('Square', 'unitix-admin-td'),
                            'round'  => esc_html__('Circle', 'unitix-admin-td'),
                            'hex'    => esc_html__('Hexagon', 'unitix-admin-td'),
                        )
                    ),
                    array(
                        'name'    => esc_html__('Icon Animation', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose an icon animation', 'unitix-admin-td'),
                        'id'      => 'animation',
                        'type'    => 'select',
                        'default' => '',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
                    ),
                    array(
                        'name'      =>  esc_html__('Shadow', 'unitix-admin-td'),
                        'desc'    => esc_html__('Shows a shadow below the shape.', 'unitix-admin-td'),
                        'id'        => 'shape_shadow',
                        'type'      => 'select',
                        'options'   =>  array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                        'default'   => 'show'
                    ),
                )
            )
        )
    ),
    'icon' => array(
        'shortcode'   => 'icon',
        'title'       => esc_html__('Icon', 'unitix-admin-td'),
        'desc'        => esc_html__('Displays a Font Awesome icon.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'General',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Font Size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Size of font to use for icon ( set to 0 to inhertit font size from container )', 'unitix-admin-td'),
                        'id'      => 'size',
                        'type'    => 'slider',
                        'default' => '16',
                        'attr'    => array(
                            'max'  => 48,
                            'min'  => 0,
                            'step' => 1
                        )
                    ),
                )
            ),
            array(
                'title'   => 'Icon',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Icon', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of button to display', 'unitix-admin-td'),
                        'id'      => 'content',
                        'type'    => 'select',
                        'options' => 'icons',
                        'default' => 'glass',
                        'admin_label' => true
                    )
                ),
            ),
        ),
    ),
    'button' =>  array(
        'shortcode'   => 'button',
        'title'       => esc_html__('Button', 'unitix-admin-td'),
        'desc'        => esc_html__('Adds a Bootstrap button to the page.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'General',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Button type', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of button to display', 'unitix-admin-td'),
                        'id'      => 'type',
                        'type'    => 'select',
                        'default' => 'default',
                        'options' => array(
                            'default' => esc_html__('Default', 'unitix-admin-td'),
                            'primary' => esc_html__('Primary', 'unitix-admin-td'),
                            'info'    => esc_html__('Info', 'unitix-admin-td'),
                            'success' => esc_html__('Success', 'unitix-admin-td'),
                            'warning' => esc_html__('Warning', 'unitix-admin-td'),
                            'danger'  => esc_html__('Danger', 'unitix-admin-td'),
                            'link'    => esc_html__('Link', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Button size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Size of button to display', 'unitix-admin-td'),
                        'id'      => 'size',
                        'type'    => 'select',
                        'default' => 'normal',
                        'options' => array(
                            'normal' => esc_html__('Default', 'unitix-admin-td'),
                            'lg'      => esc_html__('Large', 'unitix-admin-td'),
                            'sm'      => esc_html__('Small', 'unitix-admin-td'),
                            'xs'      => esc_html__('Mini', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Text', 'unitix-admin-td'),
                        'id'      => 'title',
                        'holder'  => 'button',
                        'type'    => 'text',
                        'default' => esc_html__('My button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add a label to the button', 'unitix-admin-td'),
                    ),
                    array(
                        'name'        => esc_html__('Open Modal', 'unitix-admin-td'),
                        'desc'        => esc_html__('Select the modal to open on click(overrides the link option)', 'unitix-admin-td'),
                        'id'          => 'modal',
                        'type'        => 'select',
                        'default'     => '',
                        'blank'       => esc_html__('None', 'unitix-admin-td'),
                        'options'     => 'custom_post_id',
                        'post_type'   => 'oxy_modal',
                    ),
                    array(
                        'name'    => esc_html__('Link', 'unitix-admin-td'),
                        'id'      => 'link',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Where the button links to', 'unitix-admin-td'),
                    ),
                )
            ),
            array(
                'title'   => 'Advanced',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Extra classes', 'unitix-admin-td'),
                        'id'      => 'xclass',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Add an extra class to the button', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Open Link In', 'unitix-admin-td'),
                        'id'      => 'link_open',
                        'type'    => 'select',
                        'default' => '_self',
                        'options' => array(
                            '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                            '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                            '_parent' => esc_html__('Open the linked document in the parent frameset', 'unitix-admin-td'),
                            '_top'    => esc_html__('Open the linked document in the full body of the window', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Where the button link opens to', 'unitix-admin-td'),
                    ),
                )
            ),
            array(
                'title'   => 'Icon',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Show Icon', 'unitix-admin-td'),
                        'desc'    => esc_html__('Use an icon in this button', 'unitix-admin-td'),
                        'id'      => 'show_icon',
                        'type'    => 'select',
                        'default' => 'no-icon',
                        'options' => array(
                            'no-icon' => esc_html__('No Icon', 'unitix-admin-td'),
                            'left'    => esc_html__('On Left', 'unitix-admin-td'),
                            'right'   => esc_html__('On Right', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Icon', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of button to display', 'unitix-admin-td'),
                        'id'      => 'icon',
                        'type'    => 'select',
                        'options' => 'icons',
                        'default' => ''
                    )
                ),
            ),
        ),
    ),
    'fancybutton' =>  array(
        'shortcode'   => 'fancybutton',
        'title'       => esc_html__('Fancy Button', 'unitix-admin-td'),
        'desc'        => esc_html__('Creates a fancy call to action button with an icon.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'General',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Button type', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of button to display', 'unitix-admin-td'),
                        'id'      => 'type',
                        'type'    => 'select',
                        'default' => 'default',
                        'admin_label' => true,
                        'options' => array(
                            'default' => esc_html__('Default', 'unitix-admin-td'),
                            'primary' => esc_html__('Primary', 'unitix-admin-td'),
                            'info'    => esc_html__('Info', 'unitix-admin-td'),
                            'success' => esc_html__('Success', 'unitix-admin-td'),
                            'warning' => esc_html__('Warning', 'unitix-admin-td'),
                            'danger'  => esc_html__('Danger', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Button size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Size of button to display', 'unitix-admin-td'),
                        'id'      => 'size',
                        'type'    => 'select',
                        'default' => 'normal',
                        'options' => array(
                            'normal'      => esc_html__('Default', 'unitix-admin-td'),
                            'lg' => esc_html__('Large', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'      => esc_html__('Button Alignment', 'unitix-admin-td'),
                        'id'        => 'align',
                        'type'      => 'select',
                        'default'   => 'center',
                        'options' => array(
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                        ),
                        'desc'    => esc_html__('Align the button.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Text', 'unitix-admin-td'),
                        'id'      => 'label',
                        'type'    => 'text',
                        'admin_label' => true,
                        'default' => esc_html__('My button', 'unitix-admin-td'),
                        'desc'    => esc_html__('Add a label to the button', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Link', 'unitix-admin-td'),
                        'id'      => 'link',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Where the button links to', 'unitix-admin-td'),
                    ),
                )
            ),
            array(
                'title'   => 'Advanced',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Extra classes', 'unitix-admin-td'),
                        'id'      => 'xclass',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Add an extra class to the button', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Open Link In', 'unitix-admin-td'),
                        'id'      => 'link_open',
                        'type'    => 'select',
                        'default' => '_self',
                        'options' => array(
                            '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                            '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                            '_parent' => esc_html__('Open the linked document in the parent frameset', 'unitix-admin-td'),
                            '_top'    => esc_html__('Open the linked document in the full body of the window', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Where the button link opens to', 'unitix-admin-td'),
                    ),
                )
            ),
            array(
                'title'   => 'Icon',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Icon Position', 'unitix-admin-td'),
                        'desc'    => esc_html__('Which side of the button to show the icon.', 'unitix-admin-td'),
                        'id'      => 'icon_position',
                        'type'    => 'select',
                        'default' => 'left',
                        'options' => array(
                            'left'  => esc_html__('Left', 'unitix-admin-td'),
                            'right' => esc_html__('Right', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Icon Animation', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose an icon animation', 'unitix-admin-td'),
                        'id'      => 'animation',
                        'type'    => 'select',
                        'default' => 'none',
                        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
                    ),
                    array(
                        'name'    => esc_html__('Icon', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of button to display', 'unitix-admin-td'),
                        'id'      => 'icon',
                        'admin_label' => true,
                        'type'    => 'select',
                        'options' => 'icons',
                        'default' => ''
                    )
                ),
            ),
        ),
    ),
    'vc_message' => array(
        'shortcode'   => 'vc_message',
        'title'       => esc_html__('Alert', 'unitix-admin-td'),
        'desc'          => esc_html__('Creates a Bootstrap Alert box.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => esc_html__('General', 'unitix-admin-td'),
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Alert type', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of alert to display', 'unitix-admin-td'),
                        'id'      => 'color',
                        'type'    => 'select',
                        'default' => 'alert-success',
                        'options' => array(
                            'alert-success' => esc_html__('Success', 'unitix-admin-td'),
                            'alert-info'    => esc_html__('Information', 'unitix-admin-td'),
                            'alert-warning' => esc_html__('Warning', 'unitix-admin-td'),
                            'alert-danger'  => esc_html__('Danger', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Title', 'unitix-admin-td'),
                        'id'      => 'title',
                        'type'    => 'text',
                        'holder'  => 'h2',
                        'default' => esc_html__('Watch Out!', 'unitix-admin-td'),
                        'desc'    => esc_html__('The bold text that appears first in the alert', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Main Text', 'unitix-admin-td'),
                        'id'      => 'content',
                        'type'    => 'text',
                        'holder'  => 'div',
                        'default' => esc_html__('Change a few things up and try submitting again.', 'unitix-admin-td'),
                        'desc'    => esc_html__('Main text that will appear in the alert box', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Dismissable', 'unitix-admin-td'),
                        'id'      => 'dismissable',
                        'type'    => 'select',
                        'default' => 'false',
                        'desc'    => esc_html__('Defines if the alert can be removed using an x in the corner.', 'unitix-admin-td'),
                        'options' => array(
                            'true'  => esc_html__('Closable', 'unitix-admin-td'),
                            'false' => esc_html__('Not Closable', 'unitix-admin-td'),
                        ),
                    )
                )
            ),
        ),
    ),
    'panel' => array(
        'shortcode' => 'panel',
        'title'     => esc_html__('Panel', 'unitix-admin-td'),
        'desc'      => esc_html__('Creates a Bootstrap Panel with a title and some content.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => esc_html__('General', 'unitix-admin-td'),
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Title', 'unitix-admin-td'),
                        'id'      => 'title',
                        'type'    => 'text',
                        'holder'  => 'h3',
                        'default' => '',
                        'desc'    => esc_html__('The title of the panel.', 'unitix-admin-td'),
                    ),
                )
            )
        ),

    ),
    'progress' =>    array(
        'shortcode'   => 'progress',
        'title'       => esc_html__('Progress Bar', 'unitix-admin-td'),
        'desc'        => esc_html__('Creates a Bootstrap progress bar with a % value.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Percentage', 'unitix-admin-td'),
                        'desc'    => esc_html__('Percentage of the progress bar', 'unitix-admin-td'),
                        'id'      => 'percentage',
                        'type'    => 'slider',
                        'default' => '50',
                        'attr'    => array(
                            'max'  => 100,
                            'min'  => 1,
                            'step' => 1
                        )
                    ),
                    array(
                        'name'    => esc_html__('Bar Type', 'unitix-admin-td'),
                        'desc'    => esc_html__('Type of bar to display', 'unitix-admin-td'),
                        'id'      => 'type',
                        'type'    => 'select',
                        'default' => 'progress',
                        'options' => array(
                            'progress'                        => esc_html__('Normal', 'unitix-admin-td'),
                            'progress progress-striped'       => esc_html__('Striped', 'unitix-admin-td'),
                            'progress progress-striped active'=> esc_html__('Animated', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Bar Style', 'unitix-admin-td'),
                        'desc'    => esc_html__('Style of bar to display', 'unitix-admin-td'),
                        'id'      => 'style',
                        'type'    => 'select',
                        'default' => 'progress-bar-info',
                        'options' => array(
                            'progress-bar-primary'  => esc_html__('primary', 'unitix-admin-td'),
                            'progress-bar-info'     => esc_html__('Info', 'unitix-admin-td'),
                            'progress-bar-success'  => esc_html__('Success', 'unitix-admin-td'),
                            'progress-bar-warning'  => esc_html__('Warning', 'unitix-admin-td'),
                            'progress-bar-danger'   => esc_html__('Danger', 'unitix-admin-td'),
                        ),
                    ),


                )
            ),
        ),
    ),
    'lead' => array(
        'shortcode'   => 'lead',
        'title'       => esc_html__('Lead Paragraph', 'unitix-admin-td'),
        'desc'        => esc_html__('Adds a lead paragraph, with slightly larger and bolder text.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'      => esc_html__('Text Alignment', 'unitix-admin-td'),
                        'id'        => 'align',
                        'type'      => 'select',
                        'default'   => 'center',
                        'options' => array(
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'center' => esc_html__('Center', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                            'justify'  => esc_html__('Justify', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the lead text.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Lead Text', 'unitix-admin-td'),
                        'holder'    => 'p',
                        'id'        => 'content',
                        'type'      => 'textarea',
                        'default'   => '',
                        'desc'    => esc_html__('Text to show in the lead text paragraph.', 'unitix-admin-td'),
                    ),
                )
            )
        )
    ),
    'blockquote' => array(
        'shortcode'   => 'blockquote',
        'title'       => esc_html__('Blockquote', 'unitix-admin-td'),
        'desc'        => esc_html__('Creates a quotation.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'      => esc_html__('Text Alignment', 'unitix-admin-td'),
                        'id'        => 'align',
                        'type'      => 'select',
                        'default'   => 'left',
                        'options' => array(
                            'left'   => esc_html__('Left', 'unitix-admin-td'),
                            'right'  => esc_html__('Right', 'unitix-admin-td'),
                            'justify'  => esc_html__('Justify', 'unitix-admin-td')
                        ),
                        'desc'    => esc_html__('Sets the text alignment of the lead text.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'      => esc_html__('Quote Text', 'unitix-admin-td'),
                        'holder'    => 'p',
                        'id'        => 'content',
                        'type'      => 'textarea',
                        'default'   => '',
                        'desc'    => esc_html__('Text to show in the quote.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Who?', 'unitix-admin-td'),
                        'id'      => 'who',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Who said the quote.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Citation', 'unitix-admin-td'),
                        'id'      => 'cite',
                        'type'    => 'text',
                        'default' => '',
                        'desc'    => esc_html__('Citation of the quote (i.e the source).', 'unitix-admin-td'),
                    ),
                )
            )
        )
    ),
    'code' => array(
        'shortcode'   => 'code',
        'title'       => esc_html__('Code', 'unitix-admin-td'),
        'desc'        => esc_html__('For use adding source code to a page.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'      => esc_html__('Source Code', 'unitix-admin-td'),
                        'holder'    => 'p',
                        'id'        => 'content',
                        'type'      => 'textarea',
                        'default'   => '',
                        'desc'    => esc_html__('Source code to display.', 'unitix-admin-td'),
                    ),
                )
            )
        )
    ),
    'countdown' => array(
        'shortcode'   => 'countdown',
        'title'       => esc_html__('Countdown Timer', 'unitix-admin-td'),
        'desc'        => esc_html__('Adds a countdown timer for coming soon pages.', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'      => esc_html__('Countdown Date', 'unitix-admin-td'),
                        'id'        => 'date',
                        'type'      => 'text',
                        'default'   => '',
                        'admin_label' => true,
                        'desc'    => esc_html__('Date to countdown to in the format YYYY/MM/DD HH:MM.', 'unitix-admin-td'),
                    ),
                    array(
                        'name'    => esc_html__('Number Font Size', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose size of the font to use for the countdown numbers.', 'unitix-admin-td'),
                        'id'      => 'number_size',
                        'type'    => 'select',
                        'options' => array(
                            'normal' => esc_html__('Normal', 'unitix-admin-td'),
                            'super'  => esc_html__('Super (60px)', 'unitix-admin-td'),
                            'hyper'  => esc_html__('Hyper (96px)', 'unitix-admin-td'),
                        ),
                        'default' => 'normal',
                    ),
                    array(
                        'name'    => esc_html__('Number Font Weight', 'unitix-admin-td'),
                        'desc'    => esc_html__('Choose weight of the font of the countdown numbers.', 'unitix-admin-td'),
                        'id'      => 'number_weight',
                        'type'    => 'select',
                        'options' => array(
                            'regular'  => esc_html__('Regular', 'unitix-admin-td'),
                            'black'    => esc_html__('Black', 'unitix-admin-td'),
                            'bold'     => esc_html__('Bold', 'unitix-admin-td'),
                            'light'    => esc_html__('Light', 'unitix-admin-td'),
                            'hairline' => esc_html__('Hairline', 'unitix-admin-td'),
                        ),
                        'default' => 'regular',
                    ),
                    array(
                        'name'    => esc_html__('Number Underline', 'unitix-admin-td'),
                        'desc'    => esc_html__('Adds an underline effect below the countdown numbers.', 'unitix-admin-td'),
                        'id'      => 'number_underline',
                        'default' => 'no-underline',
                        'type' => 'select',
                        'options' => array(
                            'underline'    => esc_html__('Show', 'unitix-admin-td'),
                            'no-underline' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                )
            )
        )
    ),
    'sharing' => array(
        'shortcode'   => 'sharing',
        'title'       => esc_html__('Social Sharing Icons', 'unitix-admin-td'),
        'desc'        => esc_html__('Adds Social Sharing icons to your pages', 'unitix-admin-td'),
        'insert_with' => 'dialog',
        'sections'    => array(
            array(
                'title'   => 'general',
                'fields'  => array(
                    array(
                        'name'    => esc_html__('Show Facebook', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show Facebook share icon', 'unitix-admin-td'),
                        'id'      => 'fb_show',
                        'default' => 'show',
                        'type' => 'select',
                        'options' => array(
                            'show'    => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Show Twitter', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show Twitter share icon', 'unitix-admin-td'),
                        'id'      => 'twitter_show',
                        'default' => 'show',
                        'type' => 'select',
                        'options' => array(
                            'show'    => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Show Google+', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show Google+ share icon', 'unitix-admin-td'),
                        'id'      => 'google_show',
                        'default' => 'show',
                        'type' => 'select',
                        'options' => array(
                            'show'    => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Show Pinterest', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show Pinterest share icon', 'unitix-admin-td'),
                        'id'      => 'pinterest_show',
                        'default' => 'show',
                        'type' => 'select',
                        'options' => array(
                            'show'    => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                    array(
                        'name'    => esc_html__('Show LinkedIn', 'unitix-admin-td'),
                        'desc'    => esc_html__('Show LinkedIn share icon', 'unitix-admin-td'),
                        'id'      => 'linkedin_show',
                        'default' => 'show',
                        'type' => 'select',
                        'options' => array(
                            'show'    => esc_html__('Show', 'unitix-admin-td'),
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                        ),
                    ),
                )
            )
        )
    ),
    'vc_video' => array(
        'shortcode'     => 'vc_video',
        'title'         => esc_html__('Video Player', 'unitix-admin-td'),
        'desc'          => esc_html__('Adds a video player.', 'unitix-admin-td'),
        'insert_with'   => 'dialog',
        'has_content'   => false,
        'sections'      => array(
            array(
                'title' => esc_html__('Video Options', 'unitix-admin-td'),
                'fields' => array(
                    array(
                        'name'      => esc_html__('Video URL', 'unitix-admin-td'),
                        'id'        => 'link',
                        'type'      => 'text',
                        'default'   =>  '',
                    ),
                )
            )
        )
    ),
);
