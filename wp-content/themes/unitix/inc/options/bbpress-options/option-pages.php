<?php
/**
 * Options for BBPres
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license **LICENSE**
 * @version 1.18.7
 */
$extra_bb_press_header_options = array(
    array(
        'name'    => esc_html__('Header Height', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose the amount of padding added to the height of the header', 'unitix-admin-td'),
        'id'      => 'bbpress_header_header_height',
        'type'    => 'select',
        'options' => array(
            'normal'     => esc_html__('Normal', 'unitix-admin-td'),
            'short'      => esc_html__('Short', 'unitix-admin-td'),
            'tiny'       => esc_html__('Tiny', 'unitix-admin-td'),
            'nopadding' => esc_html__('No Padding', 'unitix-admin-td'),
        ),
        'default' => 'normal',
    ),
    array(
        'name'    => esc_html__('Header Swatch', 'unitix-admin-td'),
        'desc'    => esc_html__('Select the colour scheme to use for the header on this page.', 'unitix-admin-td'),
        'id'      => 'bbpress_header_header_swatch',
        'type' => 'select',
        'default' => 'swatch-red-white',
        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
    )
);
$bbpress_header_options = include OXY_THEME_DIR . 'inc/options/global-options/section-header-text.php';
$bbpress_header_section_options = include OXY_THEME_DIR . 'inc/options/global-options/section-background-image.php';

// remove header text
unset($bbpress_header_options[0]);
unset($bbpress_header_options[1]);

// set defaults for blog heading and section
$bbpress_header_options[2]['default'] = 'super';
$bbpress_header_options[3]['default'] = 'light';

global $oxy_theme;
$oxy_theme->register_option_page(array(
    'menu_title' => esc_html__('BBPress', 'unitix-admin-td'),
    'page_title' => esc_html__('BBPress Theme Settings', 'unitix-admin-td'),
    'slug'       => THEME_SHORT . '-bbpress',
    'main_menu'  => false,
    'icon'       => 'tools',
    'sections'   => array(
        'bbpress-general' => array(
            'title'   => esc_html__('General BBPress Options', 'unitix-admin-td'),
            'header'  => esc_html__('', 'unitix-admin-td'),
            'fields' => array(
                array(
                    'name'    => esc_html__('BBPress Page Style', 'unitix-admin-td'),
                    'desc'    => esc_html__('Select a layout style to use for your blog pages.', 'unitix-admin-td'),
                    'id'      => 'bbpress_style',
                    'default' => 'right',
                    'type'    => 'radio',
                    'options' => array(
                        'right'     => esc_html__('Right Sidebar', 'unitix-admin-td'),
                        'left'      => esc_html__('Left Sidebar', 'unitix-admin-td'),
                        'fullwidth' => esc_html__('Full Width', 'unitix-admin-td'),

                    ),
                    'default' => 'right',
                ),
                array(
                    'name'    => esc_html__('BBPres Swatch', 'unitix-admin-td'),
                    'desc'    => esc_html__('Choose a color swatch for the BBPress pages', 'unitix-admin-td'),
                    'id'      => 'bbpress_swatch',
                    'type'    => 'select',
                    'default' => 'swatch-white',
                    'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
                ),
            )
        ),
        'bbpress-header-text' => array(
            'title'   => esc_html__('BBPress Header Titles', 'unitix-admin-td'),
            'header'  => esc_html__('Change the text for some of your BBPress page headings.', 'unitix-admin-td'),
            'fields'  => array(
                array(
                    'name' => esc_html__('Forums Title', 'unitix-admin-td'),
                    'desc' => esc_html__('Title that is shown on the main forums archive page.', 'unitix-admin-td'),
                    'id'   => 'bbpress_header_forums',
                    'type' => 'text',
                    'default' => esc_html__('Forums', 'unitix-admin-td')
                ),
                array(
                    'name' => esc_html__('Topics Title', 'unitix-admin-td'),
                    'desc' => esc_html__('Title that is shown on the main topics archive page.', 'unitix-admin-td'),
                    'id'   => 'bbpress_header_topics',
                    'type' => 'text',
                    'default' => esc_html__('Topics', 'unitix-admin-td')
                )
            )
        ),
        'bbpress-header-options' => array(
            'title'   => esc_html__('BBPress Header Options', 'unitix-admin-td'),
            'header'  => esc_html__('Change how your BBPress headers look.', 'unitix-admin-td'),
            'fields'  => array_merge(
                array(
                    array(
                        'name' => esc_html__('Show Header', 'unitix-admin-td'),
                        'desc' => esc_html__('Show or hide the header at the top of the page.', 'unitix-admin-td'),
                        'id'   => 'bbpress_header_show_header',
                        'type' => 'select',
                        'default' => 'show',
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                    ),
                ),
                array(
                    array(
                        'name' => esc_html__('Show Breadcrumbs', 'unitix-admin-td'),
                        'desc' => esc_html__('Show or hide the breadcrumbs in the header', 'unitix-admin-td'),
                        'id'   => 'bbpress_header_show_breadcrumbs',
                        'type' => 'select',
                        'default' => 'show',
                        'options' => array(
                            'hide' => esc_html__('Hide', 'unitix-admin-td'),
                            'show' => esc_html__('Show', 'unitix-admin-td'),
                        ),
                    )
                )
            )
        ),
        'bbpress-header-heading' => array(
            'title'   => esc_html__('BBPress Header Heading', 'unitix-admin-td'),
            'header'  => esc_html__('Change the text of your BBPress heading here.', 'unitix-admin-td'),
            'fields'  => array_merge( $extra_bb_press_header_options, oxy_prefix_options_id( 'bbpress_header', $bbpress_header_options )),
        ),
        'bbpress-header-section' => array(
            'title'   => esc_html__('BBPress Header Section', 'unitix-admin-td'),
            'header'  => esc_html__('Change the appearance of your BBPress header section.', 'unitix-admin-td'),
            'fields'  => oxy_prefix_options_id( 'bbpress_header', $bbpress_header_section_options )
        ),
    )
));
