<?php
/**
 * Themes shortcode options go here
 *
 * @package Unitix
 * @subpackage Core
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */

return array(
     array(
        'name'        => esc_html__('Header', 'unitix-admin-td'),
        'id'          => 'header',
        'type'        => 'text',
        'default'     => '',
        'desc'        => esc_html__('Header text', 'unitix-admin-td'),
        'admin_label' => true,
    ),
    array(
        'name'    => esc_html__('Header Type', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose the type of header you want to use', 'unitix-admin-td'),
        'id'      => 'header_type',
        'type'    => 'select',
        'options' => array(
            'h1'      => esc_html__('h1', 'unitix-admin-td'),
            'h2'      => esc_html__('h2', 'unitix-admin-td'),
            'h3'      => esc_html__('h3', 'unitix-admin-td'),
            'h4'      => esc_html__('h4', 'unitix-admin-td'),
            'h5'      => esc_html__('h5', 'unitix-admin-td'),
            'h6'      => esc_html__('h6', 'unitix-admin-td')
        ),
        'default' => 'h1',
    ),
    array(
        'name'    => esc_html__('Header Font Size', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose size of the font to use in your header', 'unitix-admin-td'),
        'id'      => 'header_size',
        'type'    => 'select',
        'options' => array(
            'normal' => esc_html__('Normal', 'unitix-admin-td'),
            'super'  => esc_html__('Super (60px)', 'unitix-admin-td'),
            'hyper'  => esc_html__('Hyper (96px)', 'unitix-admin-td'),
        ),
        'default' => 'normal',
    ),
    array(
        'name'    => esc_html__('Header Font Weight', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose weight of the font to use in the header', 'unitix-admin-td'),
        'id'      => 'header_weight',
        'type'    => 'select',
        'options' => array(
            'regular'  => esc_html__('Regular', 'unitix-admin-td'),
            'black'    => esc_html__('Black', 'unitix-admin-td'),
            'bold'     => esc_html__('Bold', 'unitix-admin-td'),
            'light'    => esc_html__('Light', 'unitix-admin-td'),
            'hairline' => esc_html__('Hairline', 'unitix-admin-td'),
        ),
        'default' => 'regular',
    ),
    array(
        'name' => esc_html__('Header Alignment', 'unitix-admin-td'),
        'desc' => esc_html__('Align the text shown in the header left, right or center.', 'unitix-admin-td'),
        'id'   => 'header_align',
        'type' => 'select',
        'default' => 'center',
        'options' => array(
            'center' => esc_html__('Center', 'unitix-admin-td'),
            'left'   => esc_html__('Left', 'unitix-admin-td'),
            'right'  => esc_html__('Right', 'unitix-admin-td'),
            'justify'  => esc_html__('Justify', 'unitix-admin-td')
        )
    ),
    array(
        'name'    => esc_html__('Header Underline', 'unitix-admin-td'),
        'desc'    => esc_html__('Adds an underline effect below the header.', 'unitix-admin-td'),
        'id'      => 'header_underline',
        'default' => 'bordered-header',
        'type' => 'radio',
        'options' => array(
            'bordered-header' => esc_html__('Show', 'unitix-admin-td'),
            'no-bordered-header' => esc_html__('Hide', 'unitix-admin-td'),
        )
    )
);