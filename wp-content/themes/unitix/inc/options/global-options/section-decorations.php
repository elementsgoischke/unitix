<?php
/**
 * All decorations that can be used with sections
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license **LICENSE**
 * @version 1.18.7
 */

return array(
    'none'              => esc_html__('No Decoration', 'unitix-admin-td'),
    'arrow'             => esc_html__('Arrow', 'unitix-admin-td'),
    'circle'            => esc_html__('Circle', 'unitix-admin-td'),
    'clouds'            => esc_html__('Clouds', 'unitix-admin-td'),
    'cross'             => esc_html__('Cross', 'unitix-admin-td'),
    'curtain'           => esc_html__('Curtain', 'unitix-admin-td'),
    'left-slope'        => esc_html__('Left Slope', 'unitix-admin-td'),
    'right-slope'       => esc_html__('Right Slope', 'unitix-admin-td'),
    'lines'             => esc_html__('Lines', 'unitix-admin-td'),
    'rounded'           => esc_html__('Rounded', 'unitix-admin-td'),
    'rounded-inverted'  => esc_html__('Rounded Inverted', 'unitix-admin-td'),
    'triunitix'          => esc_html__('Triunitix', 'unitix-admin-td'),
    'triunitix-inverted' => esc_html__('Triange Inverted', 'unitix-admin-td'),
    'zig-zag'           => esc_html__('Zig Zag', 'unitix-admin-td'),
    'zipper'            => esc_html__('Zipper', 'unitix-admin-td'),
);