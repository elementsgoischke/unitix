<?php
/**
 * Themes shortcode options go here
 *
 * @package Unitix
 * @subpackage Core
 * @since 1.0
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.18.7
 */


return array(
    array(
        'name'    => esc_html__('Background Image', 'unitix-admin-td'),
        'id'      => 'background_image',
        'store'   => 'url',
        'type'    => 'upload',
        'default' => '',
        'desc'    => esc_html__('Choose an image to use for this rows background.', 'unitix-admin-td'),
    ),
    array(
        'name'    => esc_html__('Background Video mp4', 'unitix-admin-td'),
        'id'      => 'background_video',
        'type'    => 'text',
        'default' => '',
        'desc'    => esc_html__('Enter url of a h.264 (mp4) video to use for this rows background.', 'unitix-admin-td'),
    ),
    array(
        'name'    => esc_html__('Background Video webm', 'unitix-admin-td'),
        'id'      => 'background_video_webm',
        'type'    => 'text',
        'default' => '',
        'desc'    => esc_html__('Enter url of a webm video to use for this rows background.', 'unitix-admin-td'),
    ),
    array(
        'name'      => esc_html__('Background Position Vertical', 'unitix-admin-td'),
        'desc'      => esc_html__('Set the vertical position of background image. 0 value represents the top horizontal edge of the section. Positive values will push the background down.', 'unitix-admin-td'),
        'id'        => 'background_position_vertical',
        'type'      => 'slider',
        'default'   => '0',
        'attr'      => array(
            'max'       => 100,
            'min'       => -100,
            'step'      => 1,
        )
    ),
    array(
        'name'      => esc_html__('Overlay Colour', 'unitix-admin-td'),
        'desc'      => esc_html__('Set the colour of the video & image overlay', 'unitix-admin-td'),
        'id'        => 'overlay_colour',
        'type'      => 'colour',
        'default'   => '#000000',
        'attr'      => array(
            'max'       => 1,
            'min'       => 0.1,
            'step'      => 0,
        )
    ),
    array(
        'name'      => esc_html__('Overlay Opacity', 'unitix-admin-td'),
        'desc'      => esc_html__('Set the opacity of the video & image overlay', 'unitix-admin-td'),
        'id'        => 'overlay_opacity',
        'type'      => 'slider',
        'default'   => '0',
        'attr'      => array(
            'max'       => 1,
            'min'       => 0,
            'step'      => 0.1,
        )
    ),
    array(
        'name'      => esc_html__('Overlay Grid', 'unitix-admin-td'),
        'desc'      => esc_html__('Adds an overlay pattern image', 'unitix-admin-td'),
        'id'        => 'overlay_grid',
        'type'    => 'select',
        'options' => array(
            'off'  => esc_html__('Off', 'unitix-admin-td'),
            'on' => esc_html__('On', 'unitix-admin-td')
        ),
        'default' => 'off',
    ),
    array(
        'name'    => esc_html__('Image Background Size', 'unitix-admin-td'),
        'desc'    => esc_html__('Set how the image will fit into the section', 'unitix-admin-td'),
        'id'      => 'background_image_size',
        'type'    => 'select',
        'options' => array(
            'cover' => esc_html__('Full Width', 'unitix-admin-td'),
            'auto'  => esc_html__('Actual Size', 'unitix-admin-td'),
        ),
        'default' => 'cover',
    ),
    array(
        'name'    => esc_html__('Image Background Repeat', 'unitix-admin-td'),
        'id'      => 'background_image_repeat',
        'type'    => 'select',
        'default' => 'no-repeat',
        'options' => array(
            'no-repeat' => esc_html__('No repeat', 'unitix-admin-td'),
            'repeat-x'  => esc_html__('Repeat horizontally', 'unitix-admin-td'),
            'repeat-y'  => esc_html__('Repeat vertically', 'unitix-admin-td'),
            'repeat'    => esc_html__('Repeat horizontally and vertically', 'unitix-admin-td')
        ),
        'desc'    => esc_html__('Set how the image will be repeated', 'unitix-admin-td'),
    ),

    array(
        'name'    => esc_html__('Image Background Parallax', 'unitix-admin-td'),
        'id'      => 'background_image_attachment',
        'type'    => 'select',
        'default' => 'scroll',
        'options' => array(
            'scroll' => esc_html__('Scroll', 'unitix-admin-td'),
            'fixed'  => esc_html__('Fixed', 'unitix-admin-td'),
        ),
        'desc'    => esc_html__('Set the way the background scrolls with the page. Scroll = normal Fixed = Parallax effect.', 'unitix-admin-td'),
    ),
);