<?php
/**
 * Creates all theme metaboxes
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 * @copyright (c) 2014 Oxygenna.com
 * @license **LICENSE**
 * @version 1.18.7
 */

global $oxy_theme;

$extra_header_options = array(
     array(
        'name' => esc_html__('Show Header', 'unitix-admin-td'),
        'desc' => esc_html__('Show or hide the header.', 'unitix-admin-td'),
        'id'   => 'show_header',
        'type' => 'select',
        'default' => 'hide',
        'options' => array(
            'hide' => esc_html__('Hide', 'unitix-admin-td'),
            'show' => esc_html__('Show', 'unitix-admin-td'),
        ),
    ),
    array(
        'name'    => esc_html__('Header Height', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose the amount of padding added to the height of the header', 'unitix-admin-td'),
        'id'      => 'header_height',
        'type'    => 'select',
        'options' => array(
            'normal'     => esc_html__('Normal', 'unitix-admin-td'),
            'short'      => esc_html__('Short', 'unitix-admin-td'),
            'tiny'       => esc_html__('Tiny', 'unitix-admin-td'),
            'nopadding' => esc_html__('No Padding', 'unitix-admin-td'),
        ),
        'default' => 'normal',
    ),
    array(
        'name'    => esc_html__('Header Swatch', 'unitix-admin-td'),
        'desc'    => esc_html__('Select the colour scheme to use for the header on this page.', 'unitix-admin-td'),
        'id'      => 'header_swatch',
        'type' => 'select',
        'default' => 'swatch-red-white',
        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
    )
);
$section_header_options = include OXY_THEME_DIR . 'inc/options/global-options/section-header-text.php';
$section_header_background_options = include OXY_THEME_DIR . 'inc/options/global-options/section-background-image.php';

/*  PAGE HEADER OPTIONS */
$oxy_theme->register_metabox( array(
    'id' => 'page_header_header',
    'title' => esc_html__('Header Options', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('page', 'oxy_service'),
    'javascripts' => array(
        array(
            'handle' => 'header_options_script',
            'src'    => OXY_THEME_URI . 'inc/assets/js/metaboxes/header-options.js',
            'deps'   => array( 'jquery'),
            'localize' => array(
                'object_handle' => 'theme',
                'data'          => THEME_SHORT
            ),
        ),
    ),
    'fields' => array_merge( $extra_header_options, $section_header_options, $section_header_background_options )
));

// Page sidebar option
$oxy_theme->register_metabox( array(
    'id' => 'page_sidebar_swatch',
    'title' => esc_html__('Sidebar Template Options', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('page', 'oxy_service'),
    'javascripts' => array(
        array(
            'handle' => 'sidebar_swatch',
            'src'    => OXY_THEME_URI . 'inc/assets/js/metaboxes/sidebar-options.js',
            'deps'   => array( 'jquery'),
        ),
    ),
    'fields' => array(
        array(
            'name'    => esc_html__('Page Swatch', 'unitix-admin-td'),
            'desc'    => esc_html__('Select the colour scheme to use for this page and sidebar.', 'unitix-admin-td'),
            'id'      => 'sidebar_page_swatch',
            'type' => 'select',
            'default' => 'swatch-white-red',
            'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
        ),
        array(
            'name'    => esc_html__('Page Decoration', 'unitix-admin-td'),
            'desc'    => esc_html__('Choose a decoration to use at the top of this page.', 'unitix-admin-td'),
            'id'      => 'sidebar_page_decoration',
            'type'    => 'select',
            'default' => '',
            'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
        ),
    )
));

/*  PAGE HEADER OPTIONS */
$default_swatches = include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php';
$oxy_theme->register_metabox( array(
    'id' => 'page_site_overrides',
    'title' => esc_html__('Site Overrides', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('page'),
    'fields' => array(
        array(
            'name'    => esc_html__('Show Top Bar', 'unitix-admin-td'),
            'desc'    => esc_html__('Show or hide the sites top bar (ideal for landing pages).', 'unitix-admin-td'),
            'id'      => 'site_top_bar',
            'type' => 'select',
            'default' => 'show',
            'options' => array(
                'show' => esc_html__('Show Top Bar', 'unitix-admin-td'),
                'hide' => esc_html__('Hide Top Bar', 'unitix-admin-td'),
            )
        ),
        array(
            'name'    => esc_html__('Show Top Navigation', 'unitix-admin-td'),
            'desc'    => esc_html__('Show or hide the sites top navigation (ideal for landing pages).', 'unitix-admin-td'),
            'id'      => 'site_top_nav',
            'type' => 'select',
            'default' => 'show',
            'options' => array(
                'show' => esc_html__('Show Top Nav', 'unitix-admin-td'),
                'hide' => esc_html__('Hide Top Nav', 'unitix-admin-td'),
            )
        ),
        array(
            'name'    => esc_html__('Override Top Navigation Swatch', 'unitix-admin-td'),
            'desc'    => esc_html__('Override the default site top nav swatch (only for this page).', 'unitix-admin-td'),
            'id'      => 'site_top_swatch',
            'type' => 'select',
            'default' => '',
            'options' => array_merge( array(
                '' => esc_html__('Default Top Nav Swatch', 'unitix-admin-td'),
            ), $default_swatches )
        ),
        array(
            'name'    => esc_html__('Override Footer Swatch', 'unitix-admin-td'),
            'desc'    => esc_html__('Override the default site footer swatch (only for this page).', 'unitix-admin-td'),
            'id'      => 'site_footer_swatch',
            'type' => 'select',
            'default' => '',
            'options' => array_merge( array(
                '' => esc_html__('Default Footer Swatch', 'unitix-admin-td'),
            ), $default_swatches )
        ),
    )
));

/* SWATCH METABOX */
$oxy_theme->register_metabox( array(
    'id'       => 'swatch_colours_metabox',
    'title'    => esc_html__('Swatch Colours', 'unitix-admin-td'),
    'priority' => 'default',
    'context'  => 'advanced',
    'pages'    => array('oxy_swatch'),
    'fields'   => array(
        array(
            'name'    => esc_html__('Text Colour', 'unitix-admin-td'),
            'id'      => 'text',
            'desc'    => esc_html__('Text colour to use for this swatch.', 'unitix-admin-td'),
            'default' => '#444',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Heading Colour', 'unitix-admin-td'),
            'id'      => 'header',
            'desc'    => esc_html__('Colour of all headings in this swatch.', 'unitix-admin-td'),
            'default' => '#222',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Small Colour', 'unitix-admin-td'),
            'id'      => 'small',
            'desc'    => esc_html__('Colour of <small> tags.', 'unitix-admin-td'),
            'default' => '#666',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Icon Colour', 'unitix-admin-td'),
            'id'      => 'icon',
            'desc'    => esc_html__('Colour of all icons.', 'unitix-admin-td'),
            'default' => '#444',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Link Colour', 'unitix-admin-td'),
            'id'      => 'link',
            'desc'    => esc_html__('Colour of all text links.', 'unitix-admin-td'),
            'default' => '#e74c3c',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Link Colour Hover', 'unitix-admin-td'),
            'id'      => 'link_hover',
            'desc'    => esc_html__('Colour of all text links on hover.', 'unitix-admin-td'),
            'default' => '#df2e1b',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Link Colour Active', 'unitix-admin-td'),
            'id'      => 'link_active',
            'desc'    => esc_html__('Colour of all text links he moment it is clicked.', 'unitix-admin-td'),
            'default' => '#df2e1b',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Link Colour Headings', 'unitix-admin-td'),
            'id'      => 'link_headings',
            'desc'    => esc_html__('Colour of all heading links.', 'unitix-admin-td'),
            'default' => '#e74c3c',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Background Colour', 'unitix-admin-td'),
            'id'      => 'background',
            'desc'    => esc_html__('Background colour used for this swatch.', 'unitix-admin-td'),
            'default' => '#FFF',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Foreground Colour', 'unitix-admin-td'),
            'id'      => 'foreground',
            'desc'    => esc_html__('Colour used for foreground elements in this swatch.', 'unitix-admin-td'),
            'default' => '#e74c3c',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Overlay Colour', 'unitix-admin-td'),
            'id'      => 'overlay',
            'desc'    => esc_html__('Colour used for overlays e.g. portfolio hover effect.', 'unitix-admin-td'),
            'default' => '#000',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Overlay Opacity %', 'unitix-admin-td'),
            'desc'    => esc_html__('How see through is the overlay in percentage.', 'unitix-admin-td'),
            'id'      => 'overlay_alpha',
            'type'    => 'slider',
            'default' => 7,
            'attr'    => array(
                'max'  => 100,
                'min'  => 0,
                'step' => 1
            )
        ),
        array(
            'name'    => esc_html__('Form Background Colour', 'unitix-admin-td'),
            'id'      => 'form_background',
            'desc'    => esc_html__('Colour used for background of form elements.', 'unitix-admin-td'),
            'default' => '#e9eeef',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Form Text Colour', 'unitix-admin-td'),
            'id'      => 'form_text',
            'desc'    => esc_html__('Colour used for text of form elements.', 'unitix-admin-td'),
            'default' => '#888',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Form Active Colour', 'unitix-admin-td'),
            'id'      => 'form_active',
            'desc'    => esc_html__('Colour used for border of an active input element.', 'unitix-admin-td'),
            'default' => '#cddadd',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Primary Button Colour', 'unitix-admin-td'),
            'id'      => 'primary_button_background',
            'desc'    => esc_html__('Colour used for all primary buttons used in this swatch.', 'unitix-admin-td'),
            'default' => '#e74c3c',
            'type'    => 'colour',
        ),
        array(
            'name'    => esc_html__('Primary Button Text Colour', 'unitix-admin-td'),
            'id'      => 'primary_button_text',
            'desc'    => esc_html__('Colour used for all primary button text used in this swatch.', 'unitix-admin-td'),
            'default' => '#FFF',
            'type'    => 'colour',
        )
    )
));

// swatch status metabox
$oxy_theme->register_metabox( array(
    'id'       => 'swatch_status_metabox',
    'title'    => esc_html__('Swatch Status', 'unitix-admin-td'),
    'priority' => 'default',
    'context'  => 'side',
    'pages'    => array('oxy_swatch'),
    'fields'   => array(
        array(
            'name'    => esc_html__('Swatch Status', 'unitix-admin-td'),
            'id'      => 'status',
            'desc'    => esc_html__('Turns the swatch on and off.', 'unitix-admin-td'),
            'default' => 'active',
            'type'    => 'select',
            'options' => array(
                'enabled' => esc_html__('Enabled', 'unitix-admin-td'),
                'disabled' => esc_html__('Disabled', 'unitix-admin-td'),
            )
        ),
    )
));

$link_options = array(
    'id'    => 'link_metabox',
    'title' => esc_html__('Link', 'unitix-admin-td'),
    'priority' => 'default',
    'context'  => 'advanced',
    'pages'    => array('oxy_service', 'oxy_staff', 'oxy_portfolio_image'),
    'javascripts' => array(
        array(
            'handle' => 'slider_links_options_script',
            'src'    => OXY_THEME_URI . 'inc/assets/js/metaboxes/slider-links-options.js',
            'deps'   => array( 'jquery'),
            'localize' => array(
                'object_handle' => 'theme',
                'data'          => THEME_SHORT
            ),
        ),
    ),
    'fields'  => array(
        array(
            'name' => esc_html__('Link Type', 'unitix-admin-td'),
            'desc' => esc_html__('Make this post link to something.  Default link will link to the single item page.', 'unitix-admin-td'),
            'id'   => 'link_type',
            'type' => 'select',
            'options' => array(
                'default'   => esc_html__('Default Link', 'unitix-admin-td'),
                'page'      => esc_html__('Page', 'unitix-admin-td'),
                'post'      => esc_html__('Post', 'unitix-admin-td'),
                'portfolio' => esc_html__('Portfolio', 'unitix-admin-td'),
                'category'  => esc_html__('Category', 'unitix-admin-td'),
                'url'       => esc_html__('URL', 'unitix-admin-td')
            ),
            'default' => 'default',
        ),
        array(
            'name'     => esc_html__('Page Link', 'unitix-admin-td'),
            'desc'     => esc_html__('Choose a page to link this item to', 'unitix-admin-td'),
            'id'       => 'page_link',
            'type'     => 'select',
            'options'  => 'taxonomy',
            'taxonomy' => 'pages',
            'default' =>  '',
        ),
        array(
            'name'     => esc_html__('Post Link', 'unitix-admin-td'),
            'desc'     => esc_html__('Choose a post to link this item to', 'unitix-admin-td'),
            'id'       => 'post_link',
            'type'     => 'select',
            'options'  => 'taxonomy',
            'taxonomy' => 'posts',
            'default' =>  '',
        ),
        array(
            'name'     => esc_html__('Portfolio Link', 'unitix-admin-td'),
            'desc'     => esc_html__('Choose a portfolio item to link this item to', 'unitix-admin-td'),
            'id'       => 'portfolio_link',
            'type'     => 'select',
            'options'  => 'taxonomy',
            'taxonomy' => 'oxy_portfolio_image',
            'default' =>  '',
        ),
        array(
            'name'     => esc_html__('Category Link', 'unitix-admin-td'),
            'desc'     => esc_html__('Choose a category list to link this item to', 'unitix-admin-td'),
            'id'       => 'category_link',
            'type'     => 'select',
            'options'  => 'categories',
            'default' =>  '',
        ),
        array(
            'name'    => esc_html__('URL Link', 'unitix-admin-td'),
            'desc'     => esc_html__('Choose a URL to link this item to', 'unitix-admin-td'),
            'id'      => 'url_link',
            'type'    => 'text',
            'default' =>  '',
        ),
        array(
            'name'    => esc_html__('Open Link In', 'unitix-admin-td'),
            'id'      => 'target',
            'type'    => 'select',
            'default' => '_self',
            'options' => array(
                '_self'   => esc_html__('Same page as it was clicked ', 'unitix-admin-td'),
                '_blank'  => esc_html__('Open in new window/tab', 'unitix-admin-td'),
                '_parent' => esc_html__('Open the linked document in the parent frameset', 'unitix-admin-td'),
                '_top'    => esc_html__('Open the linked document in the full body of the window', 'unitix-admin-td')
            ),
            'desc'    => esc_html__('Where the link will open.', 'unitix-admin-td'),
        ),
    ),
);

$oxy_theme->register_metabox( $link_options );

// modify link options metabox for slideshow image before registering
unset($link_options['fields'][0]['options']['default']);
$link_options['fields'][0]['options']['none'] = esc_html__('No Link', 'unitix-admin-td');
$link_options['fields'][0]['default'] = 'none';
$link_options['pages'] = array('oxy_slideshow_image');
$link_options['id'] = 'slide_link_metabox';
$link_options['fields'][6]['options']['magnific'] = esc_html__('Open in magnific popup', 'unitix-admin-td');

$oxy_theme->register_metabox( $link_options );


$oxy_theme->register_metabox( array(
    'id' => 'Citation',
    'title' => esc_html__('Citation', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_testimonial'),
    'fields' => array(
        array(
            'name'    => esc_html__('Citation', 'unitix-admin-td'),
            'desc'    => esc_html__('Reference to the source of the quote', 'unitix-admin-td'),
            'id'      => 'citation',
            'type'    => 'text',
        ),
    )
));

$oxy_theme->register_metabox( array(
    'id' => 'services_icon_metabox',
    'title' => esc_html__('Service Image & Icon', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_service'),
    'fields' => array(
        array(
            'name' => esc_html__('Image Type', 'unitix-admin-td'),
            'id'   => 'image_type',
            'desc'    => esc_html__('Services can show an uploaded Image, or an Icon or Both Image and Icon.', 'unitix-admin-td'),
            'type' => 'select',
            'options' => array(
                'image'       => esc_html__('Featured Image', 'unitix-admin-td'),
                'icon'        => esc_html__('Icon', 'unitix-admin-td'),
                'both'        => esc_html__('Featured Image & Icon', 'unitix-admin-td'),
                'nothing'     => esc_html__('Nothing', 'unitix-admin-td'),
            )
        ),
        array(
            'name'    => esc_html__('Icon', 'unitix-admin-td'),
            'desc'    => esc_html__('If you select Icon or Featured Image & Icon Image Type this is the icon that will be used.', 'unitix-admin-td'),
            'id'      => 'icon',
            'type'    => 'select',
            'options' => 'icons',
        ),
        array(
            'name'    => esc_html__('Animation', 'unitix-admin-td'),
            'desc'    => esc_html__('If you select Icon or Featured Image & Icon Image Type the animation will be applied to the icon. If Feature Image is selected, the animation will be applied to the image.', 'unitix-admin-td'),
            'id'      => 'icon_animation',
            'type'    => 'select',
            'default' => 'bounce',
            'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-button-animations.php'
        ),
    )
));

// STAFF METABOXES

$oxy_theme->register_metabox( array(
    'id' => 'staff_swatch',
    'title' => esc_html__('Swatch', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_staff'),
    'fields' => array(
        array(
            'name'    => esc_html__('Staff Detail Section Swatch', 'unitix-admin-td'),
            'desc'    => esc_html__('Select the colour scheme to use for the top section in the detail page.', 'unitix-admin-td'),
            'id'      => 'staff_swatch',
            'type' => 'select',
            'default' => 'swatch-white-red',
            'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
        )
    )
));

$oxy_theme->register_metabox( array(
    'id' => 'staff_info',
    'title' => esc_html__('Personal Details', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_staff'),
    'fields' => array(
        array(
            'name'    => esc_html__('Job Title', 'unitix-admin-td'),
            'desc'    => esc_html__('Sub header that is shown below the staff members name.', 'unitix-admin-td'),
            'id'      => 'position',
            'type'    => 'text',
        ),
        array(
            'name'    => esc_html__('Personal Moto Title', 'unitix-admin-td'),
            'desc'    => esc_html__('The cheeky title that pops up when a staff member image is hovered over.', 'unitix-admin-td'),
            'id'      => 'moto_title',
            'type'    => 'text',
        ),
        array(
            'name'    => esc_html__('Personal Moto Text', 'unitix-admin-td'),
            'desc'    => esc_html__('The cheeky text that pops up when a staff member image is hovered over.', 'unitix-admin-td'),
            'id'      => 'moto_text',
            'type'    => 'text',
        ),
    )
));

$staff_social = array();
for( $i = 0 ; $i < 5 ; $i++ ) {
    $staff_social[] =
        array(
            'name' => esc_html__('Social Icon', 'unitix-admin-td') . ' ' . ($i+1),
            'desc' => esc_html__('Social Network Icon to show for this Staff Member', 'unitix-admin-td'),
            'id'   => 'icon' . $i,
            'type' => 'select',
            'options' => 'icons',
        );
    $staff_social[] =
        array(
            'name'  => esc_html__('Social Link', 'unitix-admin-td'). ' ' . ($i+1),
            'desc' => esc_html__('Add the url to the staff members social network here.', 'unitix-admin-td'),
            'id'    => 'link' . $i,
            'type'  => 'text',
            'std'   => '',
        );
}

$oxy_theme->register_metabox( array(
    'id' => 'staff_social',
    'title' => esc_html__('Social', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_staff'),
    'fields' => $staff_social,
));

$oxy_theme->register_metabox( array(
    'id' => 'portfolio_template_metabox',
    'title' => esc_html__('Portfolio Options', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_portfolio_image'),
    'fields' => array(
        array(
            'name'    => esc_html__('Portfolio Swatch', 'unitix-admin-td'),
            'desc'    => esc_html__('Select the colour scheme to use for the portfolio.', 'unitix-admin-td'),
            'id'      => 'portfolio_swatch',
            'type' => 'select',
            'default' => 'swatch-white-red',
            'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
        ),
        array(
            'name' => esc_html__('Portfolio Template', 'unitix-admin-td'),
            'id'   => 'template',
            'desc'    => esc_html__('Select a template to use for this portfolio item single page. Big Image - Will add a full width header to the top of the page which will contain the featured image, Header Title & Description with the skill list on the right side. Smaller Image - Will create a header with a smaller image and Header Title, Description and link on the right side of the image.Full Width Page - Allows you to create your own page using sections, just like a regular full width page.', 'unitix-admin-td'),
            'type' => 'select',
            'options' => array(
                'big'     => esc_html__('Big Image', 'unitix-admin-td'),
                'small'   => esc_html__('Smaller Image', 'unitix-admin-td'),
                'page.php'=> esc_html__('Fullwidth Page Template', 'unitix-admin-td'),
            )
        ),
        array(
            'name'    => esc_html__('Top Navigation', 'unitix-admin-td'),
            'desc'    => esc_html__('Display navigation and title at the top of the page.Otherwise only title will be displayed above description', 'unitix-admin-td'),
            'id'      => 'navigation',
            'type'    => 'select',
            'options' => array(
                'on'    => esc_html__('On', 'unitix-admin-td'),
                'off'   => esc_html__('Off', 'unitix-admin-td'),
            ),
            'default' => 'on',
        ),
        array(
            'name'  => esc_html__('Small template side text', 'unitix-admin-td'),
            'id'    => 'description',
            'type'  => 'textarea',
            'attr'  => array( 'rows' => '10', 'style' => 'width:100%' ),
            'desc'  => esc_html__('Text will appear in single portfolio small template on the right side of the page', 'unitix-admin-td'),
            'std'   => '',
        ),
        array(
            'name'  => esc_html__('External url', 'unitix-admin-td'),
            'desc'  => esc_html__('Link will appear in the single portfolio item underneath the skills list.', 'unitix-admin-td'),
            'id'    => 'link',
            'type'  => 'text',
            'std'   => '',
        ),
    )
));

$oxy_theme->register_metabox( array(
    'id'       => 'service_template_metabox',
    'title'    => esc_html__('Service Template', 'unitix-admin-td'),
    'priority' => 'default',
    'context'  => 'advanced',
    'pages'    => array('oxy_service'),
    'fields'   => array(
        array(
            'name'    => esc_html__('Service Page Template', 'unitix-admin-td'),
            'id'      => 'template',
            'desc'    => esc_html__('Select a page template to use for this service', 'unitix-admin-td'),
            'type'    => 'select',
            'options' => array(
                'page.php'                  => esc_html__('Full Width', 'unitix-admin-td'),
                'template-leftsidebar.php'  => esc_html__('Left Sidebar', 'unitix-admin-td'),
                'template-rightsidebar.php' => esc_html__('Right Sidebar', 'unitix-admin-td'),
            ),
            'default' => 'page.php',
        ),
    )
));

$product_category_options = array(
    array(
        'name'    => esc_html__('Category Swatch', 'unitix-admin-td'),
        'desc'    => esc_html__('Swatch that will be used by this category page.', 'unitix-admin-td'),
        'id'      => 'category_swatch',
        'type'    => 'select',
        'default' => 'swatch-white-red',
        'options' => include OXY_THEME_DIR . 'inc/options/shortcodes/shortcode-swatches-options.php'
    ),
    array(
        'name'    => esc_html__('Category Decoration', 'unitix-admin-td'),
        'desc'    => esc_html__('Choose a decoration style to use at the top of your category page.', 'unitix-admin-td'),
        'id'      => 'category_decoration',
        'type'    => 'select',
        'default' => '',
        'options' => include OXY_THEME_DIR . 'inc/options/global-options/section-decorations.php',
    )
);
$oxy_theme->register_metabox( array(
    'id' => 'category_header',
    'title' => esc_html__('Category Header Type', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'taxonomies' => array('product_cat'),
    'fields' => array_merge( $product_category_options, $extra_header_options, $section_header_options, $section_header_background_options )
));

$oxy_theme->register_metabox( array(
    'id' => 'tag_header',
    'title' => esc_html__('Product Tag Header Type', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'taxonomies' => array('product_tag'),
    'fields' => array_merge( $product_category_options, $extra_header_options, $section_header_options, $section_header_background_options )
));

$oxy_theme->register_metabox(array(
    'id' => 'post_modal_options',
    'title' => esc_html__('Modal Options', 'unitix-admin-td'),
    'priority' => 'default',
    'context' => 'advanced',
    'pages' => array('oxy_modal'),
    'fields' => array(
        array(
            'name' => esc_html__('Fade Modal', 'unitix-admin-td'),
            'desc' => esc_html__('Apply a CSS fade transition to the modal.', 'unitix-admin-td'),
            'id'   => 'fade_modal',
            'type' => 'select',
            'default' => 'fade',
            'options' => array(
                'fade'    => esc_html__('On', 'unitix-admin-td'),
                'no-fade' => esc_html__('Off', 'unitix-admin-td'),
            )
        ),
        array(
            'name' => esc_html__('Modal Size', 'unitix-admin-td'),
            'desc' => esc_html__('Modal size can be large, normal or small.', 'unitix-admin-td'),
            'id'   => 'modal_size',
            'type' => 'select',
            'options' => array(
                'modal-lg' => esc_html__('Large Modal', 'unitix-admin-td'),
                'modal-nm' => esc_html__('Normal Modal', 'unitix-admin-td'),
                'modal-sm' => esc_html__('Small Modal', 'unitix-admin-td'),
            ),
            'default' => 'modal-nm',
        ),
    )
));
