<?php
/**
 * Extra custom classes for the VC composer
 *
 * @package Unitix
 * @subpackage Admin
 * @since 0.1
 *
 */

if( class_exists('WPBakeryShortCodesContainer') && class_exists('WPBakeryShortCode') ) {
    // Features list and Feature   
    class WPBakeryShortCode_Panel extends WPBakeryShortCodesContainer {
    }    
}